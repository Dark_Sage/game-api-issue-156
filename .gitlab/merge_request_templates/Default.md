**Related Issue URL:**\
<ENTER HERE>

---

**Description of changes:**\
<ENTER HERE>

---

**Smart commit messages:** [(?)](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#default-closing-pattern)  
<ENTER HERE>

---

<p>
<details>
<summary>RuneMate Contributor Agreement</summary>
This RuneMate Contributor Agreement (this "Agreement") applies to any Contributions you make to any Work.
<br /><br />
This is a binding legal agreement on you and any organization you represent. If you are signing this Agreement on behalf of your employer or other organization, you represent and warrant that you have the authority to agree to this Agreement on behalf of the organization.
<br /><br />
<b>1. Definitions.</b>

"Contribution" means any original work, including any modification of or addition to any existing work, that you submit to RuneMate in any manner for inclusion in any Work.

"We" and "us" means RuneMate.

"Work" means any project, work or materials owned or managed by RuneMate.

"You" and "your" means you and any organization on whose behalf you are entering this Agreement.

<b>2. Copyright Assignment, License and Waiver.</b>

 - <b>(a) Assignment.</b> By submitting a Contribution, you assign to RuneMate all right, title and interest in any copyright you have in the Contribution, and you waive any rights that may affect our ownership of the copyright in the Contribution.

 - <b>(b) License to RuneMate.</b> If your assignment in Section 2(a) is ineffective for any reason, you grant to us and to any recipient of any Work distributed by us, a perpetual, worldwide, transferable, non-exclusive, no-charge, royalty-free, irrevocable, and sublicensable license to use, reproduce, prepare derivative works of, publicly display, publicly perform, sublicense, and distribute Contributions and any derivative work created based on a Contribution. If your license grant is ineffective for any reason, you irrevocably waive and covenant to not assert any claim you may have against us, our successors in interest, and any of our direct or indirect licensees and customers, arising out of our, our successors in interest's, or any of our direct or indirect licensees' or customers' use, reproduction, preparation of derivative works, public display, public performance, sublicense, and distribution of a Contribution.

 - <b>(c) License to You.</b> We grant to you a perpetual, worldwide, transferable, non-exclusive, no-charge, royalty-free, irrevocable, and sublicensable license to use, reproduce, prepare derivative works of, publicly display, publicly perform, sublicense, and distribute a Contribution and any derivative works you create based on a Contribution.

<b>3. Patent License.</b> You grant to us and to any recipient of any Work distributed by us, a perpetual, worldwide, transferable, non-exclusive, no-charge, royalty-free, irrevocable, and sublicensable patent license to make, have made, use, sell, offer to sell, import, and otherwise transfer the Contribution in whole or in part, alone or included in any Work under any patent you own, or license from a third party, that is necessarily infringed by the Contribution or by combination of the Contribution with any Work.

<b>4. Your Representations and Warranties.</b> By submitting a Contribution, you represent and warrant that: (a) each Contribution you submit is an original work and you can legally grant the rights set out in this Agreement; (b) the Contribution does not, and any exercise of the rights granted by you will not, infringe any third party's intellectual property or other right; and (c) you are not aware of any claims, suits, or actions pertaining to the Contribution. You will notify us immediately if you become aware or have reason to believe that any of your representations and warranties is or becomes inaccurate.

<b>5. Intellectual Property.</b> Except for the assignment and licenses set forth in this Agreement, this Agreement does not transfer any right, title or interest in any intellectual property right of either party to the other. If you choose to provide us with suggestions, ideas or improvement, recommendations or other feedback, on any Work we may use your feedback without any restriction or payment.

<b>6. Miscellaneous.</b> This Agreement does not create a partnership, agency relationship, or joint venture between the parties. We may assign this Agreement without notice or restriction. If any provision of this Agreement is unenforceable, that provision will be modified to render it enforceable to the extent possible to effect the parties' intention and the remaining provisions will not be affected. The parties may amend this Agreement only in a written amendment signed by both parties. This Agreement comprises the parties' entire agreement relating to the subject matter of this Agreement.
</details>
</p>

 - [ ] I agree to and accept the RuneMate Contributor Agreement on my behalf and on behalf of my organization, if applicable.


