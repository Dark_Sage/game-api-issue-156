import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
    idea
    `java-library`
    `maven-publish`
    kotlin("jvm") version "1.6.10"
    id("io.freefair.lombok") version "6.3.0"
    id("org.jetbrains.dokka") version "1.6.10"
    id("com.github.johnrengelman.shadow") version "7.1.2"
}

group = "com.runemate"
version = "1.8.4"

repositories {
    maven("https://gitlab.com/api/v4/projects/10471880/packages/maven")
    mavenCentral()
    mavenLocal()
}

val bootClass by extra("com.runemate.client.boot.Boot")

val externalRepositoryUrl: String by project
val externalRepositoryPrivateToken: String? by project

val runemate by configurations.creating {
    configurations["implementation"].extendsFrom(this)
}

dependencies {
    compileOnly(kotlin("stdlib"))
    runemate("com.runemate:runemate-client:3.6.3.0:all")
    implementation("org.json:json:20211205")
    implementation("org.jblas:jblas:1.2.5")

    compileOnly("com.google.code.gson:gson:2.8.9")
    compileOnly("org.jetbrains:annotations:22.0.0")
    compileOnly("com.google.guava:guava:31.0.1-jre")
    compileOnly("org.apache.commons:commons-lang3:3.12.0")
    compileOnly("org.apache.commons:commons-math3:3.6.1")
    compileOnly("org.apache.commons:commons-text:1.10.0")
    compileOnly("commons-io:commons-io:2.11.0")
}

tasks {
    withType<JavaCompile> {
        options.encoding = "UTF-8"
    }

    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = JavaVersion.VERSION_11.toString()
    }

    java {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11

        withSourcesJar()
    }
}

val dokkaJavadocJar by tasks.register<Jar>("dokkaJavadocJar") {
    dependsOn(tasks.dokkaJavadoc)
    from(tasks.dokkaJavadoc.flatMap { it.outputDirectory })
    archiveClassifier.set("javadoc")
}

val dokkaHtmlJar by tasks.register<Jar>("dokkaHtmlJar") {
    dependsOn(tasks.dokkaHtml)
    from(tasks.dokkaHtml.flatMap { it.outputDirectory })
    archiveClassifier.set("html-doc")
}

task("launch", JavaExec::class) {
    group = "runemate"
    classpath = files(runemate, tasks.shadowJar)
    mainClass.set(bootClass)
}

tasks.shadowJar {
    dependencies {
        runemate.dependencies.forEach {
            exclude(dependency(it))
        }
    }
}

tasks.register("testJar", Jar::class) {
    group = "build"
    from(sourceSets.test.get().output)
    archiveClassifier.set("test")
}

publishing {
    publications {
        register("mavenJava", MavenPublication::class) {
            from(components["java"])
            artifact(dokkaJavadocJar)
            artifact(dokkaHtmlJar)
        }
        register("localMavenJava", MavenPublication::class) {
            from(components["java"])
        }
    }
    repositories.maven {
        name = "external"
        url = uri(externalRepositoryUrl)
        if (externalRepositoryPrivateToken != null) {
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = externalRepositoryPrivateToken
            }
            authentication.create<HttpHeaderAuthentication>("header")
        }
    }
}
