package com.runemate.ui.binding;

import java.text.*;
import javafx.beans.binding.*;
import javafx.collections.*;

public class MetricBinding extends StringBinding {

    private final NumberFormat format;
    private final NumberExpression expression;

    public MetricBinding(final NumberExpression expression) {
        this(expression, 0);
    }

    public MetricBinding(final NumberExpression expression, final int decimalPlaces) {
        this.expression = expression;
        super.bind(expression);
        format = NumberFormat.getNumberInstance();
        format.setMaximumFractionDigits(decimalPlaces);
    }

    @Override
    protected String computeValue() {
        if (format.getMaximumFractionDigits() > 0) {
            return format.format(expression.doubleValue());
        }
        return format.format(expression.longValue());
    }

    @Override
    public void dispose() {
        super.unbind(expression);
    }

    @Override
    public ObservableList<?> getDependencies() {
        return FXCollections.singletonObservableList(expression);
    }

}
