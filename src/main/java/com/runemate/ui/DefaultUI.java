package com.runemate.ui;

import com.runemate.game.api.client.embeddable.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.internal.*;
import com.runemate.ui.control.*;
import javafx.animation.*;
import javafx.beans.property.*;
import javafx.scene.*;
import javafx.util.*;
import lombok.*;
import lombok.experimental.*;

@Accessors(fluent = true)
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class DefaultUI implements EmbeddableUI {

    @Getter
    AbstractBot bot;
    @Getter
    StopWatch runtime = new StopWatch();
    @Getter
    LongProperty runtimeProperty = new SimpleLongProperty(0);

    ObjectProperty<ControlPanel> controlPanel;
    Timeline timeline;

    public DefaultUI(@NonNull final AbstractBot bot) {
        this.bot = bot;
        this.runtime.start();

        timeline = new Timeline(new KeyFrame(Duration.millis(100)));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();

        this.controlPanel = new SimpleObjectProperty<>(new ControlPanel(this));
        //Update stopwatch task
        addRepeatTask(() -> {
            runtimeProperty().set(runtime().getRuntime());
            if (bot().isStopped()) {
                timeline.stop();
            }
        });
        //Update bot status task
        addRepeatTask(() -> {
            final var text = (String) bot.getConfiguration().get("bot.status");
            if (text != null) {
                controlPanel.get().setStatusText(text);
            }
        });
    }

    @InternalAPI
    public void addRepeatTask(@NonNull Runnable runnable) {
        timeline.getKeyFrames().add(new KeyFrame(Duration.ZERO, e -> runnable.run()));
    }

    @Override
    @InternalAPI
    public ObjectProperty<? extends Node> botInterfaceProperty() {
        return controlPanel;
    }

    public static void setStatus(@NonNull String status) {
        final var bot = Environment.getBot();
        if (bot != null) {
            setStatus(bot, status);
        }
    }

    public static void setStatus(@NonNull AbstractBot bot, @NonNull String status) {
        bot.getConfiguration().put("bot.status", status);
    }

}
