package com.runemate.ui.control;

import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.ui.*;
import com.runemate.ui.binding.*;
import com.runemate.ui.tracker.*;
import java.net.*;
import java.util.*;
import javafx.application.*;
import javafx.embed.swing.*;
import javafx.fxml.*;
import javafx.scene.image.*;
import javafx.scene.layout.*;
import javafx.scene.text.*;

public class TotalLootControl extends HBox implements Initializable {

    private static final int COINS = 995;


    @FXML
    private Text hourlyProfitText;

    @FXML
    private ImageView imageView;

    @FXML
    private Text profitText;

    private final DefaultUI parent;
    private final InventoryTracker tracker;

    public TotalLootControl(final DefaultUI parent, final InventoryTracker tracker) {
        this.parent = parent;
        this.tracker = tracker;
        FXUtil.loadFxml(this, "/fxml/total_loot_tracker.fxml");
    }

    @Override
    public void initialize(final URL url, final ResourceBundle resourceBundle) {
        parent.bot().getPlatform().invokeLater(() -> {
            //Create a dummy SpriteItem so we can get the item image
            final var item = new SpriteItem(COINS, 10000);
            final var image = item.getImage().get(10000);
            if (image != null) {
                Platform.runLater(() -> imageView.setImage(SwingFXUtils.toFXImage(image, null)));
            }
        });

        profitText.textProperty().bind(new MetricBinding(tracker.getProfit()));
        hourlyProfitText.textProperty().bind(new MetricBinding(new RateBinding(tracker.getProfit(), parent.runtimeProperty())));
    }
}
