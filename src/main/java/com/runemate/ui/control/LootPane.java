package com.runemate.ui.control;

import com.runemate.game.internal.*;
import com.runemate.ui.*;
import com.runemate.ui.tracker.*;
import javafx.application.*;
import javafx.collections.*;
import javafx.geometry.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import lombok.*;
import lombok.experimental.*;

@InternalAPI
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class LootPane extends TitledPane {

    DefaultUI ui;
    VBox container;

    public LootPane(@NonNull final DefaultUI ui) {
        this.ui = ui;
        final var tracker = new InventoryTracker(ui.bot());
        tracker.getItems().addListener((MapChangeListener<Integer, InventoryTracker.ItemTracker>) change -> {
            if (change.wasAdded()) {
                addTracker(change.getValueAdded());
            }
        });

        container = new VBox();
        container.setFillWidth(true);
        container.setSpacing(12);
        container.setPadding(new Insets(12));

        setText("Loot");
        setContent(container);
        setExpanded(false);

        container.getChildren().add(new TotalLootControl(ui, tracker));
    }

    private void addTracker(@NonNull InventoryTracker.ItemTracker tracker) {
        synchronized (container) {
            Platform.runLater(() -> {
                ui.bot().getLogger().info("[LootTracker] Adding tracker for " + tracker.getName());
                container.getChildren().add(new ItemControl(ui, tracker));
            });
        }
    }
}
