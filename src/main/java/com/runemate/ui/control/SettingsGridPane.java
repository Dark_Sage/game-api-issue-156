package com.runemate.ui.control;

import com.google.common.base.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.ui.*;
import com.runemate.ui.setting.descriptor.open.*;
import com.runemate.ui.setting.open.*;
import java.util.*;
import javafx.geometry.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class SettingsGridPane extends GridPane {

    private final DefaultUI parent;

    public SettingsGridPane(DefaultUI parent) {
        this.parent = parent;
        setAlignment(Pos.TOP_LEFT);
        setGridLinesVisible(false);
        setPadding(new Insets(4, 4, 4, 24));
        setVgap(8);
        setHgap(8);

        final var textConstraints = new ColumnConstraints();
        textConstraints.setHalignment(HPos.RIGHT);
        textConstraints.setFillWidth(true);

        final var valueConstraints = new ColumnConstraints();
        valueConstraints.setHalignment(HPos.LEFT);
        valueConstraints.setFillWidth(true);

        getColumnConstraints().addAll(textConstraints, valueConstraints);
    }

    public void addItems(DefaultUI parent, SettingsManager manager, SettingsDescriptor settings, List<SettingDescriptor> items) {
        for (int row = 0; row < items.size(); row++) {
            final var item = items.get(row);
            final var label = new Label(item.title());

            //TODO Enum, List etc
            final Control control;
            if (item.type() == boolean.class) {
                control = SettingControlFactory.createCheckbox(manager, settings, item);
            } else if (item.type() == int.class) {
                control = SettingControlFactory.createIntSpinner(manager, settings, item);
            } else if (item.type() == double.class) {
                control = SettingControlFactory.createDoubleSpinner(manager, settings, item);
            } else if (item.type() == String.class) {
                control = SettingControlFactory.createTextField(manager, settings, item);
            } else if (item.type() == Coordinate.class) {
                control = SettingControlFactory.createPlayerPositionButton(parent.bot().getPlatform(), manager, settings, item);
            } else if (((Class<?>) item.type()).isEnum()) {
                control = SettingControlFactory.createEnumComboBox(manager, settings, item);
            } else {
                control = null;
            }

            if (control == null) {
                Environment.getLogger()
                    .warn("[SettingsManager] Unknown setting type: "
                        + item.type().getTypeName()
                        + " @ "
                        + settings.group().group()
                        + "."
                        + item.key());
                continue;
            }

            if (!Strings.isNullOrEmpty(item.setting().description())) {
                label.setTooltip(new Tooltip(item.setting().description()));
                control.setTooltip(new Tooltip(item.setting().description()));
            }
            add(label, 0, row);
            add(control, 1, row);
        }
    }
}
