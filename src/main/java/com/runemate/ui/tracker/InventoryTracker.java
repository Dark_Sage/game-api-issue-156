package com.runemate.ui.tracker;

import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.net.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.listeners.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.concurrent.*;
import javafx.application.*;
import javafx.beans.binding.*;
import javafx.beans.property.*;
import javafx.beans.value.*;
import javafx.collections.*;
import lombok.*;
import lombok.experimental.*;
import org.jetbrains.annotations.*;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class InventoryTracker implements InventoryListener, EquipmentListener {

    AbstractBot bot;
    ObservableMap<Integer, ItemTracker> items = FXCollections.observableMap(new ConcurrentHashMap<>());
    IntegerProperty profit = new SimpleIntegerProperty(0);

    public InventoryTracker(final AbstractBot bot) {
        this.bot = bot;
        bot.getEventDispatcher().addListener(this);
    }

    @Nullable
    public synchronized ItemTracker getItem(int id) {
        var def = ItemDefinition.get(id);
        if (def == null) {
            return null;
        }
        if (def.isNoted()) {
            id = def.getUnnotedId();
            def = ItemDefinition.get(id);
            if (def == null) {
                return null;
            }
        }
        var item = items.get(id);
        if (item == null) {
            final var lookup = def.isTradeable() ? GrandExchange.lookup(id) : null;
            item = new ItemTracker(id, def.getName(), lookup == null ? def.getShopValue() : lookup.getPrice());
            items.put(id, item);
        }
        return item;
    }

    @Override
    public void onItemEquipped(final ItemEvent event) {
        //Inverse of inventory operations, so we don't have -1 when we equip an item
        final var item = getItem(event.getItem().getId());
        if (item != null) {
            update(item.quantity, event.getQuantityChange());
            update(profit, item.value * event.getQuantityChange());
        }
    }

    @Override
    public void onItemUnequipped(final ItemEvent event) {
        //Inverse of inventory operations, so we don't have +1 when we un-equip an item
        final var item = getItem(event.getItem().getId());
        if (item != null) {
            update(item.quantity, -event.getQuantityChange());
            update(profit, item.value * -event.getQuantityChange());
        }
    }

    @Override
    public void onItemAdded(final ItemEvent event) {
        final var item = getItem(event.getItem().getId());
        if (item != null) {
            update(item.quantity, event.getQuantityChange());
            update(profit, item.value * event.getQuantityChange());
        }
    }

    @Override
    public void onItemRemoved(final ItemEvent event) {
        final var item = getItem(event.getItem().getId());
        if (item != null) {
            update(item.quantity, -event.getQuantityChange());
            update(profit, item.value * -event.getQuantityChange());
        }
    }

    private void update(@NonNull WritableNumberValue expr, int change) {
        final var current = expr.getValue().intValue();
        Platform.runLater(() -> expr.setValue(current + change));
    }

    @Value
    public static class ItemTracker {

        int id;
        String name;
        int value;
        IntegerProperty quantity = new SimpleIntegerProperty(0);
        BooleanBinding showing = quantity.isNotEqualTo(0);
    }
}
