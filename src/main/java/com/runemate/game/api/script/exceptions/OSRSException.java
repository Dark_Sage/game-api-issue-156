package com.runemate.game.api.script.exceptions;

public class OSRSException extends RuntimeException {
    public OSRSException(String owner, String name) {
        super(owner + '#' + name + " is unsupported on OSRS");
    }

    public OSRSException() {
        super("Invoked method is unsupported on OSRS.");
    }
}
