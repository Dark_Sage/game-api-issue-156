package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.local.hud.*;
import lombok.*;

@Value
@ToString(onlyExplicitlyIncluded = true)
public class MenuInteractionEvent implements Event {

    @ToString.Include(rank = 5)
    int param1;
    @ToString.Include(rank = 6)
    int param2;
    @ToString.Include(rank = 4)
    int opcode;
    @ToString.Include(rank = 3)
    long param0;
    @ToString.Include(rank = 2)
    String action;
    @ToString.Include(rank = 1)
    String target;
    @ToString.Include(rank = 7)
    int mouseX;
    @ToString.Include(rank = 8)
    int mouseY;

    @Getter(lazy = true)
    MenuItem.Type targetType = MenuItem.Type.getByOpcode(opcode);

    @Getter(lazy = true)
    Interactable targetEntity = getTargetType().resolve(opcode, param0, param1, param2);

    @Deprecated
    public MenuItem.Type getType() {
        return getTargetType();
    }

    @Deprecated
    public String getOption() {
        return getAction();
    }

    @Deprecated
    public String getTitle() {
        return getTarget();
    }
}
