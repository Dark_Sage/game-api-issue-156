package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import lombok.*;

@Value
public class MessageEvent implements Event {
    Chatbox.Message.Type type;
    String sender;
    String message;

    @Deprecated
    public String getSender() {
        return getSpeaker();
    }

    //In what world is this a better name than getSender()?
    public String getSpeaker() {
        return JagTags.remove(sender);
    }

    public String getMessage() {
        return JagTags.remove(message);
    }

    public boolean isPlayerModerator() {
        return sender.startsWith("<img=0>");
    }

    public boolean isJagexModerator() {
        return sender.startsWith("<img=1>");
    }

    public Chatbox.Message.Type getType() {
        return type;
    }
}
