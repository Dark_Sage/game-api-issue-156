package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

public interface GroundItemListener extends EventListener {
    default void onGroundItemSpawned(GroundItemSpawnedEvent event) {
    }
}
