package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

public interface MenuInteractionListener extends EventListener {
    void onInteraction(MenuInteractionEvent event);
}
