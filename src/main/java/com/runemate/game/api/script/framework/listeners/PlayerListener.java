package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.EventListener;

public interface PlayerListener extends EventListener {

    default void onPlayerHitsplat(HitsplatEvent event) {
    }

    default void onPlayerMoved(PlayerMovementEvent event) {
    }

    //TODO change to CombatInfo change
    default void onPlayerDeath(DeathEvent event) {
    }

    default void onPlayerAnimationChanged(AnimationEvent event) {
    }

    default void onPlayerTargetChanged(TargetEvent event) {
    }

}
