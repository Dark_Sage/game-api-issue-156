package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

public interface VarpListener extends EventListener {
    void onValueChanged(VarpEvent event);
}
