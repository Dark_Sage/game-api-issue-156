package com.runemate.game.api.osrs.net;

import com.runemate.io.*;
import java.io.*;
import java.net.*;
import java.util.regex.*;

/**
 * For use getting the prices of items in dead man mode.
 */
public final class Zybez {
    private static final String API_URL_BASE =
        "http://forums.zybez.net/runescape-2007-prices/api/item/";
    private static final Pattern AVERAGE_PATTERN = Pattern.compile("average\":");

    private Zybez() {
    }

    public static int getAveragePrice(int itemId) {
        try (IOTunnel tunnel = new IOTunnel(new URL(API_URL_BASE + itemId).openStream())) {
            String page = new String(tunnel.readAsArray());
            if (page.contains("average\":")) {
                return (int) Double.parseDouble(AVERAGE_PATTERN.split(page)[1].split(",")[0]);
            } else {
                System.err.println("[Debug] Unable to get the price of " + itemId
                    + " because we didn't receive the expected response.");
            }
        } catch (IOException ioe) {
            String className = ioe.getClass().getSimpleName();
            System.err.println("[Debug] Unable to get the price of " + itemId + " because of " + (
                className.matches("^[AEIOU].*") ? "an " : "a "
            ) + className);
        }
        return -1;
    }
}