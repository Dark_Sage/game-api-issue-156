package com.runemate.game.api.osrs.region;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.osrs.entities.*;
import java.util.*;
import java.util.function.*;

public class OSRSSpotAnimations {

    public static LocatableEntityQueryResults<SpotAnimation> getLoaded(
        Predicate<SpotAnimation> filter
    ) {
        final ArrayList<SpotAnimation> spotAnimationEntities = new ArrayList<>(4);
        for (final long uid : OpenRegion.getLoadedSpotAnimations()) {
            SpotAnimation animation = new OSRSSpotAnimation(uid);
            if (filter == null || filter.test(animation)) {
                spotAnimationEntities.add(animation);
            }
        }
        return new LocatableEntityQueryResults<>(spotAnimationEntities);
    }
}
