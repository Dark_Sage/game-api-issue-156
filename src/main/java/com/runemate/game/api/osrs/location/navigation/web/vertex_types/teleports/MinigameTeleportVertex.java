package com.runemate.game.api.osrs.location.navigation.web.vertex_types.teleports;

import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.teleports.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import java.io.*;
import java.util.*;
import lombok.*;

public class MinigameTeleportVertex extends TeleportVertex {
    private String minigameName;

    public MinigameTeleportVertex(
        final Coordinate destination, String minigameName,
        final Collection<WebRequirement> requirements
    ) {
        super(destination, requirements);
        this.minigameName = minigameName;
    }

    public MinigameTeleportVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(minigameName);
    }

    @Override
    public boolean step() {
        for (MinigameTeleport teleport : MinigameTeleport.values()) {
            if (minigameName.equalsIgnoreCase(teleport.getName())) {
                return teleport.activate();
            }
        }
        return false;
    }

    @Override
    public int getOpcode() {
        return 18;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeUTF(this.minigameName);
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        this.minigameName = stream.readUTF();
        return true;
    }

    @Override
    public String toString() {
        Coordinate position = getPosition();
        return "MinigameTeleportVertex(minigame: " + minigameName + ", destination: " +
            position.getX() + ", " + position.getY() + ", " + position.getPlane() + ')';
    }
}
