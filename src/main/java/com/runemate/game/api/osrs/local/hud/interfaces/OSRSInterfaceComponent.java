package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.script.exceptions.*;
import com.runemate.rmi.*;
import java.util.*;
import java.util.function.*;

public final class OSRSInterfaceComponent extends InterfaceComponent {
    public OSRSInterfaceComponent(InterfaceContainer container, long uid, int index) {
        super(container, uid, index, false);
    }

    public OSRSInterfaceComponent(
        InterfaceContainer container, InterfaceComponent parent, long uid,
        int index
    ) {
        super(container, parent, uid, index, false);
    }

    @Override
    public InterfaceComponent getChild(int index) {
        if (getParentComponent() == null && Type.CONTAINER.equals(getType())) {
            long[] children = component().getChildUids();
            if (children != null && index < children.length) {
                long component_uid = children[index];
                if (BridgeUtil.getFieldInstance(component_uid) != 0) {
                    return new OSRSInterfaceComponent(container, this, component_uid, index);
                }
            }
        }
        return null;
    }

    @Override
    public InterfaceComponent getChild(Predicate<InterfaceComponent> predicate) {
        if (getParentComponent() == null && Type.CONTAINER.equals(getType())) {
            long[] children = component().getChildUids();
            if (children != null) {
                for (int index = 0, length = children.length; index < length; ++index) {
                    long component_uid = children[index];
                    if (BridgeUtil.getFieldInstance(component_uid) != 0) {
                        OSRSInterfaceComponent component =
                            new OSRSInterfaceComponent(container, this, component_uid, index);
                        if (predicate == null || predicate.test(component)) {
                            return component;
                        }
                    }
                }
            }
        }
        return null;
    }

    @Override
    public List<InterfaceComponent> getChildren() {
        if (getParentComponent() == null && Type.CONTAINER.equals(getType())) {
            long[] children = component().getChildUids();
            if (children != null) {
                List<InterfaceComponent> components = new ArrayList<>(children.length);
                for (int index = 0, length = children.length; index < length; ++index) {
                    long component_uid = children[index];
                    if (BridgeUtil.getFieldInstance(component_uid) != 0) {
                        components.add(
                            new OSRSInterfaceComponent(container, this, component_uid, index));
                    }
                }
                return components;
            }
        }
        return Collections.emptyList();
    }

    @Override
    public int getContainedItemQuantity() {
        return component().getItemQuantity();
    }


    @Override
    public int getProjectedEntityAnimationId() {
        return component().getAnimationId();
    }

    @Override
    protected List<Object> getMouseHoveringEventVariables(Predicate<Object> condition) {
        throw new OSRSException("InterfaceComponent", "getMouseHoveringEventVariables");
    }

    private String getButtonAction() {
        String name = component().getButtonAction();
        return name != null && !name.isEmpty() ? name : null;
    }

    @Override
    public List<String> getActions() {
        List<String> actions = super.getActions();
        if (actions.isEmpty()) {
            String buttonAction = getButtonAction();
            if (buttonAction != null && !buttonAction.equals("Ok")) {
                return Collections.singletonList(buttonAction);
            }
        }
        return actions;
    }
}
