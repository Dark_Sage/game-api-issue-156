package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.script.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;

public class MakeAllInterface {
    private static final int CONTAINER = 270;
    private static final Predicate<InterfaceComponent> HAS_TEXT_PREDICATE = ic -> {
        String text = ic.getText();
        return text != null && !text.isEmpty();
    };
    private static final Predicate<InterfaceComponent> HAS_NAME_PREDICATE = ic -> {
        String name = ic.getName();
        return name != null && !name.isEmpty();
    };

    public static boolean isOpen() {
        return !Interfaces.newQuery().containers(CONTAINER).grandchildren(false)
            .types(InterfaceComponent.Type.LABEL).textContains("Choose a quantity").results()
            .isEmpty();
    }

    /**
     * Gets the selected quantity, where 0 represents "All" and -1 represents "X" (only selected when the enter quantity dialog is shown)
     *
     * @return The selected quantity if the interface is available, where 0 represents "All". Returns -1 if the interface isn't available or if the interface isn't visible because the enter quantity dialog is displayed.
     */
    public static int getSelectedQuantity() {
        for (InterfaceComponent quantityButton : getQuantityButtons()) {
            if (quantityButton.getActions().isEmpty() && quantityButton.getChildQuantity() == 10) {
                InterfaceComponent childButton = quantityButton.newQuery().containers(CONTAINER)
                    .types(InterfaceComponent.Type.LABEL).filter(HAS_TEXT_PREDICATE).results()
                    .first();
                if (childButton != null) {
                    String text = childButton.getText();
                    if (text != null) {
                        switch (text) {
                            case "All":
                                return 0;
                            case "X":
                                return -1;
                            default:
                                return Integer.valueOf(text);
                        }
                    }
                }
            }
        }
        return -1;
    }

    /**
     * Sets the selected quantity where a value of 0 represents selecting "All"
     *
     * @param quantity 0 for "All", otherwise the desired value.
     * @return true if the desired quantity is set
     */
    public static boolean setSelectedQuantity(int quantity) {
        if (getSelectedQuantity() == quantity) {
            return true;
        }
        InterfaceComponent otherQuantityButton = null;
        InterfaceComponent allButton = null;
        InterfaceComponent exactQuantityButton = null;
        for (InterfaceComponent quantityButton : getQuantityButtons()) {
            for (String action : quantityButton.getActions()) {
                if (action.equals("Other quantity")) {
                    otherQuantityButton = quantityButton;
                } else if (action.equals("All")) {
                    allButton = quantityButton;
                } else if (action.equals(Integer.toString(quantity))) {
                    exactQuantityButton = quantityButton;
                }
            }
        }
        if (quantity == 0 && allButton != null) {
            if (allButton.click()) {
                return Execution.delayUntil(
                    () -> MakeAllInterface.getSelectedQuantity() == quantity, 600, 1200);
            }
        } else if (exactQuantityButton != null) {
            if (exactQuantityButton.click()) {
                return Execution.delayUntil(
                    () -> MakeAllInterface.getSelectedQuantity() == quantity, 600, 1200);
            }
        } else if (otherQuantityButton != null) {
            if (otherQuantityButton.click()
                && Execution.delayUntil(InputDialog::isOpen, 600, 1200)) {
                return InputDialog.enterAmount(quantity);
            }
        }
        return false;
    }

    public static List<String> getSelectableItems() {
        InterfaceComponentQueryResults buttons = getItemButtons();
        List<String> items = new ArrayList<>(buttons.size());
        for (InterfaceComponent button : buttons) {
            items.add(button.getName());
        }
        return items;
    }

    /**
     * The available quantities where 0 represents "all" and -1 represents "X"
     *
     * @return a non-null Collection of Integers
     */
    public static Collection<Integer> getSelectableQuantities() {
        InterfaceComponentQueryResults buttons = getQuantityButtons();
        int selectedQuantity = getSelectedQuantity();
        Set<Integer> items = new HashSet<>(buttons.size());
        if (selectedQuantity != -1) {
            items.add(selectedQuantity);
        }
        for (InterfaceComponent quantityButton : buttons) {
            for (String action : quantityButton.getActions()) {
                switch (action) {
                    case "Other quantity":
                        items.add(-1);
                        break;
                    case "All":
                        items.add(0);
                        break;
                    default:
                        try {
                            items.add(Integer.valueOf(action));
                        } catch (NumberFormatException nfe) {
                            System.err.println("[MakeAllInterface] " + nfe.getMessage() +
                                " while parsing the value of \"" + action + "\" as an integer");
                        }
                        break;
                }
            }
        }
        return items;
    }

    /**
     * Selects the item with the given name if available using player sense to decide if hotkeys or clicking should be used.
     *
     * @param name
     * @return true if the given item was selected.
     */
    public static boolean selectItem(String name) {
        return selectItem(name, PlayerSense.getAsBoolean(PlayerSense.Key.USE_MISC_HOTKEYS));
    }

    /**
     * Selects the item with the given name if available using player sense to decide if hotkeys or clicking should be used.
     *
     * @param name
     * @return true if the given item was selected.
     */
    public static boolean selectItem(Pattern name) {
        return selectItem(name, PlayerSense.getAsBoolean(PlayerSense.Key.USE_MISC_HOTKEYS));
    }

    /**
     * Selects the item with the given name if available using hotkeys if specified, otherwise clicking.
     *
     * @param name
     * @return true if the given item was selected.
     */
    public static boolean selectItem(String name, boolean useHotkey) {
        InterfaceComponentQueryResults buttons = getItemButtons();
        for (int hotkey = 1; hotkey <= buttons.size(); ++hotkey) {
            InterfaceComponent button = buttons.get(hotkey - 1);
            if (name.equals(button.getName())) {
                return useHotkey ? Keyboard.typeKey(Integer.toString(hotkey)) : button.interact((String) null, name);
            }
        }
        return false;
    }

    public static boolean selectItem(Pattern name, boolean useHotkey) {
        InterfaceComponentQueryResults buttons = getItemButtons();
        for (int hotkey = 1; hotkey <= buttons.size(); ++hotkey) {
            InterfaceComponent button = buttons.get(hotkey - 1);
            String buttonName = button.getName();
            if (buttonName != null && name.matcher(buttonName).find()) {
                return useHotkey ? Keyboard.typeKey(Integer.toString(hotkey)) : button.interact((String) null, name);
            }
        }
        return false;
    }

    private static InterfaceComponentQueryResults getQuantityButtons() {
        return Interfaces.newQuery().containers(CONTAINER).types(InterfaceComponent.Type.CONTAINER)
            .grandchildren(false).widths(30, 35).heights(30).results();
    }

    private static InterfaceComponentQueryResults getItemButtons() {
        return Interfaces.newQuery().containers(CONTAINER).types(InterfaceComponent.Type.CONTAINER)
            .grandchildren(false).heights(72, 75).filter(HAS_NAME_PREDICATE).results();
    }
}
