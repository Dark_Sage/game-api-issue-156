package com.runemate.game.api.osrs.entities;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.osrs.region.*;
import java.util.*;
import javax.annotation.*;

public final class OSRSGameObject extends OSRSEntity implements GameObject {
    private final String resolvedClassName;
    private final Coordinate position;
    private int id;
    private boolean modelLoaded;
    private Type type;

    public OSRSGameObject(long uid, @Nonnull String type, int id, @Nonnull Coordinate position) {
        super(uid);
        this.id = id;
        this.position = position;
        this.resolvedClassName = type;
    }

    @Override
    public int getAnimationId() {
        long animableObject1;
        long animableObject2;
        switch (getType()) {
            case BOUNDARY:
            case WALL_DECORATION:
                animableObject1 = OpenGameObject.dynamicModelA(resolvedClassName, uid);
                animableObject2 = OpenGameObject.dynamicModelB(resolvedClassName, uid);
                break;
            case PRIMARY:
            case GROUND_DECORATION:
                animableObject1 = OpenGameObject.dynamicModel(resolvedClassName, uid);
                animableObject2 = 0;
                break;
            default:
                animableObject1 = 0;
                animableObject2 = 0;
        }
        if (animableObject1 != 0 || animableObject2 != 0) {
            if (animableObject1 != 0 && OpenGameObject.isAnimableObject(animableObject1)) {
                return (int) OpenGameObject.getAnimationSequence(animableObject1);
            }
            if (animableObject2 != 0 && OpenGameObject.isAnimableObject(animableObject2)) {
                return (int) OpenGameObject.getAnimationSequence(animableObject2);
            }
        }
        return -1;
    }


    @Override
    public Model getModel() {
        if (forcedModel != null) {
            return forcedModel;
        }
        if (cacheModel != null && cacheModel.isValid()) {
            return cacheModel;
        }
        if (!modelLoaded) {
            modelLoaded = true;
            CacheObjectDefinition.Extended definition =
                (CacheObjectDefinition.Extended) getDefinition();
            if (definition != null) {
                final CacheObjectDefinition.Extended local_state =
                    (CacheObjectDefinition.Extended) definition.getLocalState();
                if (local_state != null) {
                    definition = local_state;
                }
                int[][] raw_appearance = definition.getModelIds();
                if (raw_appearance != null && raw_appearance.length == 1) {
                    int[] appearance = raw_appearance[0];
                    if (appearance.length > 0) {
                        List<CacheModel> components = new ArrayList<>();
                        int specializedType = getSpecializedTypeIndicator();
                        int[] modelTypes = definition.getModelTypes();
                        if (modelTypes == null) {
                            if(specializedType == 10 || specializedType == 11){
                                for (int modelID : appearance) {
                                    components.add(CacheModel.load(modelID));
                                }
                            } else {
                                //For compatibility's sake. Client code simply returns null at this point
                                components.add(CacheModel.load(appearance[0]));
                            }
                        } else {
                            int index = -1;
                            for (int i = 0; i < modelTypes.length; ++i) {
                                if (modelTypes[i] == specializedType) {
                                    index = i;
                                    break;
                                }
                            }
                            if (index != -1 && index < appearance.length) {
                                components.add(CacheModel.load(appearance[index]));
                            } else if (backupModel == null && appearance.length == 1) {
                                components.add(CacheModel.load(appearance[0]));
                            }
                        }

                        //Null-check the models
                        components.removeIf(Objects::isNull);

                        if (!components.isEmpty()) {
                            int heightOffset =
                                OpenGameObject.dynamicHeight(resolvedClassName, uid);
                            if (heightOffset == -1) {
                                heightOffset = 0;
                            }
                            CompositeCacheModel model =
                                new CompositeCacheModel(this, /*heightOffset*/0,
                                    components
                                );
                            model.setScale(definition.getModelXScale(), definition.getModelYScale(),
                                definition.getModelZScale()
                            );
                            model.setTranslation(
                                definition.getModelXTranslation(),
                                definition.getModelYTranslation(),
                                definition.getModelZTranslation()
                            );
                            return cacheModel = model;
                        }
                    }
                }
            }
        }
        return backupModel;
    }


    @Override
    public byte getSpecializedTypeIndicator() {
        //TODO It appears placement is always -1 when the object isn't loaded. This could result in a faster isValid method. Analyze the data to confirm.
        return (byte) (OpenGameObject.dynamicPlacement(resolvedClassName, uid) & 0x3f);
    }

    @Override
    public int getId() {
        if (id != -1) {
            return id;
        }
        final long hash = getInnerHash();
        if (hash != -1) {
            return id = (int) (hash >>> 17 & 4294967295L);
        }
        return -1;
    }

    @Override
    public Type getType() {
        if (type == null) {
            switch (resolvedClassName) {
                case "BoundaryObject":
                    type = Type.BOUNDARY;
                    break;
                case "EventObject":
                    type = Type.PRIMARY;
                    break;
                case "FloorObject":
                    type = Type.GROUND_DECORATION;
                    break;
                case "WallObject":
                    type = Type.WALL_DECORATION;
                    break;
                default:
                    System.err.println(
                        "[OSRS GameObject] Unknown object type \"" + resolvedClassName + "\"");
                    type = Type.UNKNOWN;
                    break;
            }
        }
        return type;
    }

    @Override
    public GameObjectDefinition getDefinition() {
        int id = getId();
        if (id < 0) {
            return null;
        }
        return GameObjectDefinition.get(id);
    }


    private long getInnerHash() {
        return OpenGameObject.dynamicHash(resolvedClassName, uid);
    }

    @Override
    @Nonnull
    public Coordinate getPosition(Coordinate regionBase) {
        return position;
    }


    @Override
    public Coordinate.HighPrecision getHighPrecisionPosition(Coordinate regionBase) {
        final String xKey = resolvedClassName + ".regionX";
        final String yKey = resolvedClassName + ".regionY";
        if (OpenClient.isMapped(xKey) && OpenClient.isMapped(yKey)) {
            int hpRegionX = OpenGameObject.dynamicRegionX(resolvedClassName, uid);
            if (hpRegionX == -1) {
                hpRegionX = 0;
            }
            int hpRegionY = OpenGameObject.dynamicRegionY(resolvedClassName, uid);
            if (hpRegionY == -1) {
                hpRegionY = 0;
            }
            if (regionBase == null) {
                regionBase = OSRSRegion.getBase(0);
            }
            int xInset = 0;
            int yInset = 0;
            byte type = getSpecializedTypeIndicator();
            if (type == 4) {
                //A basic wall object, no insets
            } else if (type == 5) {
                int inset = 16;
                int orientation = getDirection().ordinal() & 0x3;
                xInset = (new int[] { 1, 0, -1, 0 }[orientation] * inset);
                yInset = (new int[] { 0, -1, 0, 1 }[orientation] * inset);
            } else if (type == 6) {
                int inset = 8;
                int orientation = getDirection().ordinal() & 0x3;
                xInset = (new int[] { 1, -1, -1, 1 }[orientation] * inset);
                yInset = (new int[] { -1, -1, 1, 1 }[orientation] * inset);
            } else if (type == 7) {
                //Something to do with orientation
            } else if (type == 8) {
                //TODO I give up for now
                /*int inset = 32;
                int orientation = getDirection().ordinal() + 2 & 0x3;
                xInset = (new int[]{1, -1, -1, 1}[orientation] * inset);
                yInset = (new int[]{-1, -1, 1, 1}[orientation] * inset);*/
            }
            final Coordinate.HighPrecision hp = regionBase.getHighPrecisionPosition();
            return new Coordinate.HighPrecision(
                hp.getX() + hpRegionX + xInset,
                hp.getY() + hpRegionY + yInset,
                position.getPlane()
            );
        }
        int x = position.getX() << 7;
        int y = position.getY() << 7;
        final Area.Rectangular area = getArea(regionBase);
        if (area != null) {
            x += (area.getWidth() << 6);
            y += (area.getHeight() << 6);
        }
        return new Coordinate.HighPrecision(x, y, position.getPlane());
    }

    @Nullable
    @Override
    public Area.Rectangular getArea(Coordinate regionBase) {
        GameObjectDefinition def = getDefinition();
        if (def != null) {
            GameObjectDefinition transformation = def.getLocalState();
            if (transformation != null) {
                def = transformation;
            }
            int width = def.getWidth();
            int height = def.getHeight();
            /*if (width > 1 || height > 1) {
                byte specializedTypeIndicator = getSpecializedTypeIndicator();
                //only types 10 and 11 appear to be having larger areas? type 4 does in basement of wizards tower on 2 objects but they're not within the area it seems....
                if (specializedTypeIndicator != 10 && specializedTypeIndicator != 11) {
                    System.out.println("OSRS GameObject of type " + specializedTypeIndicator + " named " + def.getName() + " (" + getId() + ") has a width of " + width + " and height of " + height);
                }
            }*/
            if (width != height) {
                Direction direction = getDirection();
                if (direction == null) {
                    return null;
                }
                width = direction.ordinal() % 2 == 0 ? def.getWidth() : def.getHeight();
                height = direction.ordinal() % 2 == 0 ? def.getHeight() : def.getWidth();
            }
            final Coordinate root = getPosition(regionBase);
            return new Area.Rectangular(root, root.derive(width - 1, height - 1));
        }
        return super.getArea(regionBase);
    }

    @Override
    public String toString() {
        String name = null;
        GameObjectDefinition definition = getDefinition();
        if (definition != null) {
            name = definition.getName();
            if ("null".equals(name)) {
                name = null;
            }
            if (name == null) {
                definition = definition.getLocalState();
                if (definition != null) {
                    name = definition.getName();
                    if ("null".equals(name)) {
                        name = null;
                    }
                }
            }
        }
        final Coordinate position = getPosition();
        return (name != null ? name : "#" + getId()) + " [" + (
            position != null ?
                position.getX() + ", " + position.getY() + ", " + position.getPlane() : ""
        ) + ']';
    }

    @Override
    public int getHighPrecisionOrientation() {
        int orientationInset = 0;
        byte type = getSpecializedTypeIndicator();
        Direction direction = getDirection();
        if (type == 8) {
            //TODO I give up for now
            //orientationInset = -256 * 3;
        } else if (type == 11) {
            orientationInset = 256;
        }

        return direction != null ? direction.getHighPrecisionOrientation(false) + orientationInset :
            0;
    }

    @Override
    public Direction getDirection() {
        //placement is -1 when the object isn't loaded it appears!
        int placement = OpenGameObject.dynamicPlacement(resolvedClassName, uid);
        int cardinalDirection = placement >> 6;
        if (cardinalDirection < 0 || cardinalDirection >= Direction.values().length) {
            return null;
        }
        return Direction.values()[cardinalDirection];
    }

    @Deprecated
    @Override
    public Set<Integer> getSpotAnimationIds() {
        return Collections.emptySet();
    }

    @Override
    public int getOrientationAsAngle() {
        Direction direction = getDirection();
        return direction != null ? direction.getAngle() : 0;
    }

    @Override
    public boolean isValid() {
        if (super.isValid()) {
            Area.Rectangular area = getArea();
            if (area != null && area.isLoaded()) {
                return !GameObjects.getLoadedWithin(area, id).isEmpty();
            }
        }
        return false;
    }
}
