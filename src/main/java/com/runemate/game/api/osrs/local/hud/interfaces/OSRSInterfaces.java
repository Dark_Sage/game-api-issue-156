package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.rmi.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.function.*;

public final class OSRSInterfaces {
    private OSRSInterfaces() {
    }

    public static List<InterfaceContainer> getLoaded(
        final Predicate<InterfaceContainer> predicate
    ) {
        final long[] interface_uids = OpenInterfaceContainer.getLoaded();
        final ArrayList<InterfaceContainer> containers = new ArrayList<>(interface_uids.length);
        for (int index = 0; index < interface_uids.length; ++index) {
            final long interface_uid = interface_uids[index];
            if (interface_uid != 0) {
                containers.add(new OSRSInterfaceContainer(interface_uid, index));
            }
        }
        containers.trimToSize();
        return Parallelize.collectToList(containers, predicate);
    }

    public static InterfaceComponent getAt(final int containerIndex, final int componentIndex) {
        InterfaceContainer iface = getAt(containerIndex);
        if (iface != null) {
            return iface.getComponent(componentIndex);
        }
        return null;
    }

    public static InterfaceComponent getAt(
        final int containerIndex, final int componentIndex,
        final int subComponentIndex
    ) {
        final InterfaceComponent component = getAt(containerIndex, componentIndex);
        if (component != null) {
            return component.getChild(subComponentIndex);
        }
        return null;
    }

    public static OSRSInterfaceContainer getAt(final int index) {
        final long internal = OpenInterfaceContainer.getAt(index);
        if (BridgeUtil.getFieldInstance(internal) == 0) {
            return null;
        }
        return new OSRSInterfaceContainer(internal, index);
    }

    public static InteractableRectangle getRootBounds(final int arrayIndex) {
        final Rectangle rect = OpenInterfaceContainer.getRootBounds(arrayIndex);
        if (rect != null) {
            return new InteractableRectangle(rect);
        }
        return null;
    }
}
