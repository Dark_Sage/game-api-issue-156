package com.runemate.game.api.osrs.region;

import com.runemate.client.game.open.*;
import com.runemate.commons.internal.scene.locations.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.osrs.entities.*;
import java.io.*;
import java.util.*;
import java.util.function.*;

public final class OSRSGameObjects {
    private static final LocatableEntityQueryResults<GameObject> EMPTY =
        new LocatableEntityQueryResults<>(Collections.emptyList(), null);

    private OSRSGameObjects() {
    }


    public static LocatableEntityQueryResults<GameObject> getLoaded(
        final Predicate<GameObject> filter
    ) {
        Object[] objectData = (Object[]) OpenRegion.getObjectsLoaded();
        if (objectData != null) {
            Set<GameObject> objects;
            if (filter != null) {
                objects = new HashSet<>(Parallelize.collect(convertToGameObjects(objectData, Region.getBase()), filter));
            } else {
                objects = new HashSet<>(convertToGameObjects(objectData, Region.getBase()));
            }
            return new LocatableEntityQueryResults<>(objects);
        }
        return EMPTY;
    }


    public static LocatableEntityQueryResults<GameObject> getLoadedOn(
        final Coordinate c,
        final Predicate<GameObject> filter
    ) {
        Coordinate base = Region.getBase();
        Object[] objectData = (Object[]) OpenRegion.getObjectsLoadedOn(
            new Serializable[] {
                new TileArea(new Tile(c.getX() - base.getX(), c.getY() - base.getY(), c.getPlane()))
            });
        if (objectData != null) {
            Set<GameObject> objects;
            if (filter != null) {
                objects = new HashSet<>(
                    Parallelize.collect(convertToGameObjects(objectData, base), filter));
            } else {
                objects = new HashSet<>(convertToGameObjects(objectData, base));
            }
            return new LocatableEntityQueryResults<>(objects);
        }
        return EMPTY;
    }


    public static LocatableEntityQueryResults<GameObject> getLoadedWithin(
        final Area area,
        final Predicate<GameObject> filter
    ) {
        Area.Rectangular rectangular = area.toRectangular();
        Coordinate bl = rectangular.getBottomLeft();
        Coordinate tr = rectangular.getTopRight();
        Coordinate base = Region.getBase();
        Object[] objectData = (Object[]) OpenRegion.getObjectsLoadedOn(new Serializable[] {
            new TileArea(
                new Tile(bl.getX() - base.getX(), bl.getY() - base.getY(), bl.getPlane()),
                new Tile(tr.getX() - base.getX(), tr.getY() - base.getY(), tr.getPlane())
            )
        });
        if (objectData != null) {
            Set<GameObject> objects;
            if (filter != null) {
                objects = new HashSet<>(
                    Parallelize.collect(convertToGameObjects(objectData, base), filter));
            } else {
                objects = new HashSet<>(convertToGameObjects(objectData, base));
            }
            return new LocatableEntityQueryResults<>(objects);
        }
        return EMPTY;
    }

    private static List<GameObject> convertToGameObjects(Object[] response, Coordinate regionBase) {
        List<GameObject> objects = new ArrayList<>(response.length / 4);
        for (int index = 0; index < response.length; index += 4) {
            int objectUid = (int) response[index];
            String objectType = (String) response[index + 1];
            int objectId = (int) response[index + 2];
            Coordinate objectPosition = new Coordinate((int) response[index + 3])
                .derive(regionBase.getX(), regionBase.getY());
            objects.add(new OSRSGameObject(objectUid, objectType, objectId, objectPosition));
        }
        return objects;
    }
}
