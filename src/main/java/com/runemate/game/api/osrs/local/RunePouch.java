package com.runemate.game.api.osrs.local;

import com.runemate.game.api.hybrid.local.*;
import javax.annotation.*;

public class RunePouch {

    private RunePouch() {
    }

    @Nullable
    public static Rune getRune(Slot slot) {
        return slot.getRune();
    }

    public static int getQuantity(Slot slot) {
        return slot.getQuantity();
    }

    public static int getQuantity(Rune rune) {
        for (final Slot slot : Slot.values()) {
            if (slot.getRune() == rune) {
                return slot.getQuantity();
            }
        }
        return 0;
    }

    public static boolean isEmpty() {
        return getEmptySlots() == 3;
    }

    public static int getEmptySlots() {
        int empty = 0;
        for (final Slot slot : Slot.values()) {
            if (slot.getQuantity() == 0) {
                empty++;
            }
        }
        return empty;
    }

    public enum Slot {
        ONE(29, 1624),
        TWO(1622, 1625),
        THREE(1623, 1626);

        private final int type;
        private final int quantity;

        Slot(int type, int quantity) {
            this.type = type;
            this.quantity = quantity;
        }

        public Rune getRune() {
            final Varbit varbit = Varbits.load(type);
            if (varbit != null) {
                final int value = varbit.getValue();
                for (final Rune rune : Rune.values()) {
                    if (rune.osrsPouchType == value) {
                        return rune;
                    }
                }
            }
            return null;
        }

        public int getQuantity() {
            final Varbit varbit = Varbits.load(quantity);
            return varbit == null ? 0 : varbit.getValue();
        }


        @Override
        public String toString() {
            return "RunePouch.Slot." + name();
        }
    }
}
