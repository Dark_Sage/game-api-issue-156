package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.rmi.*;
import java.util.*;
import java.util.function.*;

public final class OSRSInterfaceContainer extends InterfaceContainer {

    private OpenInterfaceContainer interfaceContainer;

    public OSRSInterfaceContainer(final long uid, final int index) {
        super(uid, index);
    }

    private OpenInterfaceContainer interfaceContainer() {
        if (interfaceContainer == null) {
            interfaceContainer = OpenInterfaceContainer.create(uid);
        }
        return interfaceContainer;
    }

    @Override
    public InterfaceComponent getComponent(int index) {
        final long componentUid = interfaceContainer().getComponent(index);
        if (BridgeUtil.getFieldInstance(componentUid) != 0) {
            return new OSRSInterfaceComponent(this, componentUid, index);
        }
        return null;
    }

    @Override
    public InterfaceComponentQueryResults getComponents(Predicate<InterfaceComponent> predicate) {
        long[] component_uids = interfaceContainer().getComponents();
        ArrayList<InterfaceComponent> components = new ArrayList<>(component_uids.length);
        for (int index = 0; index < component_uids.length; ++index) {
            long component_uid = component_uids[index];

            if (BridgeUtil.getFieldInstance(component_uid) != 0) {
                OSRSInterfaceComponent component =
                    new OSRSInterfaceComponent(this, component_uid, index);
                if (predicate == null || predicate.test(component)) {
                    components.add(component);
                }
            }
        }
        components.trimToSize();
        return new InterfaceComponentQueryResults(components);
    }
}
