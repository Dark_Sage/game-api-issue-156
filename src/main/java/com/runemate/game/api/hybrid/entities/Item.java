package com.runemate.game.api.hybrid.entities;

import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.entities.details.*;
import javax.annotation.*;

public interface Item extends Identifiable {

    /**
     * The quantity of the item, also known as stack size
     */
    int getQuantity();

    /**
     * The definition of the item.
     */
    @Nullable
    ItemDefinition getDefinition();
}
