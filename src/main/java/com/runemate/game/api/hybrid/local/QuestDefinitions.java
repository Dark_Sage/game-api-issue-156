package com.runemate.game.api.hybrid.local;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.api.hybrid.cache.db.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

public class QuestDefinitions {

    /**
     * Loads the QuestDefinition from the cache using the ID of the Quest.
     *
     * @param questId ID of the quest.
     * @return a QuestDefinition object when successfully read from the cache, otherwise null
     * @see Quest.OSRS#getId()
     */
    public static QuestDefinition load(int questId) {
        return loadByCacheId(getRowIdForQuest(questId));
    }

    /**
     * Loads the QuestDefinition from the cache using the ID of the dbrow that defines the quest, not to be confused with the Quest ID.
     *
     * @param rowId ID of the dbrow.
     * @return a QuestDefinition object when successfully read from the cache, otherwise null
     */
    public static QuestDefinition loadByCacheId(int rowId) {
        final var row = DBRows.load(rowId);
        return row == null ? null : createDefinition(row);
    }

    /**
     * Returns all QuestDefinitions that can be read from the cache
     *
     * @param filter
     * @return
     */
    public static List<QuestDefinition> loadAll(final Predicate<QuestDefinition> filter) {
        var stream = getRowIdMapping().values().stream()
            .map(DBRows::load)
            .filter(Objects::nonNull)
            .map(QuestDefinitions::createDefinition)
            .filter(Objects::nonNull);

        if (filter != null) {
            stream = stream.filter(filter);
        }

        return stream.collect(Collectors.toList());
    }

    public static List<QuestDefinition> loadAll() {
        return loadAll(null);
    }

    private static CacheQuestDefinition createDefinition(final DBRow row) {
        var def = new CacheQuestDefinition();
        try {
            def.decode(row);
            return def;
        } catch (Exception e) {
            e.printStackTrace();
            Environment.getLogger().warn("Error occurred when decoding QuestDefinition for row " + row.getId());
        }
        return null;
    }

    private static int getRowIdForQuest(final int questId) {
        final var tableIndex = DBTableIndexes.load(1);
        if (tableIndex != null) {
            final var mapping = tableIndex.getTupleIndexes().get(0);
            return mapping.get(questId).get(0);
        }
        return -1;
    }

    private static Map<Integer, Integer> getRowIdMapping() {
        final var tableIndex = DBTableIndexes.load(1);
        if (tableIndex != null
            && tableIndex.getTupleTypes() != null
            && tableIndex.getTupleTypes().length == 1
            && tableIndex.getTupleTypes()[0] == BaseType.INT) {
            return tableIndex.getTupleIndexes().get(0).entrySet().stream()
                .collect(Collectors.toMap(
                    k -> (int) k.getKey(),
                    v -> v.getValue().get(0)
                ));
        }
        return Collections.emptyMap();
    }
}
