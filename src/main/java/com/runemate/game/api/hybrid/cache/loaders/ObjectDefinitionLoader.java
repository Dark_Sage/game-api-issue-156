package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.cache.item.*;
import java.util.*;

public class ObjectDefinitionLoader extends SerializedFileLoader<CacheObjectDefinition> {

    public ObjectDefinitionLoader(int file) {
        super(file);
    }

    @Override
    protected CacheObjectDefinition construct(int entry, int file, Map<String, Object> arguments) {
        return new CacheObjectDefinition(file);
    }
}
