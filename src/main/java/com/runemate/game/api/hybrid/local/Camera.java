package com.runemate.game.api.hybrid.local;

import com.google.common.util.concurrent.*;
import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.projection.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.annotations.*;
import com.runemate.game.api.script.framework.*;
import java.awt.*;
import java.awt.event.*;
import java.util.concurrent.*;

public final class Camera {

    public static final ConcurrentMap<AbstractBot, ExecutorService> executorMap =
        new ConcurrentHashMap<>(5);
    public static final ConcurrentMap<AbstractBot, FutureTask<Boolean>> lastTaskMap =
        new ConcurrentHashMap<>(5);
    private static final Color NORMAL_CHAT_TEXT_COLOR = new Color(150, 150, 150);
    private final static int FIXED_MAX_JAG_ZOOM = 1448;
    private final static int FIXED_MIN_JAG_ZOOM = 181;
    private final static int RESIZABLE_MAX_JAG_ZOOM = 3190;
    private final static int RESIZABLE_MIN_JAG_ZOOM = 398;
    //Varc values were taken from CS2-Script #42 (camera_do_zoom) and confirmed in-game
    private final static int MIN_VARC_ZOOM = 128;
    private final static int MAX_VARC_ZOOM = 896;

    private Camera() {
    }

    /**
     * Gets the current camera yaw as an angle
     *
     * @return The camera yaw as an angle (0-359), or -1 if unavailable
     */
    public static int getYaw() {
        return (int) (OpenCamera.getJagYaw() / 2048.0 * 360.0);
    }

    /**
     * Gets the normalized camera pitch
     *
     * @return The camera pitch, normalized between 0 and 1, or -1 if unavailable
     */
    public static double getPitch() {
        return Math.max(
            0,
            CommonMath.round(CommonMath.normalize(OpenCamera.getJagPitch(), 128, 383), 3)
        );
    }

    /**
     * An OSRS-only method to get the camera x
     *
     * @return a value representing an axis of the camera's position.
     */
    @OSRSOnly
    public static int getX() {
        return OpenCamera.getX();
    }

    /**
     * An OSRS-only method to get the camera y
     *
     * @return a value representing an axis of the camera's position.
     */
    @OSRSOnly
    public static int getY() {
        return OpenCamera.getY();
    }

    /**
     * An OSRS-only method to get the camera z
     *
     * @return a value representing an axis of the camera's position.
     */
    @OSRSOnly
    public static int getZ() {
        return OpenCamera.getZ();
    }

    @OSRSOnly

    public static int getJagYaw() {
        return OpenCamera.getJagYaw();
    }

    @OSRSOnly

    public static int getJagPitch() {
        return OpenCamera.getJagPitch();
    }

    /**
     * Gets the zoom level on OSRS in the pure form used by the OSRS game engine.
     * Not normalized at all.
     * In resizable mode, the maximum and minimum values seem to depend on the screen height.
     * Used for projection.
     *
     * @return Zoom level
     */
    @OSRSOnly
    public static int getJagZoom() {
        return OpenCamera.getJagZoom();
    }

    /**
     * Gets the zoom level as seen from the in-game zoom setting.
     * Not normalized at all.
     * Unlike JagZoom, the minimum and maximum values do not vary with the display mode.
     *
     * @return Zoom level. From 128 (fully zoomed out) to 896 (fully zoomed in).
     */
    @OSRSOnly
    public static int getZoomSetting() {
        //Varc74 could also be used, the two values seem to be always identical
        return Varcs.getInt(73);
    }

    /**
     * Gets the normalized camera zoom between 0 and 1, osrs only
     *
     * @return Normalized zoom level
     */
    @OSRSOnly
    public static double getZoom() {
        return CommonMath.normalize(getZoomSetting(), MIN_VARC_ZOOM, MAX_VARC_ZOOM);
    }


    /**
     * @param zoom      Normalized zoom between 0.0 and 1.0
     * @param tolerance Tolerance of the provided zoom value
     */
    @OSRSOnly
    public static void setZoom(double zoom, double tolerance) {
        int zoomRange = MAX_VARC_ZOOM - MIN_VARC_ZOOM;
        setZoomSetting(MIN_VARC_ZOOM + (int) (zoom * zoomRange), (int) (tolerance * zoomRange));
    }

    /**
     * Tries to set the zoom value to the value provided.
     *
     * @param desiredZoom From 128 to 896
     * @param tolerance   Absolute tolerance
     * @return True if the final zoom was within provided tolerance, false otherwise.
     */
    @OSRSOnly
    public static boolean setZoomSetting(int desiredZoom, int tolerance) {
        if (desiredZoom > MAX_VARC_ZOOM || desiredZoom < MIN_VARC_ZOOM) {
            throw new IllegalArgumentException(
                "The desired VarcZoom of " + desiredZoom + " is not valid"
            );
        }
        int initial = getZoomSetting();
        int offset = desiredZoom - initial;
        long startTime = System.currentTimeMillis();
        Shape viewport = Projection.getViewport();
        if (viewport != null) {
            Mouse.move(new InteractableShape(viewport));
        }
        int max_scroll_time = Random.nextInt(3000, 5000);
        if (offset > 0) {
            while (desiredZoom > getZoomSetting() &&
                (System.currentTimeMillis() - startTime) < max_scroll_time) {
                Mouse.scroll(false);
            }
        } else {
            while (desiredZoom < getZoomSetting() &&
                (System.currentTimeMillis() - startTime) < max_scroll_time) {
                Mouse.scroll(true);
            }
        }
        return Math.abs(getZoomSetting() - desiredZoom) <= tolerance;
    }

    @OSRSOnly
    public static boolean setJagZoom(int desiredJagZoom, int tolerance) {
        boolean resizable = OSRSInterfaceOptions.isViewportResizable();
        if (desiredJagZoom > (resizable ? RESIZABLE_MAX_JAG_ZOOM : FIXED_MAX_JAG_ZOOM) ||
            desiredJagZoom < (resizable ? RESIZABLE_MIN_JAG_ZOOM : FIXED_MIN_JAG_ZOOM)) {
            throw new IllegalArgumentException(
                "The desiredJagZoom of " + desiredJagZoom + " is not valid in " +
                    (resizable ? "resizable" : "fixed") + " mode.");
        }
        int initial = getJagZoom();
        int offset = desiredJagZoom - initial;
        long startTime = System.currentTimeMillis();
        Shape viewport = Projection.getViewport();
        if (viewport != null) {
            Mouse.move(new InteractableShape(viewport));
        }
        int max_scroll_time = Random.nextInt(3000, 5000);
        if (offset > 0) {
            while (desiredJagZoom > getJagZoom() &&
                (System.currentTimeMillis() - startTime) < max_scroll_time) {
                Mouse.scroll(false);
            }
        } else {
            while (desiredJagZoom < getJagZoom() &&
                (System.currentTimeMillis() - startTime) < max_scroll_time) {
                Mouse.scroll(true);
            }
        }
        return Math.abs(getJagZoom() - desiredJagZoom) <= tolerance;
    }

    /**
     * Turns the camera to the specified yaw using a reasonable tolerance (uses PlayerSense)
     */
    public static boolean turnTo(int yaw) {
        return turnTo(yaw, getPitch());
    }

    /**
     * Turns the camera to the specified yaw and pitch using a reasonable tolerance (uses PlayerSense)
     */
    public static boolean turnTo(int yaw, double pitch) {
        try {
            Future<Boolean> future = concurrentlyTurnTo(yaw, pitch);
            return future != null && future.get();
        } catch (InterruptedException | ExecutionException | CancellationException ee) {
            return false;
        }
    }

    /**
     * Starts turning the camera to the specified yaw and pitch using a reasonable tolerance (uses PlayerSense and is asynchronized)
     */
    public static Future<Boolean> concurrentlyTurnTo(int yaw, double pitch) {
        return concurrentlyTurnTo(yaw, pitch,
            PlayerSense.getAsDouble(PlayerSense.Key.CAMERA_TOLERANCE)
        );
    }

    /**
     * Starts turning the camera to the specified yaw and pitch(uses PlayerSense and is asynchronized)
     * <p>
     * Bugfix by @Guru - Camera sometimes turns wrong direction
     *
     * @param tolerance - How precise does our camera movement need to be?
     *                  0 = 100% precise (must be exact), 0.05 = 95% precise, 1 = 0% precise
     */
    public static Future<Boolean> concurrentlyTurnTo(
        final int desiredYaw,
        final double desiredPitch,
        final double tolerance
    ) {
        if (desiredYaw == -1 || desiredPitch == -1) {
            return null;
        }
        if (desiredYaw < 0 || desiredYaw > 359) {
            Environment.getLogger().debug(
                "[Camera] The desired yaw must be value between 0 and 359 [" + desiredYaw +
                    " is out of range]");
            return null;
        }
        if (desiredPitch < 0 || desiredPitch > 1) {
            Environment.getLogger().debug(
                "[Camera] The desired pitch must be value between 0 and 1 [" + desiredPitch +
                    " is out of range]");
            return null;
        }
        final AbstractBot bot = Environment.getBot();
        if (bot == null) {
            return null;
        }
        FutureTask<Boolean> new_task = new FutureTask<>(() -> {
            Thread thread = Thread.currentThread();
            Timer timer = new Timer(2500, 3750);

            boolean wasd = false;
          /*boolean wasd = client.isRS3()
                    && PlayerSense.getAsBoolean(PlayerSense.Key.USE_WASD_KEYS)
                    && RS3Chat.getIsAlwaysOnChatState() == 0
                    && !Bank.isOpen()
                    && ChatDialog.getContinue() == null
                    && ChatDialog.getOptions().isEmpty();
            if (wasd) {
                String textEntry = getEnteredChatText();
                if (textEntry != null && Keyboard.typeKey(KeyEvent.VK_ESCAPE)
                        && Execution.delayWhile(() -> textEntry.equals(getEnteredChatText()), 700, 1250)) {
                    String textEntry2 = getEnteredChatText();
                    if (textEntry2 != null && Keyboard.typeKey(KeyEvent.VK_ESCAPE)
                            && !Execution.delayWhile(() -> textEntry.equals(getEnteredChatText()), 700, 1250)) {
                        return false;
                    }
                }
            }*/
            int yawOffset = RotatableCommons.getYawOffset(desiredYaw);
            double currentPitch = getPitch();
            boolean left =
                yawOffset < PlayerSense.getAsInteger(PlayerSense.Key.CAMERA_DIRECTION_BIAS);
            boolean down = currentPitch > desiredPitch;
            final int xKey = left ? KeyEvent.VK_LEFT :
                KeyEvent.VK_RIGHT; //left ? (wasd ? KeyEvent.VK_A : KeyEvent.VK_LEFT) : (wasd ? KeyEvent.VK_D : KeyEvent.VK_RIGHT);
            final int yKey = down ? KeyEvent.VK_DOWN :
                KeyEvent.VK_UP; //down ? (wasd ? KeyEvent.VK_S : KeyEvent.VK_DOWN) : (wasd ? KeyEvent.VK_W : KeyEvent.VK_UP);
            try {
                boolean moveX = Math.abs(yawOffset) > (360 * tolerance);
                boolean moveY = Math.abs(currentPitch - desiredPitch) > tolerance;
                if (moveX && !Keyboard.isPressed(xKey)) {
                    Environment.getLogger().fine(
                        "[Camera] Pressing " + xKey + " to change yaw to " + desiredYaw + " from " +
                            getYaw() + " [" + (left ? "LEFT" : "RIGHT") + "]");
                    Keyboard.pressKey(xKey);
                }
                if (moveY && !Keyboard.isPressed(yKey)) {
                    Environment.getLogger().fine(
                        "[Camera] Pressing " + yKey + " to change pitch to " + desiredPitch +
                            " from " + currentPitch + " [" + (down ? "DOWN" : "UP") + "]");
                    Keyboard.pressKey(yKey);
                }

                timer.start();
                while (bot.isRunning() && !thread.isInterrupted() && timer.isRunning() &&
                    (moveX || moveY)) {
                    if (moveX) {
                        moveX = left ? RotatableCommons.getYawOffset(desiredYaw) < (360 * tolerance) :
                            RotatableCommons.getYawOffset(desiredYaw) > (360 * tolerance);

                        if (!moveX) {
                            Environment.getLogger().fine("[Camera] It should now release the key " +
                                (left ? "LEFT" : "RIGHT") + " which is currently " +
                                (Keyboard.isPressed(xKey) ? "pressed." : "not pressed!"));
                        }
                        if (!moveX && Keyboard.isPressed(xKey)) {
                            Environment.getLogger()
                                .fine("[Camera] Releasing key " + (left ? "LEFT" : "RIGHT"));
                            Keyboard.releaseKey(xKey);
                        }
                        if (moveX && !Keyboard.isPressed(xKey)) {
                            Environment.getLogger().fine(
                                "[Camera] Pressing the " + (left ? "LEFT" : "RIGHT") +
                                    " key again because we released it too early");
                            Keyboard.pressKey(xKey);
                        }
                    }
                    if (moveY) {
                        moveY = down ? getPitch() - desiredPitch > tolerance : getPitch() - desiredPitch < tolerance;
                        if (!moveY) {
                            Environment.getLogger().fine(
                                "[Camera] It should now release the key " + (down ? "DOWN" : "UP") +
                                    " which is currently " +
                                    (Keyboard.isPressed(yKey) ? "pressed." : "not pressed!"));
                        }
                        if (!moveY && Keyboard.isPressed(yKey)) {
                            Environment.getLogger()
                                .fine("[Camera] Releasing key " + (down ? "DOWN" : "UP"));
                            Keyboard.releaseKey(yKey);
                        }
                        if (moveY && !Keyboard.isPressed(yKey)) {
                            Environment.getLogger().fine(
                                "[Camera] Pressing the " + (down ? "DOWN" : "UP") +
                                    " key again because we released it too early");
                            Keyboard.pressKey(yKey);
                        }
                    }
                    Execution.delay(10, 40, 20);
                }
                return Math.abs(RotatableCommons.getYawOffset(desiredYaw)) <= (360 * tolerance) &&
                    Math.abs(getPitch() - desiredPitch) <= tolerance;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            } finally {
                if ((!bot.isRunning() || thread.isInterrupted() || !timer.isRunning()) &&
                    (Keyboard.isPressed(xKey) || Keyboard.isPressed(yKey))) {
                    Environment.getLogger().fine(
                        "[Camera] Releasing keys due to either the bot not running, the thread being interrupted, or the safety timeout expiring.");
                    if (Keyboard.isPressed(xKey)) {
                        Environment.getLogger().fine(
                            "[Camera] Releasing the yaw controlling key which has a keycode of " +
                                xKey + ".");
                        Keyboard.releaseKey(xKey);
                    }
                    if (Keyboard.isPressed(yKey)) {
                        Environment.getLogger().fine(
                            "[Camera] Releasing the pitch controlling key which has a keycode of " +
                                yKey + ".");
                        Keyboard.releaseKey(yKey);
                    }
                }
            }
        });
        FutureTask<Boolean> last = lastTaskMap.get(bot);
        if (last != null && !last.isDone()) {
            try {
                last.get(3, TimeUnit.SECONDS);
            } catch (Exception ignored) {
            }
            last.cancel(true);
        }
        synchronized (lastTaskMap) {
            lastTaskMap.put(bot, new_task);
        }
        synchronized (executorMap) {
            executorMap.computeIfAbsent(
                bot,
                as -> Executors.newSingleThreadExecutor(getCameraMovementThreadFactory(bot))
            )
                .submit(new_task);
        }
        return new_task;
    }

    private static ThreadFactory getCameraMovementThreadFactory(AbstractBot bot) {
        return new ThreadFactoryBuilder().setNameFormat(
            "camera-thread-" + Environment.getRuneScapeProcessId() + '-' +
                bot.getMetaData().getName().replace("%", "")).build();
    }

    /**
     * Turns the camera to the specified pitch (uses PlayerSense)
     */
    public static boolean turnTo(double pitch) {
        return turnTo(getYaw(), pitch);
    }

    /**
     * Turns the camera to the specified yaw and pitch using a specified tolerance (uses PlayerSense)
     */
    public static boolean turnTo(int yaw, double pitch, double tolerance) {
        try {
            Future<Boolean> future = concurrentlyTurnTo(yaw, pitch, tolerance);
            return future != null && future.get();
        } catch (InterruptedException | ExecutionException | CancellationException ee) {
            return false;
        }
    }

    /**
     * Turns the camera to the specified target using a reasonable tolerance (uses PlayerSense and adjusts yaw and pitch)
     */
    public static boolean turnTo(Locatable target) {
        try {
            Future<Boolean> future = concurrentlyTurnTo(target);
            return future != null && future.get();
        } catch (InterruptedException | ExecutionException | CancellationException ee) {
            return false;
        }
    }

    /**
     * Starts turning the camera to the specified target using a reasonable tolerance (uses PlayerSense, is asynchronized, and adjusts yaw and pitch)
     *
     * @param target the entity to be in focus
     */
    public static Future<Boolean> concurrentlyTurnTo(Locatable target) {
        if (target == null) {
            return null;
        }
        Player center = Players.getLocal();
        int angle = getAngleYawRelativeTo(center, target);
        if (angle == -1) {
            return null;
        }
        double pitch = getPitchRelativeTo(center, target);
        if (pitch < 0 || pitch > 1) {
            return null;
        }
        return concurrentlyTurnTo(angle, pitch);
    }

    public static boolean turnTo(Locatable target, int yaw) {
        try {
            Future<Boolean> future = concurrentlyTurnTo(target, yaw);
            return future != null && future.get();
        } catch (InterruptedException | ExecutionException | CancellationException ee) {
            return false;
        }
    }

    public static Future<Boolean> concurrentlyTurnTo(Locatable target, int yaw) {
        if (target == null) {
            return null;
        }
        Player center = Players.getLocal();
        if (center == null) {
            return null;
        }
        return concurrentlyTurnTo(yaw, getPitchRelativeTo(center, target));
    }

    public static boolean turnTo(Locatable target, double pitch) {
        try {
            Future<Boolean> future = concurrentlyTurnTo(target, pitch);
            return future != null && future.get();
        } catch (InterruptedException | ExecutionException | CancellationException ee) {
            return false;
        }
    }

    public static Future<Boolean> concurrentlyTurnTo(Locatable target, double pitch) {
        if (target == null) {
            return null;
        }
        int angle = getAngleYawRelativeTo(Players.getLocal(), target);
        if (angle == -1) {
            return null;
        }
        return concurrentlyTurnTo(angle, pitch);
    }

    /**
     * Starts turning the camera to the specified yaw using a reasonable tolerance (uses PlayerSense and is asynchronized)
     */
    public static Future<Boolean> concurrentlyTurnTo(int yaw) {
        return concurrentlyTurnTo(yaw, getPitch());
    }

    /**
     * Starts turning the camera to the specified pitch using a reasonable tolerance (uses PlayerSense and is asynchronized)
     */
    public static Future<Boolean> concurrentlyTurnTo(double pitch) {
        return concurrentlyTurnTo(getYaw(), pitch);
    }

    /**
     * Reports whether the zoom is locked and cannot be changed.
     */
    public static boolean isZoomLocked() {
        if (Environment.isOSRS()) {
            Varbit varbit = Varbits.load(6357);
            return varbit != null && varbit.getValue() == 1;
        } else {
            Varbit varbit = Varbits.load(19926);
            return varbit != null && varbit.getValue() == 1;
        }
    }

    public static boolean isTurning() {
        FutureTask<Boolean> last = lastTaskMap.get(Environment.getBot());
        return last != null && !last.isDone();
    }

    private static String getEnteredChatText() {
        for (InterfaceComponent ic : Interfaces.newQuery()
            .containers(137, 228, 464, 1467, 1470, 1471, 1529)
            .grandchildren(false).types(InterfaceComponent.Type.LABEL).visible().heights(17)
            .actions("Enter message")
            .results()) {
            if (!NORMAL_CHAT_TEXT_COLOR.equals(ic.getTextColor())) {
                return ic.getText();
            }
        }
        return null;
    }

    private static int getAngleYawRelativeTo(Locatable center, Locatable target) {
        return CommonMath.getAngleOf(center, target);
    }

    private static double getPitchRelativeTo(Locatable center, Locatable target) {
        //Max I've seen is .849 (seems common
        return Math.max(.1, -0.0179 * Distance.between(center, target) + 0.688);
    }

    public static boolean isSceneTileDrawn(int floor, int tileX, int tileZ) {
        int cameraTileX = getCameraTileX();
        int cameraTileZ = getCameraTileZ();

        int minTileX = Math.max(0, cameraTileX - 25);
        int minTileZ = Math.max(0, cameraTileZ - 25);
        int maxTileX = Math.min(103, cameraTileX + 25);
        int maxTileZ = Math.min(103, cameraTileZ + 25);

        if (tileX < minTileX || tileX >= maxTileX || tileZ < minTileZ || tileZ >= maxTileZ) {
            return false;
        }
        boolean[][] tileVisibilityGrid = getTileVisibilityGrid();
        if (tileVisibilityGrid[tileX - cameraTileX + 25][tileZ - cameraTileZ + 25]) {
            return true;
        }
        int[][] heightmap = getHeightmap(floor);
        return heightmap[tileX][tileZ] - getY() >= 2000;
    }

    private static int[][] getHeightmap(int floor) {
        return OpenRegion.getTileHeights(floor);
    }

    private static boolean[][][][] getSceneVisibilityGrid() {
        return OpenCamera.getSceneVisibilityGrid();
    }

    private static boolean[][] getTileVisibilityGrid() {
        return OpenCamera.getTileVisibilityGrid();
    }

    private static int getCameraTileX() {
        return OpenCamera.getTileX();
    }

    private static int getCameraTileZ() {
        return OpenCamera.getTileZ();
    }
}