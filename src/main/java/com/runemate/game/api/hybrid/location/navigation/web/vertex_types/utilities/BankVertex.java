package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.utilities;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.util.collections.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;
import javax.annotation.*;

public abstract class BankVertex extends UtilityVertex {
    protected Pattern name, action;

    protected BankVertex(Coordinate position, Collection<WebRequirement> requirements) {
        super(position, requirements);
    }

    public BankVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    public BankVertex(
        final Pattern name, final Pattern action, final Coordinate position,
        final Collection<WebRequirement> requirements
    ) {
        super(position, requirements);
        this.name = name;
        this.action = action;
    }

    public abstract LocatableEntity getBank();

    @Override
    public boolean step() {
        return Bank.open(getBank(), action);
    }

    public Pattern getAction() {
        return action;
    }

    public Pattern getName() {
        return name;
    }

    @Override
    public String toString() {
        Coordinate position = getPosition();
        return "BankVertex(" + getName() + ", " + getAction() + ", " + position.getX() + ", " +
            position.getY() + ", " + position.getPlane() + ')';
    }

    @Nonnull
    @Override
    public Pair<WebVertex, WebPath.VertexSearchAction> getStep(Map<String, Object> args) {
        final LocatableEntity bank = getBank();
        return new Pair<>(
            bank != null && bank.isVisible() ? this : null,
            WebPath.VertexSearchAction.STOP
        );
    }
}
