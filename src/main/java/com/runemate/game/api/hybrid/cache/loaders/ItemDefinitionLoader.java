package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.cache.item.*;
import java.util.*;

public class ItemDefinitionLoader extends SerializedFileLoader<CacheItemDefinition> {

    public ItemDefinitionLoader(int file) {
        super(file);
    }

    @Override
    protected CacheItemDefinition construct(int entry, int file, Map<String, Object> arguments) {
        return new CacheItemDefinition(file);
    }
}
