package com.runemate.game.api.hybrid.util;

import java.awt.*;
import java.util.*;

public final class RSColors {
    private static final int[] HSV_TO_RGB = getHsvToRgbMapping();
    private static Comparator<Color> COMPARATOR =
        Comparator.comparingInt(Color::getRed).thenComparingInt(Color::getGreen).thenComparingInt(Color::getBlue);

    private static int[] getHsvToRgbMapping() {
        int[] map = new int[65536];
        double intensity = 0.85;
        int index = 0;
        for (int color = 0; color < 512; color++) {
            float hue = 360.0F * (0.0078125F + (color >> 3) / 64.0F);
            float saturation = 0.0625F + (color & 0x7) / 8.0F;
            for (int brightness = 0; brightness < 128; brightness++) {
                float value = brightness / 128.0F;
                float normalisedRed = 0.0F;
                float normalisedGreen = 0.0F;
                float normalisedBlue = 0.0F;
                float abc = hue / 60.0F;
                int def = (int) abc;
                int type = def % 6;
                float ghi = abc - def;
                float chroma = value * (1.0F - saturation);
                float jkl = value * (1.0F - ghi * saturation);
                float mno = value * (1.0F - (1.0F - ghi) * saturation);
                if (type == 0) {
                    normalisedRed = value;
                    normalisedGreen = mno;
                    normalisedBlue = chroma;
                } else if (type == 1) {
                    normalisedRed = jkl;
                    normalisedGreen = value;
                    normalisedBlue = chroma;
                } else if (type == 2) {
                    normalisedRed = chroma;
                    normalisedGreen = value;
                    normalisedBlue = mno;
                } else if (type == 3) {
                    normalisedRed = chroma;
                    normalisedGreen = jkl;
                    normalisedBlue = value;
                } else if (type == 4) {
                    normalisedRed = mno;
                    normalisedGreen = chroma;
                    normalisedBlue = value;
                } else if (type == 5) {
                    normalisedRed = value;
                    normalisedGreen = chroma;
                    normalisedBlue = jkl;
                }
                normalisedRed = (float) Math.pow(normalisedRed, intensity);
                normalisedGreen = (float) Math.pow(normalisedGreen, intensity);
                normalisedBlue = (float) Math.pow(normalisedBlue, intensity);
                int r = (int) (normalisedRed * 256.0F);
                int g = (int) (normalisedGreen * 256.0F);
                int b = (int) (normalisedBlue * 256.0F);
                int rgb = b + (g << 8) + (r << 16) + 0xff000000;
                map[index++] = rgb;
            }
        }
        return map;
    }

    public static int smoothValidHSLLighting(int hsl, int light) {
        if (hsl == -1) {
            return 12345678;
        } else {
            light = light * (hsl & 0x7f) >> 7;
            if (light < 2) {
                light = 2;
            } else if (light > 126) {
                light = 126;
            }

            return (hsl & 'ﾀ') + light;
        }
    }

    public static Color fromHSV(int hsv) {
        int unsigned = 0xffff & hsv;
        float hue = unsigned >> 10 & 0x3f;
        hue /= 63;
        float saturation = unsigned >> 7 & 7;
        saturation /= 7;
        float brightness = unsigned & 0x7f;
        brightness /= 127;
        return new Color(Color.HSBtoRGB(hue, saturation, brightness));
    }

    public static int hsvToRgb(int hsv) {
        return HSV_TO_RGB[hsv];
    }

    public static int hslToHsv(int hsl) {
        int hue = hsl >> 10 & 0x3f;
        int saturation = hsl >> 3 & 0x70;
        int light = hsl & 0x7f;
        if (light <= 64) {
            saturation = saturation * light >> 7;
        } else {
            saturation = (127 - light) * saturation >> 7;
        }
        int value = saturation + light;
        saturation = value != 0 ? saturation * 256 / value : saturation * 2;
        return ((hue << 10) | ((saturation >> 4) << 7) | value);
    }

    public static int rgbToHsl(int rgb) {
        double r = (rgb >> 16 & 0xff) / 256.0;
        double g = (rgb >> 8 & 0xff) / 256.0;
        double b = (rgb & 0xff) / 256.0;
        double minColor = Math.min(r, Math.min(g, b));
        double maxColor = Math.max(r, Math.max(g, b));
        double h = 0.0;
        double s = 0.0;
        double l = (minColor + maxColor) / 2.0;
        if (minColor != maxColor) {
            if (l < 0.5) {
                s = (maxColor - minColor) / (maxColor + minColor);
            } else if (l >= 0.5) {
                s = (maxColor - minColor) / (2.0 - maxColor - minColor);
            }
            if (r == maxColor) {
                h = (g - b) / (maxColor - minColor);
            } else if (g == maxColor) {
                h = (b - r) / (maxColor - minColor) + 2.0;
            } else if (b == maxColor) {
                h = (r - g) / (maxColor - minColor) + 4.0;
            }
        }
        //Normalize it to a number between 0 and 1
        h /= 6.0;
        int ih = (int) (h * 256.0);
        int is = (int) (s * 256.0);
        int il = (int) (l * 256.0);
        if (is < 0) {
            is = 0;
        } else if (is > 255) {
            is = 255;
        }
        if (il < 0) {
            il = 0;
        } else if (il > 255) {
            il = 255;
        }
        //Compress it so it's compactable into their format
        if (il > 243) {
            is >>= 4;
        } else if (il > 217) {
            is >>= 3;
        } else if (il > 192) {
            is >>= 2;
        } else if (il > 179) {
            is >>= 1;
        }
        return (il >> 1) + ((ih & 0xff) >> 2 << 10) + (is >> 5 << 7);
    }

    public static int smoothAnyHSLLighting(int hsl, int light) {
        if (hsl == -2) {
            return 12345678;
        } else if (hsl == -1) {
            if (light < 2) {
                light = 2;
            } else if (light > 126) {
                light = 126;
            }

            return light;
        } else {
            light = (hsl & 127) * light / 128;
            if (light < 2) {
                light = 2;
            } else if (light > 126) {
                light = 126;
            }

            return (hsl & 'ﾀ') + light;
        }
    }

    public static Comparator<Color> getComparator() {
        return COMPARATOR;
    }
}
