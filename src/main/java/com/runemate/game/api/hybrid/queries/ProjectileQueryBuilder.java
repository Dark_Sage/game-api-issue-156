package com.runemate.game.api.hybrid.queries;

import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.region.*;
import java.util.*;
import java.util.concurrent.*;

public class ProjectileQueryBuilder
    extends LocatableEntityQueryBuilder<Projectile, ProjectileQueryBuilder> {
    private int[] ids, animationIds, modelIds;
    private Callable<Actor> targetCallable;

    @Override
    public ProjectileQueryBuilder get() {
        return this;
    }

    @Override
    public Callable<List<? extends Projectile>> getDefaultProvider() {
        return () -> Projectiles.getLoaded().asList();
    }

    /**
     * @see com.runemate.game.api.hybrid.entities.Projectile#getSource()
     * @deprecated
     */
    @Deprecated
    public ProjectileQueryBuilder source(final Actor source) {
        return get();
    }

    public ProjectileQueryBuilder target(final Actor target) {
        this.targetCallable = () -> target;
        return get();
    }

    public ProjectileQueryBuilder target(final Callable<Actor> target) {
        this.targetCallable = target;
        return get();
    }

    public ProjectileQueryBuilder ids(final int... ids) {
        this.ids = ids;
        return get();
    }

    public ProjectileQueryBuilder animations(final int... animationIds) {
        this.animationIds = animationIds;
        return get();
    }

    public ProjectileQueryBuilder models(final int... modelIds) {
        this.modelIds = modelIds;
        return get();
    }

    @Override
    public boolean accepts(Projectile argument) {
        boolean condition;
        SpotAnimationDefinition definition = null;
        if (ids != null) {
            if (definition == null) {
                definition = argument.getDefinition();
            }
            condition = false;
            int id = definition.getId();
            for (final int value : this.ids) {
                if (value == id) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (animationIds != null) {
            if (definition == null) {
                definition = argument.getDefinition();
            }
            condition = false;
            int animationId = definition.getAnimationId();
            for (final int value : this.animationIds) {
                if (value == animationId) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (modelIds != null) {
            if (definition == null) {
                definition = argument.getDefinition();
            }
            condition = false;
            int modelId = definition.getModelId();
            for (final int value : this.modelIds) {
                if (value == modelId) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (targetCallable != null) {
            Actor target;
            try {
                target = targetCallable.call();
            } catch (Exception e) {
                target = null;
            }
            if (target != null) {
                Actor current_target = argument.getTarget();
                condition = Objects.equals(target, current_target);
                if (!condition) {
                    return false;
                }
            }
        }
        return super.accepts(argument);
    }
}
