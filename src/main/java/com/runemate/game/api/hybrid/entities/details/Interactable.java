package com.runemate.game.api.hybrid.entities.details;

import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.util.*;
import java.awt.*;
import java.util.regex.*;
import javax.annotation.*;

/**
 * An entity that can be interacted with
 */
public interface Interactable {
    /**
     * Whether the entity is visible
     *
     * @return true if visible, and false if not visible
     */
    boolean isVisible();

    /**
     * Returns a rough estimate of how visible this entity is as a percent (0 to 100)
     */
    double getVisibility();

    boolean hasDynamicBounds();

    /**
     * Gets a point within the entity that can be interacted with. Assumes the origin is the current mouse position.
     *
     * @return an InteractablePoint if available, otherwise null
     */
    @Nullable
    default InteractablePoint getInteractionPoint() {
        return getInteractionPoint(Mouse.getPosition());
    }

    /**
     * Gets a point within the entity that can be interacted with. Takes into account the direction of movement from the origin point.
     */
    @Nullable
    InteractablePoint getInteractionPoint(Point origin);

    /**
     * Checks if the projected bounds of this entity contains the Point provided.
     *
     * @param point
     * @return
     */
    boolean contains(Point point);

    /**
     * Clicks this entity in an interactable point
     *
     * @return true if successfully clicked
     */
    boolean click();

    /**
     * Moves the mouse over this entity
     *
     * @return true if the mouse is hovering the entity
     */
    default boolean hover() {
        return Mouse.move(this);
    }

    /**
     * Gets whether or not this entity is being hovered.
     *
     * @return true if the entity is currently being hovered, otherwise false.
     */
    default boolean isHovered() {
        return contains(Mouse.getPosition());
    }


    /**
     * Attempts to match action and target using Matcher.find() and then interacts with the appropriate menu item
     *
     * @param action The pattern to match the action with, if null it's ignored
     * @param target The pattern to match the target with, if null it's ignored
     */
    boolean interact(@Nullable Pattern action, @Nullable Pattern target);

    default boolean interact(@Nullable String action) {
        return interact(action, (Pattern) null);
    }

    default boolean interact(@Nullable Pattern action) {
        return interact(action, (Pattern) null);
    }

    default boolean interact(@Nullable String action, @Nullable Pattern target) {
        Pattern actionPattern = null;
        if (action != null) {
            actionPattern = Regex.getPatternForExactString(action);
        }
        return interact(actionPattern, target);
    }

    default boolean interact(@Nullable Pattern action, @Nullable String target) {
        Pattern targetPattern = null;
        if (target != null) {
            targetPattern = Regex.getPatternForExactString(target);
        }
        return interact(action, targetPattern);
    }

    default boolean interact(@Nullable String action, @Nullable String target) {
        Pattern actionPattern = null;
        if (action != null) {
            actionPattern = Regex.getPatternForExactString(action);
        }
        Pattern targetPattern = null;
        if (target != null) {
            targetPattern = Regex.getPatternForExactString(target);
        }
        return interact(actionPattern, targetPattern);
    }
}
