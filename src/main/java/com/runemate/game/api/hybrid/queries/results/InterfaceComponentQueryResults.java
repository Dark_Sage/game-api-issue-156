package com.runemate.game.api.hybrid.queries.results;

import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import java.util.*;
import java.util.concurrent.*;

public class InterfaceComponentQueryResults
    extends InteractableQueryResults<InterfaceComponent, InterfaceComponentQueryResults> {
    public InterfaceComponentQueryResults(final Collection<? extends InterfaceComponent> results) {
        super(results);
    }

    public InterfaceComponentQueryResults(
        final Collection<? extends InterfaceComponent> results,
        ConcurrentMap<String, Object> cache
    ) {
        super(results, cache);
    }

    @Override
    protected InterfaceComponentQueryResults get() {
        return this;
    }
}
