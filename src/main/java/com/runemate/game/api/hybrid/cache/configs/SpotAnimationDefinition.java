package com.runemate.game.api.hybrid.cache.configs;

import com.runemate.game.api.hybrid.cache.materials.*;
import com.runemate.game.api.hybrid.entities.details.*;
import java.awt.*;
import java.util.*;

public abstract class SpotAnimationDefinition implements Identifiable {
    public abstract int getModelId();

    public abstract int getAnimationId();

    public abstract int getAngle();

    public abstract Map<Color, Color> getColorSubstitutions();

    public abstract Map<Material, Material> getMaterialSubstitutions();
}
