package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.objects;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.script.*;
import java.io.*;
import java.util.*;
import lombok.*;
import org.apache.commons.lang3.builder.*;

public class FairyRingVertex extends ObjectVertex implements SerializableVertex {
    private final static int OSRS_CONTAINER = 398;
    private final static int RS3_CONTAINER = 734;
    private char firstLetter;
    private char secondLetter;
    private char thirdLetter;

    public FairyRingVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> restrictions, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, restrictions, protocol, stream);
    }

    public FairyRingVertex(
        int x, int y, int plane, char firstLetter, char secondLetter,
        char thirdLetter, Collection<WebRequirement> requirements
    ) {
        super(new Coordinate(x, y, plane), "Fairy ring", "Configure", requirements);
        this.firstLetter = firstLetter;
        this.secondLetter = secondLetter;
        this.thirdLetter = thirdLetter;
    }

    public FairyRingVertex(
        Coordinate position, char firstLetter, char secondLetter,
        char thirdLetter, Collection<WebRequirement> requirements
    ) {
        super(position, "Fairy ring", "Configure", requirements);
        this.firstLetter = firstLetter;
        this.secondLetter = secondLetter;
        this.thirdLetter = thirdLetter;
    }

    private static InterfaceComponent getConfirmButton() {
        return Interfaces.newQuery().containers(OSRS_CONTAINER).actions("Confirm").results()
            .first();
    }

    @Override
    public String toString() {
        return "FairyRingVertex(" + firstLetter + secondLetter + thirdLetter + ')';
    }

    @Override
    public int getOpcode() {
        return 19;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeChar(firstLetter);
        stream.writeChar(secondLetter);
        stream.writeChar(thirdLetter);
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        this.firstLetter = stream.readChar();
        this.secondLetter = stream.readChar();
        this.thirdLetter = stream.readChar();
        return true;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(getPosition());
        builder.append(firstLetter).append(secondLetter).append(thirdLetter);
        return builder.toHashCode();
    }

    @Override
    public boolean step() {
        InterfaceComponent confirmButton = getConfirmButton();
        if (confirmButton == null) {
            GameObject object = getObject();
            if (object != null
                && (object.getVisibility() >= 25 || Camera.turnTo(object))) {
                if (!object.interact(action, target)) {
                    return false;
                } else {
                    Player local = Players.getLocal();
                    if (local == null ||
                        !Execution.delayUntil(() -> getConfirmButton() != null, local::isMoving,
                            1200, 1800
                        )) {
                        return false;
                    }
                }
                confirmButton = getConfirmButton();
            }
        }
        if (confirmButton != null) {
            if (!Dial.LEFT.isSelected(String.valueOf(firstLetter)) &&
                !Dial.LEFT.rotateTo(String.valueOf(firstLetter))) {
                return false;
            }
            if (!Dial.CENTER.isSelected(String.valueOf(secondLetter)) &&
                !Dial.CENTER.rotateTo(String.valueOf(secondLetter))) {
                return false;
            }
            if (!Dial.RIGHT.isSelected(String.valueOf(thirdLetter)) &&
                !Dial.RIGHT.rotateTo(String.valueOf(thirdLetter))) {
                return false;
            }
            if (confirmButton.interact("Confirm")) {
                Player local = Players.getLocal();
                if (local != null &&
                    Execution.delayUntil(() -> local.getAnimationId() != -1, 1800, 2400)) {
                    Execution.delayWhile(() -> local.getAnimationId() == -1);
                    return true;
                }
            }
        }
        return false;
    }

    private enum Dial {
        LEFT(3985, 12072, "A", "D", "C", "B"),
        CENTER(3986, 23073, "I", "L", "K", "J"),
        RIGHT(3987, 12074, "P", "S", "R", "Q");

        private final String CLOCKWISE = "Rotate clockwise";
        private final String OSRS_AC = "Rotate counter-clockwise";
        private final String RS3_AC = "Rotate anticlockwise";

        private final int osrs_varbit;
        private final int rs3_varbit;
        private final String[] values;

        Dial(int osrs_varbit, int rs3_varbit, String... values) {
            this.osrs_varbit = osrs_varbit;
            this.rs3_varbit = rs3_varbit;
            this.values = values;
        }

        private InterfaceComponent getClockwiseComponent() {
            boolean rs3 = false;
            InterfaceComponentQueryResults ic = Interfaces.newQuery()
                .containers(rs3 ? RS3_CONTAINER : OSRS_CONTAINER).types(InterfaceComponent.Type.BOX)
                .actions(CLOCKWISE).grandchildren(false).results();
            return ic.isEmpty() ? null : ic.get(ordinal());
        }

        private InterfaceComponent getAntiClockwiseComponent() {
            boolean rs3 = false;
            InterfaceComponentQueryResults ic = Interfaces.newQuery()
                .containers(rs3 ? RS3_CONTAINER : OSRS_CONTAINER).types(InterfaceComponent.Type.BOX)
                .actions(OSRS_AC).grandchildren(false).results();
            return ic.isEmpty() ? null : ic.get(ordinal());
        }

        private boolean rotateClockwise() {
            InterfaceComponent ic = getClockwiseComponent();
            return ic != null && ic.interact(CLOCKWISE);
        }

        private boolean rotateAntiClockwise() {
            InterfaceComponent ic = getAntiClockwiseComponent();
            return ic != null && ic.interact(OSRS_AC);
        }

        public boolean rotateTo(String value) {
            if (value.length() != 1 || !Arrays.asList(values).contains(value)) {
                throw new UnsupportedOperationException("Unsupported Fairy Ring code: " + value);
            }
            int turns = Math.abs(getCurrent() - indexOf(value));
            return isSelected(value)
                || (
                turns > 2 ? rotateAntiClockwise() : rotateClockwise()
                    && Execution.delayUntil(() -> isSelected(value), 600, 1200)
            );
        }

        private int getCurrent() {
            Varbit var = Varbits.load(osrs_varbit);
            if (var == null) {
                return -1;
            }
            return var.getValue();
        }

        private int indexOf(String letter) {
            return Arrays.asList(values).indexOf(letter);
        }

        public boolean isSelected(String letter) {
            return getCurrent() == indexOf(letter);
        }
    }
}
