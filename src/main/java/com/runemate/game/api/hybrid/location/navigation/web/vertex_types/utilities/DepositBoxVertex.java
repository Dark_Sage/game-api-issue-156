package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.utilities;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.collections.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;
import javax.annotation.*;
import lombok.*;
import org.apache.commons.lang3.builder.*;

public class DepositBoxVertex extends UtilityVertex implements SerializableVertex {
    private Pattern name;

    public DepositBoxVertex(
        final Pattern name, final Coordinate position,
        final Collection<WebRequirement> requirements
    ) {
        super(position, requirements);
        this.name = name;
    }

    public DepositBoxVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    public GameObject getDepositBox() {
        return GameObjects.newQuery().names(getName()).on(getPosition()).results().first();
    }

    @Override
    public boolean step() {
        return DepositBox.open(getDepositBox());
    }

    public Pattern getName() {
        return name;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getPosition()).append(getName().pattern()).toHashCode();
    }

    @Override
    public String toString() {
        Coordinate position = getPosition();
        return "DepositBoxVertex(name=" + getName() +
            ", x=" + position.getX() + ", y=" + position.getY() + ", plane=" + position.getPlane() +
            ')';
    }

    @Override
    public int getOpcode() {
        return 6;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeUTF(name.pattern());
        stream.writeInt(name.flags());
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        this.name = Pattern.compile(stream.readUTF(), stream.readInt());
        return true;
    }

    @Nonnull
    @Override
    public Pair<WebVertex, WebPath.VertexSearchAction> getStep(Map<String, Object> args) {
        final LocatableEntity bank = getDepositBox();
        return new Pair<>(
            bank != null && bank.isVisible() ? this : null,
            WebPath.VertexSearchAction.STOP
        );
    }
}
