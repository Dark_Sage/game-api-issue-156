package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import java.awt.event.*;
import java.util.*;
import java.util.regex.*;
import javax.annotation.*;


/**
 * A utility for interacting with the game's various chat dialog boxes
 */
public final class ChatDialog {

    private ChatDialog() {
    }

    public static boolean isOpen() {
        return OSRSChatDialog.isOpen();
    }

    @Nullable
    public static String getTitle() {
        return OSRSChatDialog.getTitle();
    }

    public static boolean hasTitle() {
        return getTitle() != null;
    }

    public static boolean hasTitle(String title) {
        return Objects.equals(title, getTitle());
    }

    public static boolean hasTitle(Pattern regex) {
        String title = getTitle();
        return title != null && regex.matcher(title).matches();
    }

    @Nullable
    public static Continue getContinue() {
        InterfaceComponent button = OSRSChatDialog.getContinueButton();
        if (button != null) {
            return new Continue(button);
        }
        return null;
    }

    public static List<Option> getOptions() {
        return OSRSChatDialog.getOptions();
    }

    @Nullable
    public static String getText() {
        return OSRSChatDialog.getText();
    }

    public static boolean hasText() {
        return getText() != null;
    }

    public static boolean hasText(String text) {
        return Objects.equals(text, getText());
    }

    public static boolean hasText(Pattern regex) {
        String text = getText();
        return text != null && regex.matcher(text).matches();
    }

    @Nullable
    public static Option getOption(String... texts) {
        for (Option option : getOptions()) {
            String optText = option.getText();
            for (String text : texts) {
                if (text.equals(optText)) {
                    return option;
                }
            }
        }
        return null;
    }

    @Nullable
    public static Option getOption(Pattern... patterns) {
        for (Option option : getOptions()) {
            String optText = option.getText();
            for (Pattern pattern : patterns) {
                if (pattern.matcher(optText).matches()) {
                    return option;
                }
            }
        }
        return null;
    }

    @Nullable
    public static Option getOption(int number) {
        for (Option option : getOptions()) {
            if (option.getNumber() == number) {
                return option;
            }
        }
        return null;
    }

    public interface Selectable {
        boolean select(boolean keybind);

        boolean select();
    }

    public static final class Continue implements Selectable, Validatable {
        private final InterfaceComponent component;

        private Continue(InterfaceComponent component) {
            this.component = component;
        }

        public InterfaceComponent getComponent() {
            return component;
        }

        @Override
        public boolean select() {
            return select(PlayerSense.getAsBoolean(PlayerSense.Key.USE_MISC_HOTKEYS));
        }

        @Override
        public boolean select(boolean keybind) {
            if (!component.isVisible()) {
                return false;
            }
            String currentText = getText();
            String currentTitle = getTitle();
            if (keybind) {
                if (!Keyboard.typeKey(KeyEvent.VK_SPACE)) {
                    return false;
                }
            } else if (!component.interact("Continue")) {
                return false;
            }
            return Execution.delayUntil(() -> !component.isVisible()
                || !Objects.equals(currentText, getText())
                || !Objects.equals(currentTitle, getTitle()), 1000, 2000);
        }

        @Override
        public String toString() {
            return String.format("ChatDialog.Continue(InterfaceComponent=%s)", component);
        }

        @Override
        public int hashCode() {
            return toString().hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof Continue && toString().equals(obj.toString());
        }

        @Override
        public boolean isValid() {
            return component != null && component.isValid();
        }
    }

    public static class Option implements Selectable, Validatable {
        private final InterfaceComponent component;
        private final int number;

        public Option(InterfaceComponent component, int number) {
            this.component = component;
            this.number = number;
        }

        public String getText() {
            return component.getText();
        }

        public int getNumber() {
            return number;
        }

        public InterfaceComponent getComponent() {
            return component;
        }

        @Override
        public boolean select(boolean keybind) {
            if (!component.isVisible()) {
                return false;
            }
            String currentText = getText();
            String currentTitle = getTitle();
            if (keybind) {
                if (!Keyboard.typeKey(Integer.toString(number).charAt(0))) {
                    return false;
                }
            } else if (!component.click()) {
                return false;
            }
            return Execution.delayUntil(() -> !component.isVisible()
                || !Objects.equals(currentText, getText())
                || !Objects.equals(currentTitle, getTitle()), 1000, 2000);
        }

        @Override
        public boolean select() {
            return select(PlayerSense.getAsBoolean(PlayerSense.Key.USE_MISC_HOTKEYS));
        }

        @Override
        public String toString() {
            return String.format("ChatDialog.Option(InterfaceComponent=%s, index=%s, text=%s)",
                component, number, getText()
            );
        }

        @Override
        public int hashCode() {
            return toString().hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof Option && toString().equals(obj.toString());
        }

        @Override
        public boolean isValid() {
            return component != null && component.isValid();
        }
    }
}
