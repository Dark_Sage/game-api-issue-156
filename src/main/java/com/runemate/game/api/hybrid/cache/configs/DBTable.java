package com.runemate.game.api.hybrid.cache.configs;

import static com.runemate.game.api.hybrid.cache.db.DBTableIndex.*;

import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.cache.io.*;
import com.runemate.game.cache.item.*;
import java.io.*;
import lombok.*;

@EqualsAndHashCode(callSuper = false)
@Data
@CustomLog
public class DBTable extends IncrementallyDecodedItem {

    private final int id;
    private CS2VarType[][] types;
    private Object[] defaultColumnValues;

    @Override
    protected void decode(final Js5InputStream stream, final int opcode) throws IOException {
        if (opcode == 1) {
            int numColumns = stream.readUnsignedByte();
            CS2VarType[][] types = new CS2VarType[numColumns][];
            Object[][] defaultValues = null;

            for (int setting = stream.readUnsignedByte(); setting != 255; setting = stream.readUnsignedByte()) {
                int columnId = setting & 0x7F;
                boolean hasDefault = (setting & 0x80) != 0;
                CS2VarType[] columnTypes = new CS2VarType[stream.readUnsignedByte()];
                for (int i = 0; i < columnTypes.length; i++) {
                    columnTypes[i] = CS2VarType.fromId(stream.readShortSmart());
                }
                types[columnId] = columnTypes;

                if (hasDefault) {
                    if (defaultValues == null) {
                        defaultValues = new Object[types.length][];
                    }

                    defaultValues[columnId] = decodeColumnFields(stream, columnTypes);
                }
            }

            setTypes(types);
            setDefaultColumnValues(defaultValues);
        } else {
            log.warn("Unrecognized dbtable opcode {}" + opcode);
        }
    }
}
