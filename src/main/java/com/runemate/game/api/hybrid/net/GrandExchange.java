package com.runemate.game.api.hybrid.net;

import com.google.common.primitives.*;
import com.google.gson.*;
import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import java.io.*;
import java.net.*;
import java.net.http.*;
import java.util.*;
import java.util.function.*;
import javax.annotation.*;
import org.apache.commons.lang3.builder.*;

/**
 * The newer grand exchange class which queries the correct GrandExchange based on the current game type.
 */
public final class GrandExchange {

    private static final HttpClient httpClient = HttpClient.newBuilder()
        .followRedirects(HttpClient.Redirect.NORMAL)
        .version(HttpClient.Version.HTTP_2)
        .build();

    private static final String OFFICIAL_LOOKUP = "https://secure.runescape.com/m=itemdb_oldschool/api/catalogue/detail.json?item=";
    private static final String WIKI_LOOKUP = "https://prices.runescape.wiki/api/v1/osrs/latest?id=";

    private GrandExchange() {
    }

    public static boolean isOpen() {
        return OSRSGrandExchange.isOpen();
    }

    public static boolean close() {
        return !isOpen() || (OSRSGrandExchange.close());
    }

    /**
     * Looks up the information of this item from the internet.
     */
    @Nullable
    public static Item lookup(final int id) {
        final List<Item> fetched = lookup(new int[] { id });
        return !fetched.isEmpty() ? fetched.get(0) : null;
    }

    /**
     * Places a new buy offer within the GrandExchange.
     *
     * @param itemName The exact item name of what you want to purchase.
     * @param quantity The quantity you want to buy.
     * @param price    The price you want to buy at.
     * @return If the offer was successfully placed.
     */
    public static boolean placeBuyOffer(
        final String itemName, final int quantity,
        final int price
    ) {
        if (!GrandExchange.isOpen()) {
            return false;
        }
        return OSRSGrandExchange.newOffer(itemName, Offer.Type.BUY, quantity, price);
    }

    /**
     * Places a new sell offer within the GrandExchange.
     *
     * @param itemName The exact item name of what you want to sell.
     * @param quantity The quantity you want to sell.
     * @param price    The price you want to sell at.
     * @return If the offer was successfully placed.
     */
    public static boolean placeSellOffer(
        final String itemName, final int quantity,
        final int price
    ) {
        if (!GrandExchange.isOpen()) {
            return false;
        }
        return OSRSGrandExchange.newOffer(itemName, Offer.Type.SELL, quantity, price);
    }

    public static boolean abortOffer(Slot slot) {
        if (!GrandExchange.isOpen()) {
            return false;
        }
        return OSRSGrandExchange.abortOffer(slot);
    }

    /**
     * @return the price of each item being purchased in the currently open offer
     */
    public static int getOfferedPrice() {
        return OSRSGrandExchange.getOfferedPrice();
    }

    /**
     * @return the quantity of each the item being purchased in the currently open offer
     */
    public static int getOfferedQuantity() {
        return OSRSGrandExchange.getOfferedQuantity();
    }

    /**
     * Collects any unclaimed items from the Grand Exchange to the player's inventory
     *
     * @return if the collection was successful, false otherwise
     */
    public static boolean collectToInventory() {
        if (!GrandExchange.isOpen()) {
            return false;
        }
        return OSRSGrandExchange.collectToInventory();
    }

    /**
     * Collects any unclaimed items from the Grand Exchange to the player's bank
     *
     * @return if the collection was successful, false otherwise
     */
    public static boolean collectToBank() {
        if (!GrandExchange.isOpen()) {
            return false;
        }
        return OSRSGrandExchange.collectToBank();
    }

    public static Screen getOpenedScreen() {
        return OSRSGrandExchange.getOpenedScreen();
    }

    /**
     * Looks up the information of these items from the internet.
     * This can take a few seconds so please use a separate thread
     */
    public static List<Item> lookup(final int... ids) {
        List<Integer> missingItems = Ints.asList(ids);
        List<Item> found = new ArrayList<>(ids.length);
        found.addAll(lookupOnOfficial(ids));
        if (found.size() != ids.length) {
            //Remove all found items from the missingItems list
            missingItems.removeIf(id -> found.stream().anyMatch(item -> item.getId() == id));
            //Attempt to get the price from the wiki instead for any items that we weren't able to find.
            found.addAll(lookupOnWiki(Ints.toArray(missingItems)));
            if (found.size() != ids.length) {
                missingItems.removeIf(id -> found.stream().anyMatch(item -> item.getId() == id));
                //TODO Consider resolving with additional services
            }
        }
        return found;
    }

    /**
     * Looks up the the price information from the official grand exchange api.
     */
    private static List<Item> lookupOnOfficial(final int... ids) {
        final List<Item> items = new ArrayList<>(ids.length);
        for (int itemId : ids) {
            try {
                final HttpRequest request = HttpRequest.newBuilder().GET()
                    .uri(URI.create(OFFICIAL_LOOKUP + itemId))
                    .build();

                HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
                JsonElement jsonElement;
                JsonObject jsonObject;
                boolean unexpectedFormat = false;
                jsonElement = JsonParser.parseString(response.body());
                if (!jsonElement.isJsonObject()) {
                    unexpectedFormat = true;
                } else {
                    jsonObject = jsonElement.getAsJsonObject();
                    if (!jsonObject.has("item")) {
                        unexpectedFormat = true;
                    } else {
                        jsonElement = jsonObject.get("item");
                        if (!jsonElement.isJsonObject()) {
                            unexpectedFormat = true;
                        } else {
                            items.add(
                                new Item(Item.Source.OFFICIAL, jsonElement.getAsJsonObject()));
                        }
                    }
                }
                if (unexpectedFormat && Environment.isSDK()) {
                    System.err.println("[SDK Debug] Failed to get the price of " + itemId +
                        " in the official item database. The response was in an unexpected format.");
                }
            } catch (final IOException | JsonSyntaxException | InterruptedException e) {
                if (Environment.isSDK()) {
                    System.err.println("[SDK Debug] Failed to lookup the price of " + itemId +
                        " in the official item database. Ensure you're not trying to lookup an untradeable or noted item.");
                }
            }
        }
        return items;
    }

    /**
     * Looks up item prices on the grand exchange wiki which as of 7/6/21 relies on data
     * supplied by RuneLite.
     */
    private static List<Item> lookupOnWiki(final int... ids) {
        final List<Item> items = new ArrayList<>(ids.length);
        for (int itemId : ids) {
            try {
                final HttpRequest request = HttpRequest.newBuilder().GET()
                    .uri(URI.create(WIKI_LOOKUP + itemId))
                    .build();

                HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
                JsonElement jsonElement;
                JsonObject jsonObject;
                boolean unexpectedFormat = false;
                jsonElement = JsonParser.parseString(response.body());
                if (!jsonElement.isJsonObject()) {
                    unexpectedFormat = true;
                } else {
                    jsonObject = jsonElement.getAsJsonObject();
                    if (jsonObject.keySet().size() != 1 || !jsonObject.has("data")) {
                        unexpectedFormat = true;
                    } else {
                        jsonElement = jsonObject.get("data");
                        if (jsonElement == null || !jsonElement.isJsonObject()) {
                            unexpectedFormat = true;
                        } else {
                            items.add(new Item(Item.Source.WIKI, jsonElement.getAsJsonObject()));
                        }
                    }
                }
                if (unexpectedFormat && Environment.isSDK()) {
                    System.err.println("[SDK Debug] Failed to get the price of " + itemId +
                        " in the Wiki's item database. The response was in an unexpected format.");
                }
            } catch (final IOException | JsonSyntaxException | InterruptedException e) {
                if (Environment.isSDK()) {
                    System.err.println("[SDK Debug] Failed to lookup the price of " + itemId +
                        " in the Wiki's item database. Ensure you're not trying to lookup an untradeable or noted item.");
                }
            }
        }
        return items;
    }

    public static boolean open() {
        return open(Npcs.newQuery().names("Grand Exchange clerk", "Grand Exchange Clerk")
            .actions("Exchange").visible().results().nearest(), "Exchange");
    }

    public static boolean open(String action) {
        return open(Npcs.newQuery().names("Grand Exchange clerk", "Grand Exchange Clerk")
            .actions("Exchange").visible().results().nearest(), action);
    }

    public static boolean open(Npc clerk, String action) {
        if (isOpen()) {
            return true;
        }
        if (clerk != null && clerk.interact(action)) {
            Player local = Players.getLocal();
            return local != null &&
                Execution.delayUntil(GrandExchange::isOpen, local::isMoving, 1500, 2500);
        }
        return false;
    }

    public static List<Slot> getSlots(final Predicate<GrandExchange.Slot> predicate) {
        return OSRSGrandExchange.getSlots(predicate);
    }

    public static List<Slot> getSlots() {
        return getSlots(null);
    }

    public static Slot getSlot(int i) {
        final List<Slot> slots = getSlots();
        return i >= slots.size() ? null : slots.get(i);
    }

    public static GrandExchangeQueryResults getUnusedSlots() {
        return newQuery().inUse(false).results();
    }

    public static GrandExchangeQueryBuilder newQuery() {
        return new GrandExchangeQueryBuilder();
    }

    public enum Screen {
        OVERVIEW,
        SETUP_BUY_OFFER,
        SETUP_SELL_OFFER,
        ACTIVE_BUY_OFFER,
        ACTIVE_SELL_OFFER
    }

    public static class Offer {

        private final long uid;
        private ItemDefinition itemDefinition;
        private Type type;
        private final OpenGrandExchangeOffer rmi;

        Offer(long uid) {
            this.uid = uid;
            rmi = OpenGrandExchangeOffer.create(uid);
        }


        public ItemDefinition getItem() {
            if (itemDefinition == null) {
                final int itemId = rmi.getItemId();
                if (itemId != 0) {
                    itemDefinition = ItemDefinition.get(itemId);
                }
            }
            return itemDefinition;
        }

        /**
         * @return The total quantity of the item being bought or sold in the offer.
         */

        public int getItemQuantity() {
            return rmi.getTotalOfferQuantity();
        }

        /**
         * @return The price per item
         */

        public int getItemPrice() {
            return rmi.getItemPrice();
        }

        /**
         * @return The total number of coins spent/received so far on the offer.
         */

        public int getWealthTransferred() {
            return rmi.getCoinsExchanged();
        }

        /**
         * @return The total number of items sold/received so far on the offer.
         */

        public int getItemsTransferred() {
            return rmi.getItemsExchanged();
        }

        /**
         * @return From 0-100, the percentage completion of the offer.
         */
        public double getCompletion() {
            return ((double) getItemsTransferred() / (double) getItemQuantity()) * 100;
        }

        /**
         * @return The current state of the offer
         */
        public State getState() {
            return State.get(getRawState());
        }

        /**
         * @return The type of offer, either BUY or SELL
         */
        public Type getType() {
            if (type == null) {
                final int raw = getRawState();
                final State state = State.get(raw);
                if (state != null) {
                    if (state.buyState == raw) {
                        type = Type.BUY;
                    } else {
                        type = Type.SELL;
                    }
                }
            }
            return type;
        }

        public boolean isBuyOffer() {
            return getType() == Type.BUY;
        }

        public boolean isSellOffer() {
            return getType() == Type.SELL;
        }


        private byte getRawState() {
            return (byte) rmi.getState();
        }

        @Override
        public String toString() {
            return String.format(
                "GrandExchange.Offer(type=%s, state=%s, item={%s}, price=%s, quantity=%s, completion=%s)",
                getType(),
                getState(),
                getItem(),
                getItemPrice(),
                getItemQuantity(),
                getCompletion()
            );
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder()
                .append(isBuyOffer())
                .append(getItemQuantity())
                .append(getItemPrice())
                .append(getCompletion())
                .append(getItem() == null ? -1 : getItem().getId())
                .toHashCode();
        }

        @Override
        public boolean equals(Object object) {
            if (!(object instanceof Offer)) {
                return false;
            }
            final Offer argument = (Offer) object;
            return argument.hashCode() == hashCode()
                && Objects.equals(argument.getItem(), getItem())
                && argument.getItemPrice() == getItemPrice()
                && argument.getItemQuantity() == getItemQuantity()
                && argument.isBuyOffer() == isBuyOffer()
                && argument.getCompletion() == getCompletion()
                && argument.getRawState() == getRawState();
        }

        public enum State {
            STARTING(1, 9),
            IN_PROGRESS(2, 10),
            COMPLETING(3, 11),
            CANCELLING(4, 12),
            COMPLETED(5, 13),
            EMPTY(0, 0);

            private final int buyState, sellState;

            State(int buyState, int sellState) {
                this.buyState = buyState;
                this.sellState = sellState;
            }

            public static State get(int id) {
                for (State state : values()) {
                    if (id == state.buyState || id == state.sellState) {
                        return state;
                    }
                }
                return null;
            }

            @Override
            public String toString() {
                return "State." + name();
            }
        }

        public enum Type {
            BUY,
            SELL
        }
    }

    public static final class Item {
        private final int id, price;
        private final boolean membersOnly;
        private final String name, description;
        private final Source source;

        public Item(
            int id, int price, boolean membersOnly, String name, String description,
            Source source
        ) {
            this.id = id;
            this.price = price;
            this.membersOnly = membersOnly;
            this.name = name;
            this.description = description;
            this.source = source;
        }

        private Item(Source source, JsonObject json) {
            if (Objects.equals(source, Source.OFFICIAL)) {
                try {
                    this.source = source;
                    this.id = json.getAsJsonPrimitive("id").getAsInt();
                    this.membersOnly = json.getAsJsonPrimitive("members").getAsBoolean();
                    this.name = json.getAsJsonPrimitive("name").getAsString();
                    this.description = json.getAsJsonPrimitive("description").getAsString();
                    String priceString =
                        json.getAsJsonObject("current").getAsJsonPrimitive("price").getAsString()
                            .trim();
                    int multiplier = 1;
                    if (priceString.endsWith("k")) {
                        multiplier = 1000;
                    } else if (priceString.endsWith("m")) {
                        multiplier = 1000000;
                    } else if (priceString.endsWith("b")) {
                        multiplier = 1000000000;
                    }
                    priceString = priceString.replace(",", "").replace("k", "").replace("m", "")
                        .replace("b", "");
                    this.price = (int) (Double.parseDouble(priceString) * multiplier);
                } catch (Exception e) {
                    throw new JsonSyntaxException(
                        "The response we received from the official GE API is missing essential data.",
                        e
                    );
                }
            } else if (Objects.equals(source, Source.WIKI)) {
                //We verify that there is price data available in the json at an earlier step.
                try {
                    this.source = Source.WIKI;
                    String jsonKey = json.keySet().iterator().next();
                    this.id = Integer.parseInt(jsonKey);
                    json = json.getAsJsonObject(jsonKey);
                    int lowPrice = 0;
                    int highPrice = 0;
                    //The low and high values can be null according to the documentation.
                    if (json.has("low")) {
                        lowPrice = json.getAsJsonPrimitive("low").getAsInt();
                    }
                    if (json.has("high")) {
                        highPrice = json.getAsJsonPrimitive("high").getAsInt();
                    }
                /*
                If the high and low price are both available, average them.
                If only one of the prices are available, assign it.
                if neither prices are available then the price will be set to 0.
                */
                    this.price = Math.min(lowPrice, highPrice) > 0 ? (lowPrice + highPrice) / 2 :
                        Math.max(lowPrice, highPrice);
                    //We don't have access to this data currently but can provide it if the wiki makes the following route official
                    //https://prices.runescape.wiki/api/v1/osrs/mapping
                    //https://oldschool.runescape.wiki/w/RuneScape:Real-time_Prices#Mapping
                    this.description = null;
                    this.name = null;
                    this.membersOnly = false;
                } catch (Exception e) {
                    throw new JsonSyntaxException(
                        "The response we received from the wiki's GE API is missing essential data.",
                        e
                    );
                }
            } else {
                throw new IllegalArgumentException(
                    "Unable to parse item data from the specified source (" + source + ")");
            }
        }

        public String getDescription() {
            return description;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public int getPrice() {
            return price;
        }

        /**
         * Gets the data source that was used for getting this items price
         */
        public Source getSource() {
            return source;
        }

        public boolean isMembersOnly() {
            return membersOnly;
        }

        @Override
        public String toString() {
            return "Item{" +
                "id=" + id +
                ", price=" + price +
                ", name='" + name + '\'' +
                ", source=" + source +
                '}';
        }

        public enum Source {
            OFFICIAL,
            WIKI
        }
    }

    public static class Slot {
        private final long slot_uid;
        private final int index;

        private Offer offer;

        public Slot(long slot_uid, int index) {
            this.slot_uid = slot_uid;
            this.index = index;
        }

        public Offer getOffer() {
            if (offer == null) {
                offer = new GrandExchange.Offer(slot_uid);
            }
            return offer.getRawState() != 0 ? offer : null;
        }

        /**
         * @return true if there is an active offer in the Slot
         */
        public boolean inUse() {
            final Offer offer = getOffer();
            return offer != null && offer.getItem() != null;
        }

        public int getIndex() {
            return index;
        }

        @Override
        public String toString() {
            return String.format("GrandExchange.Slot(index=%s, offer={%s})", index, getOffer());
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder().append(index).append(getOffer()).toHashCode();
        }

        @Override
        public boolean equals(Object object) {
            if (!(object instanceof Slot)) {
                return false;
            }
            final Slot argument = (Slot) object;
            return argument.hashCode() == hashCode()
                && argument.getIndex() == getIndex();
        }
    }
}
