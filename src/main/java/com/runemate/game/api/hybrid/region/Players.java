package com.runemate.game.api.hybrid.region;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.osrs.region.*;
import java.util.function.*;
import javax.annotation.*;

/**
 * For retrieval, sorting, and analysis of Players
 */
public final class Players {
    private Players() {
    }

    /**
     * Gets the player at the given index in the array (advanced)
     */
    public static Player getAt(final int index) {
        return OSRSPlayers.getAt(index);
    }

    /**
     * Gets a list of all loaded players interacting with a specific Actor (Npc/Player)
     */
    public static LocatableEntityQueryResults<Player> getLoaded(final Actor target) {
        return getLoaded(player -> {
            final Actor playerTarget = player.getTarget();
            return playerTarget != null && playerTarget.equals(target);
        });
    }

    public static LocatableEntityQueryResults<Player> getLoaded(
        final Predicate<? super Player> filter
    ) {
        return OSRSPlayers.getLoaded(filter);
    }

    public static LocatableEntityQueryResults<Player> getLoaded(final String... names) {
        return getLoaded(getNamePredicate(names));
    }

    /**
     * Gets a List of all loaded players
     */
    public static LocatableEntityQueryResults<Player> getLoaded() {
        return getLoaded((Predicate<? super Player>) null);
    }

    /**
     * Gets the local player, also known as your character.
     */
    @Nullable
    public static Player getLocal() {
        return OSRSPlayers.getLocal();
    }

    /**
     * Gets the weight of the local player as it appears in-game
     */
    public static int getLocalPlayerWeight() {
        return OpenPlayer.getWeight();
    }

    /**
     * Gets a Player model hash filter
     */
    public static Predicate<Player> getModelPredicate(final int... acceptedHashes) {
        return player -> {
            final Model model = player.getModel();
            if (model != null) {
                final int hash = model.hashCode();
                for (int accepted : acceptedHashes) {
                    if (hash == accepted) {
                        return true;
                    }
                }
            }
            return false;
        };
    }

    public static Predicate<Player> getNamePredicate(final String... acceptedNames) {
        return player -> {
            final String name = player.getName();
            if (name != null) {
                for (final String acceptedName : acceptedNames) {
                    if (name.equals(acceptedName)) {
                        return true;
                    }
                }
            }
            return false;
        };
    }

    public static PlayerQueryBuilder newQuery() {
        return new PlayerQueryBuilder();
    }

}
