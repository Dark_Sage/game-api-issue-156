package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.cache.item.*;
import java.util.*;

public class EnumDefinitionLoader extends SerializedFileLoader<CacheEnumDefinition> {

    public EnumDefinitionLoader(int archive) {
        super(archive);
    }

    @Override
    protected CacheEnumDefinition construct(int entry, int file, Map<String, Object> arguments) {
        return new CacheEnumDefinition(file);
    }
}
