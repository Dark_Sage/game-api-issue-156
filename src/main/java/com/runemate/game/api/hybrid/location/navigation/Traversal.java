package com.runemate.game.api.hybrid.location.navigation;

import com.runemate.client.boot.*;
import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.osrs.location.navigation.*;
import com.runemate.game.api.osrs.region.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.annotations.*;
import java.util.*;
import java.util.regex.*;
import javax.annotation.*;

public final class Traversal {

    private static final Pattern STAMINA_ENHANCERS =
        Pattern.compile("^Stamina (mix|potion)\\([1-4]\\)$");
    private static Web defaultWeb;

    private Traversal() {
    }

    /**
     * Gets the default web for the current game mode.
     */
    @Nullable
    public static Web getDefaultWeb() {
        if (defaultWeb != null) {
            return defaultWeb;
        }
        final ServiceLoader<IWeb> iweb = ServiceLoader.load(IWeb.class, Boot.getResourceClassLoader());
        for (IWeb web : iweb) {
            defaultWeb = (Web) web;
        }
        return defaultWeb;
    }

    /**
     * Gets the local players walking destination (the minimap flags coordinate)
     */

    @Nullable
    public static Coordinate getDestination() {
        final int destX = OpenClient.getDestinationX(), destY = OpenClient.getDestinationY();
        if (destX > 0 && destY > 0) {
            final Coordinate base = OSRSRegion.getBase();
            if (base != null) {
                return base.derive(destX, destY);
            }
        }
        return null;
    }


    public static boolean isRunEnabled() {
        return OSRSTraversal.isRunEnabled();
    }

    /**
     * @return Run energy as a percentage
     */
    public static int getRunEnergy() {
        return OSRSTraversal.getRunEnergy();
    }

    public static boolean toggleRun() {
        return OSRSTraversal.toggleRun();
    }

    /**
     * Determines if your player's stamina is currently enhanced due to the usage of a stamina potion (OSRS only)
     *
     * @return true if your stamina (run regeneration rate) is currently boosted because of a stamina potion.
     */
    @OSRSOnly
    public static boolean isStaminaEnhanced() {
        if (Environment.isOSRS()) {
            Varbit staminaPotionVarbit = Varbits.load(25);
            if (staminaPotionVarbit != null) {
                return staminaPotionVarbit.getValue() == 1;
            }
        }
        return false;
    }

    /**
     * Drinks any stamina potions or mixes in your inventory (OSRS only)
     *
     * @return true is a stamina enhancer became active.
     */
    @OSRSOnly
    public static boolean drinkStaminaEnhancer(boolean delayUntilStaminaIsEnhanced) {
        if (Environment.isOSRS()) {
            SpriteItem staminaPotion =
                Inventory.newQuery().names(STAMINA_ENHANCERS).unnoted().actions("Drink").results()
                    .sortByDistanceFromMouse().first();
            if (staminaPotion != null && staminaPotion.interact("Drink")) {
                return !delayUntilStaminaIsEnhanced ||
                    Execution.delayUntil(() -> !staminaPotion.isValid() || isStaminaEnhanced(), 900,
                        1800
                    );
            }
        }
        return false;
    }

    /**
     * Drinks any stamina potions or mixes in your inventory (OSRS only)
     *
     * @return true is a stamina enhancer became active.
     */
    @OSRSOnly
    public static boolean drinkStaminaEnhancer() {
        return drinkStaminaEnhancer(true);
    }

    @OSRSOnly
    static boolean hasStaminaEnhancer() {
        return !Inventory.newQuery().names(STAMINA_ENHANCERS).unnoted().actions("Drink").results()
            .isEmpty();
    }

    @RS3Only
    public static boolean isResting() {
        //TODO use interface component sprite ids to detect this since cosmetic overrides can result in these changing.
        Player local = Players.getLocal();
        if (local != null) {
            int stanceId = local.getStanceId();
            return stanceId == 22641 || stanceId == 22640 || stanceId == 22637 ||
                stanceId == 22635 || stanceId == 22634;
        }
        return false;
    }

    @RS3Only
    public boolean rest() {
        InterfaceComponent ic =
            Interfaces.newQuery().containers(1465).types(InterfaceComponent.Type.SPRITE)
                .grandchildren(false).actions("Turn run mode off", "Rest").results().first();
        return ic != null && ic.interact("Rest") &&
            Execution.delayUntil(Traversal::isResting, 1800, 2400);
    }
}