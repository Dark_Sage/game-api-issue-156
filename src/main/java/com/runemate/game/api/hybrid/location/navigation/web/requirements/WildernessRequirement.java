package com.runemate.game.api.hybrid.location.navigation.web.requirements;

import com.runemate.game.api.hybrid.local.*;
import java.io.*;
import lombok.*;

public class WildernessRequirement extends WebRequirement implements SerializableRequirement {
    private int minimumLevel;
    private int maximumLevel;

    public WildernessRequirement(int maximumLevel) {
        this(0, maximumLevel);
    }

    public WildernessRequirement(int minimumLevel, int maximumLevel) {
        this.minimumLevel = minimumLevel;
        this.maximumLevel = maximumLevel;
    }

    public WildernessRequirement(int protocol, ObjectInput stream) {
        super(protocol, stream);
    }

    public int getMinimumLevel() {
        return minimumLevel;
    }

    public int getMaximumLevel() {
        return maximumLevel;
    }

    @Override
    public boolean isMet0() {
        short wildernessDepth = Wilderness.getDepth();
        return wildernessDepth >= minimumLevel && wildernessDepth <= maximumLevel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof WildernessRequirement) {
            WildernessRequirement that = (WildernessRequirement) o;
            return minimumLevel == that.minimumLevel && maximumLevel == that.maximumLevel;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = minimumLevel;
        result = 31 * result + maximumLevel;
        return result;
    }

    @Override
    public String toString() {
        return "WildernessRequirement{minimumLevel=" + minimumLevel + ", maximumLevel=" +
            maximumLevel + "}";
    }

    @Override
    public int getOpcode() {
        return 5;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeShort(minimumLevel);
        stream.writeShort(maximumLevel);
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        minimumLevel = stream.readShort();
        maximumLevel = stream.readShort();
        return true;
    }
}
