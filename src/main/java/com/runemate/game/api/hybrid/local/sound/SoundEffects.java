package com.runemate.game.api.hybrid.local.sound;

import com.runemate.client.game.open.*;
import com.runemate.game.api.osrs.local.sound.*;
import java.util.*;

public class SoundEffects {

    public static List<EmittedSoundEffect> getEmittingSoundEffects() {
        List<Long> list = OpenSoundEffect.getLoaded();
        List<EmittedSoundEffect> emittedSoundEffects = new ArrayList<>(list.size());
        for (Long uid : list) {
            emittedSoundEffects.add(new OSRSEmittedSoundEffect(uid));
        }
        return emittedSoundEffects;
    }
}
