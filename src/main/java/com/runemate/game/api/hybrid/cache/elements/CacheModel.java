package com.runemate.game.api.hybrid.cache.elements;

import com.google.common.hash.*;
import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.loaders.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.cache.*;
import com.runemate.game.cache.io.*;
import com.runemate.game.cache.item.*;
import java.awt.*;
import java.io.*;
import java.util.List;
import java.util.*;

@SuppressWarnings({ "MismatchedReadAndWriteOfArray", "FieldCanBeLocal" })
public class CacheModel implements Validatable, DecodedItem {
    private static final ModelLoader OSRS_LOADER = new ModelLoader();
    private final boolean rs3;
    public int vertexCount;
    protected int[] aIndices, bIndices, cIndices;
    protected int id;
    protected int version;
    protected int[] xVertices, yVertices, zVertices;
    int[][] ab_osrs_$;
    int[][] ao_osrs_$;
    private int[] triangleSkinValues;
    private short[] texTriangleZ;
    private byte[] faceRenderPriorities;
    private byte[] faceRenderType;
    private byte[] textureCoords;
    @SuppressWarnings("unused")
    private int triangleFaceCount;
    @SuppressWarnings("unused")
    private byte priority;
    private short[] texTriangleY;
    private byte[] textureRenderTypes;
    private int textureTriangleCount;
    private byte[] faceAlphas;
    private short[] faceColors;
    private int[] triangleSkins;
    private short[] faceTextures;
    private int triangleCount;
    private int[] vertexSkins;
    private short[] texTriangleX;
    private int[] af_rs3_$;
    private int[] ah_rs3_$;
    private short[] al_rs3_$;
    @SuppressWarnings("unused")
    private int am_rs3_$;
    private int[] an_rs3_$;
    private int[] ao_rs3_$;
    private byte[] ap_rs3_$;
    private byte[] as_rs3_$;
    private short[] at_rs3_$;
    private int[] au_rs3_$;
    private int[] av_rs3_$;
    private short[] aw_rs3_$;
    private byte[] d_rs3_$;
    private byte[] g_rs3_$;
    private int h_rs3_$;
    private short[] i_rs3_$;
    private float[] j_rs3_$;
    private int[] r_rs3_$;
    @SuppressWarnings("unused")
    private int t_osrs_$;
    private float[] u_rs3_$;
    private byte[] v_rs3_$;
    private byte[] w_rs3_$;
    private int y_rs3_$;

    public CacheModel(int id) {
        this.rs3 = false;
        this.id = id;
    }

    public static CacheModel load(final int id) {
        return load(JS5CacheController.getLargestJS5CacheController(), id);
    }

    public static CacheModel load(JS5CacheController controller, final int id) {
        if (id >= 0) {
            try {
                return OSRS_LOADER.load(controller, id, 0);
            } catch (final Throwable ioe) {
                System.err.println("Unable to load cache model for " + id + ": \"" +
                    ioe.getClass().getSimpleName() + " : " + ioe.getMessage() + '"');
                ioe.printStackTrace();
            }
        }
        return null;
    }

    public static List<CacheModel> loadAll(Collection<Integer> ids) {
        if (ids.size() == 0) {
            return Collections.emptyList();
        }
        List<CacheModel> modelComponentList = new ArrayList<>(ids.size());
        JS5CacheController controller = JS5CacheController.getLargestJS5CacheController();
        ids.forEach(id -> {
            CacheModel component = load(controller, id);
            if (component != null) {
                modelComponentList.add(component);
            }
        });
        return modelComponentList;
    }

    public static List<CacheModel> loadAll(final int[] ids) {
        if (ids.length == 0) {
            return Collections.emptyList();
        }
        List<CacheModel> modelComponentList = new ArrayList<>(ids.length);
        JS5CacheController controller = JS5CacheController.getLargestJS5CacheController();
        for (int id : ids) {
            CacheModel component = load(controller, id);
            if (component != null) {
                modelComponentList.add(component);
            }
        }
        return modelComponentList;
    }

    public static List<CacheModel> loadAll() {
        JS5CacheController cache = JS5CacheController.getLargestJS5CacheController();
        int[] groups = OSRS_LOADER.groups(cache);
        List<CacheModel> models = new ArrayList<>(groups.length);
        for (int group : groups) {
            try {
                models.add(OSRS_LOADER.load(cache, group, 0));
            } catch (final Throwable ioe) {
                System.err.println("Unable to load cache model for " + group + ": \"" +
                    ioe.getClass().getSimpleName() + " : " + ioe.getMessage() + '"');
                ioe.printStackTrace();
            }
        }
        return models;
    }

    public static Map<Integer, byte[]> retrieveAll() {
        JS5CacheController cache = JS5CacheController.getLargestJS5CacheController();
        int[] groups = OSRS_LOADER.groups(cache);
        Map<Integer, byte[]> modelFiles = new HashMap<>(groups.length);
        for (int group : groups) {
            try {
                modelFiles.put(group, OSRS_LOADER.retrieve(cache, group, 0));
            } catch (final Throwable ioe) {
                System.err.println("Unable to retrieve cache model for " + group + ": \"" +
                    ioe.getClass().getSimpleName() + " : " + ioe.getMessage() + '"');
                ioe.printStackTrace();
            }
        }
        return modelFiles;
    }

    public void translate(int[] offsets) {
        translate(offsets[0], offsets[1], offsets[2]);
    }

    @Override
    public void decode(Js5InputStream stream) throws IOException {
        if (rs3) {
            decodeRS3(stream);
        } else {
            byte[] bytes = stream.getBackingArray();
            if (bytes[bytes.length - 1] == -3 && bytes[bytes.length - 2] == -1) {
                load4(bytes);
            } else if (bytes[bytes.length - 1] == -2 && bytes[bytes.length - 2] == -1) {
                load3(bytes);
            } else if (bytes[bytes.length - 1] == -1 && bytes[bytes.length - 2] == -1) {
                load2(bytes);
            } else {
                load1(bytes);
            }
        }
    }

    /*
       Must use a set since there are sooo many polygons
     */
    public Set<Color> getColors() {
        Set<Color> colors = new HashSet<>(faceColors.length);
        for (int color : faceColors) {
            colors.add(RSColors.fromHSV(color));
        }
        return colors;
    }

    public int getId() {
        return id;
    }

    @SuppressWarnings("UnstableApiUsage")
    @Override
    public int hashCode() {
        final Hasher hasher = Hashing.md5().newHasher();
        for (final int a : aIndices) {
            hasher.putInt(a);
        }
        for (final int b : bIndices) {
            hasher.putInt(b);
        }
        for (final int c : cIndices) {
            hasher.putInt(c);
        }
        for (final int x : xVertices) {
            hasher.putInt(x);
        }
        for (final int y : yVertices) {
            hasher.putInt(y);
        }
        for (final int z : zVertices) {
            hasher.putInt(z);
        }
        return hasher.hash().asInt();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CacheModel that = (CacheModel) o;
        return vertexCount == that.vertexCount
            && Arrays.equals(aIndices, that.aIndices)
            && Arrays.equals(bIndices, that.bIndices)
            && Arrays.equals(cIndices, that.cIndices)
            && Arrays.equals(xVertices, that.xVertices)
            && Arrays.equals(yVertices, that.yVertices)
            && Arrays.equals(zVertices, that.zVertices);
    }

    @Override
    public String toString() {
        return "CacheModel{" +
            "version=" + version +
            ", zVertices=" + Arrays.toString(zVertices) +
            ", yVertices=" + Arrays.toString(yVertices) +
            ", xVertices=" + Arrays.toString(xVertices) +
            '}';
    }

    @Override
    public boolean isValid() {
        return xVertices != null && aIndices != null;
    }

    public void translate(int xOff, int yOff, int zOff) {
        for (int index = 0; index < vertexCount; index++) {
            xVertices[index] += xOff;
            yVertices[index] += yOff;
            zVertices[index] += zOff;
        }
    }

    @SuppressWarnings("DuplicatedCode")
    private void load1(byte[] var1) {
        boolean var2 = false;
        boolean var3 = false;
        JByteBuffer var4 = new JByteBuffer(var1);
        JByteBuffer var5 = new JByteBuffer(var1);
        JByteBuffer var6 = new JByteBuffer(var1);
        JByteBuffer var7 = new JByteBuffer(var1);
        JByteBuffer var8 = new JByteBuffer(var1);
        var4.setPosition(var1.length - 18);
        int var9 = var4.readUnsignedShort();
        int var10 = var4.readUnsignedShort();
        int var11 = var4.readUnsignedByte();
        int var12 = var4.readUnsignedByte();
        int var13 = var4.readUnsignedByte();
        int var14 = var4.readUnsignedByte();
        int var15 = var4.readUnsignedByte();
        int var16 = var4.readUnsignedByte();
        int var17 = var4.readUnsignedShort();
        int var18 = var4.readUnsignedShort();
        /*int var19 = */
        var4.readUnsignedShort();
        int var20 = var4.readUnsignedShort();
        int var21 = 0;
        int var45 = var21 + var9;
        int var23 = var45;
        var45 += var10;
        int var24 = var45;
        if (var13 == 255) {
            var45 += var10;
        }

        int var25 = var45;
        if (var15 == 1) {
            var45 += var10;
        }

        int var26 = var45;
        if (var12 == 1) {
            var45 += var10;
        }

        int var27 = var45;
        if (var16 == 1) {
            var45 += var9;
        }

        int var28 = var45;
        if (var14 == 1) {
            var45 += var10;
        }

        int var29 = var45;
        var45 += var20;
        int var30 = var45;
        var45 += var10 * 2;
        int var31 = var45;
        var45 += var11 * 6;
        int var32 = var45;
        var45 += var17;
        int var33 = var45;
        var45 += var18;
//        int var10000 = var45 + var19;
        this.vertexCount = var9;
        this.triangleFaceCount = var10;
        this.t_osrs_$ = var11;
        this.xVertices = new int[var9];
        this.yVertices = new int[var9];
        this.zVertices = new int[var9];
        this.aIndices = new int[var10];
        this.bIndices = new int[var10];
        this.cIndices = new int[var10];
        if (var11 > 0) {
            this.textureRenderTypes = new byte[var11];
            this.texTriangleX = new short[var11];
            this.texTriangleY = new short[var11];
            this.texTriangleZ = new short[var11];
        }

        if (var16 == 1) {
            this.vertexSkins = new int[var9];
        }

        if (var12 == 1) {
            this.faceRenderType = new byte[var10];
            this.textureCoords = new byte[var10];
            this.faceTextures = new short[var10];
        }

        if (var13 == 255) {
            this.faceRenderPriorities = new byte[var10];
        } else {
            this.priority = (byte) var13;
        }

        if (var14 == 1) {
            this.faceAlphas = new byte[var10];
        }

        if (var15 == 1) {
            this.triangleSkinValues = new int[var10];
        }

        this.faceColors = new short[var10];
        var4.setPosition(var21);
        var5.setPosition(var32);
        var6.setPosition(var33);
        var7.setPosition(var45);
        var8.setPosition(var27);
        int var35 = 0;
        int var36 = 0;
        int var37 = 0;

        int ivar1;
        int ivar2;
        int ivar3;
        int ivar4;
        for (int i = 0; i < var9; ++i) {
            ivar1 = var4.readUnsignedByte();
            ivar2 = 0;
            if ((ivar1 & 1) != 0) {
                ivar2 = var5.readQuarterBoundSmart();
            }

            ivar3 = 0;
            if ((ivar1 & 2) != 0) {
                ivar3 = var6.readQuarterBoundSmart();
            }

            ivar4 = 0;
            if ((ivar1 & 4) != 0) {
                ivar4 = var7.readQuarterBoundSmart();
            }

            this.xVertices[i] = var35 + ivar2;
            this.yVertices[i] = var36 + ivar3;
            this.zVertices[i] = var37 + ivar4;
            var35 = this.xVertices[i];
            var36 = this.yVertices[i];
            var37 = this.zVertices[i];
            if (var16 == 1) {
                this.vertexSkins[i] = var8.readUnsignedByte();
            }
        }

        var4.setPosition(var30);
        var5.setPosition(var26);
        var6.setPosition(var24);
        var7.setPosition(var28);
        var8.setPosition(var25);

        int jvar1;
        for (int j = 0; j < var10; ++j) {
            this.faceColors[j] = (short) var4.readUnsignedShort();
            if (var12 == 1) {
                jvar1 = var5.readUnsignedByte();
                if ((jvar1 & 1) == 1) {
                    this.faceRenderType[j] = 1;
                    var2 = true;
                } else {
                    this.faceRenderType[j] = 0;
                }

                if ((jvar1 & 2) == 2) {
                    this.textureCoords[j] = (byte) (jvar1 >> 2);
                    this.faceTextures[j] = this.faceColors[j];
                    this.faceColors[j] = 127;
                    if (this.faceTextures[j] != -1) {
                        var3 = true;
                    }
                } else {
                    this.textureCoords[j] = -1;
                    this.faceTextures[j] = -1;
                }
            }

            if (var13 == 255) {
                this.faceRenderPriorities[j] = var6.readByte();
            }

            if (var14 == 1) {
                this.faceAlphas[j] = var7.readByte();
            }

            if (var15 == 1) {
                this.triangleSkinValues[j] = var8.readUnsignedByte();
            }
        }

        var4.setPosition(var29);
        var5.setPosition(var23);
        int var38 = 0;
        int var39 = 0;
        int var40 = 0;
        int var41 = 0;

        int kvar1;
        int kvar2;
        for (int k = 0; k < var10; ++k) {
            kvar1 = var5.readUnsignedByte();
            if (kvar1 == 1) {
                var38 = var4.readQuarterBoundSmart() + var41;
                var39 = var4.readQuarterBoundSmart() + var38;
                var40 = var4.readQuarterBoundSmart() + var39;
                var41 = var40;
                this.aIndices[k] = var38;
                this.bIndices[k] = var39;
                this.cIndices[k] = var40;
            }

            if (kvar1 == 2) {
                var39 = var40;
                var40 = var4.readQuarterBoundSmart() + var41;
                var41 = var40;
                this.aIndices[k] = var38;
                this.bIndices[k] = var39;
                this.cIndices[k] = var40;
            }

            if (kvar1 == 3) {
                var38 = var40;
                var40 = var4.readQuarterBoundSmart() + var41;
                var41 = var40;
                this.aIndices[k] = var38;
                this.bIndices[k] = var39;
                this.cIndices[k] = var40;
            }

            if (kvar1 == 4) {
                kvar2 = var38;
                var38 = var39;
                var39 = kvar2;
                var40 = var4.readQuarterBoundSmart() + var41;
                var41 = var40;
                this.aIndices[k] = var38;
                this.bIndices[k] = var39;
                this.cIndices[k] = var40;
            }
        }

        var4.setPosition(var31);

        for (int l = 0; l < var11; ++l) {
            this.textureRenderTypes[l] = 0;
            this.texTriangleX[l] = (short) var4.readUnsignedShort();
            this.texTriangleY[l] = (short) var4.readUnsignedShort();
            this.texTriangleZ[l] = (short) var4.readUnsignedShort();
        }

        if (this.textureCoords != null) {
            boolean mvar1 = false;
            int mvar2;
            for (int m = 0; m < var10; ++m) {
                mvar2 = this.textureCoords[m] & 255;
                if (mvar2 != 255) {
                    if (this.aIndices[m] == (this.texTriangleX[mvar2] & '\uffff') &&
                        this.bIndices[m] == (this.texTriangleY[mvar2] & '\uffff') &&
                        this.cIndices[m] == (this.texTriangleZ[mvar2] & '\uffff')) {
                        this.textureCoords[m] = -1;
                    } else {
                        mvar1 = true;
                    }
                }
            }

            if (!mvar1) {
                this.textureCoords = null;
            }
        }

        if (!var3) {
            this.faceTextures = null;
        }

        if (!var2) {
            this.faceRenderType = null;
        }
    }

    @SuppressWarnings("DuplicatedCode")
    private void load2(byte[] var1) {
        JByteBuffer var2 = new JByteBuffer(var1);
        JByteBuffer var3 = new JByteBuffer(var1);
        JByteBuffer var4 = new JByteBuffer(var1);
        JByteBuffer var5 = new JByteBuffer(var1);
        JByteBuffer var6 = new JByteBuffer(var1);
        JByteBuffer var7 = new JByteBuffer(var1);
        JByteBuffer var8 = new JByteBuffer(var1);
        var2.setPosition(var1.length - 23);
        int var9 = var2.readUnsignedShort();
        int var10 = var2.readUnsignedShort();
        int var11 = var2.readUnsignedByte();
        int var12 = var2.readUnsignedByte();
        int var13 = var2.readUnsignedByte();
        int var14 = var2.readUnsignedByte();
        int var15 = var2.readUnsignedByte();
        int var16 = var2.readUnsignedByte();
        int var17 = var2.readUnsignedByte();
        int var18 = var2.readUnsignedShort();
        int var19 = var2.readUnsignedShort();
        int var20 = var2.readUnsignedShort();
        int var21 = var2.readUnsignedShort();
        int var22 = var2.readUnsignedShort();
        int var23 = 0;
        int var24 = 0;
        int var25 = 0;
        if (var11 > 0) {
            this.textureRenderTypes = new byte[var11];
            var2.setPosition(0);

            byte ivar1;
            for (int i = 0; i < var11; ++i) {
                ivar1 = this.textureRenderTypes[i] = var2.readByte();
                if (ivar1 == 0) {
                    ++var23;
                }

                if (ivar1 >= 1 && ivar1 <= 3) {
                    ++var24;
                }

                if (ivar1 == 2) {
                    ++var25;
                }
            }
        }

        int var26 = var11 + var9;
        int var28 = var26;
        if (var12 == 1) {
            var26 += var10;
        }

        int var29 = var26;
        var26 += var10;
        int var30 = var26;
        if (var13 == 255) {
            var26 += var10;
        }

        int var31 = var26;
        if (var15 == 1) {
            var26 += var10;
        }

        int var32 = var26;
        if (var17 == 1) {
            var26 += var9;
        }

        int var33 = var26;
        if (var14 == 1) {
            var26 += var10;
        }

        int var34 = var26;
        var26 += var21;
        int var35 = var26;
        if (var16 == 1) {
            var26 += var10 * 2;
        }

        int var36 = var26;
        var26 += var22;
        int var37 = var26;
        var26 += var10 * 2;
        int var38 = var26;
        var26 += var18;
        int var39 = var26;
        var26 += var19;
        int var40 = var26;
        var26 += var20;
        int var41 = var26;
        var26 += var23 * 6;
        int var42 = var26;
        var26 += var24 * 6;
        int var43 = var26;
        var26 += var24 * 6;
        int var44 = var26;
        var26 += var24 * 2;
        int var45 = var26;
        var26 += var24;
        int var46 = var26;
        var26 += var24 * 2 + var25 * 2;
        this.vertexCount = var9;
        this.triangleFaceCount = var10;
        this.t_osrs_$ = var11;
        this.xVertices = new int[var9];
        this.yVertices = new int[var9];
        this.zVertices = new int[var9];
        this.aIndices = new int[var10];
        this.bIndices = new int[var10];
        this.cIndices = new int[var10];
        if (var17 == 1) {
            this.vertexSkins = new int[var9];
        }

        if (var12 == 1) {
            this.faceRenderType = new byte[var10];
        }

        if (var13 == 255) {
            this.faceRenderPriorities = new byte[var10];
        } else {
            this.priority = (byte) var13;
        }

        if (var14 == 1) {
            this.faceAlphas = new byte[var10];
        }

        if (var15 == 1) {
            this.triangleSkinValues = new int[var10];
        }

        if (var16 == 1) {
            this.faceTextures = new short[var10];
        }

        if (var16 == 1 && var11 > 0) {
            this.textureCoords = new byte[var10];
        }

        this.faceColors = new short[var10];
        if (var11 > 0) {
            this.texTriangleX = new short[var11];
            this.texTriangleY = new short[var11];
            this.texTriangleZ = new short[var11];
        }

        var2.setPosition(var11);
        var3.setPosition(var38);
        var4.setPosition(var39);
        var5.setPosition(var40);
        var6.setPosition(var32);
        int var48 = 0;
        int var49 = 0;
        int var50 = 0;

        int jvar1;
        int jvar2;
        int jvar3;
        int jvar4;
        for (int j = 0; j < var9; ++j) {
            jvar1 = var2.readUnsignedByte();
            jvar2 = 0;
            if ((jvar1 & 1) != 0) {
                jvar2 = var3.readQuarterBoundSmart();
            }

            jvar3 = 0;
            if ((jvar1 & 2) != 0) {
                jvar3 = var4.readQuarterBoundSmart();
            }

            jvar4 = 0;
            if ((jvar1 & 4) != 0) {
                jvar4 = var5.readQuarterBoundSmart();
            }

            this.xVertices[j] = var48 + jvar2;
            this.yVertices[j] = var49 + jvar3;
            this.zVertices[j] = var50 + jvar4;
            var48 = this.xVertices[j];
            var49 = this.yVertices[j];
            var50 = this.zVertices[j];
            if (var17 == 1) {
                this.vertexSkins[j] = var6.readUnsignedByte();
            }
        }

        var2.setPosition(var37);
        var3.setPosition(var28);
        var4.setPosition(var30);
        var5.setPosition(var33);
        var6.setPosition(var31);
        var7.setPosition(var35);
        var8.setPosition(var36);

        for (int k = 0; k < var10; ++k) {
            this.faceColors[k] = (short) var2.readUnsignedShort();
            if (var12 == 1) {
                this.faceRenderType[k] = var3.readByte();
            }

            if (var13 == 255) {
                this.faceRenderPriorities[k] = var4.readByte();
            }

            if (var14 == 1) {
                this.faceAlphas[k] = var5.readByte();
            }

            if (var15 == 1) {
                this.triangleSkinValues[k] = var6.readUnsignedByte();
            }

            if (var16 == 1) {
                this.faceTextures[k] = (short) (var7.readUnsignedShort() - 1);
            }

            if (this.textureCoords != null && this.faceTextures[k] != -1) {
                this.textureCoords[k] = (byte) (var8.readUnsignedByte() - 1);
            }
        }

        var2.setPosition(var34);
        var3.setPosition(var29);
        int var51 = 0;
        int var52 = 0;
        int var53 = 0;
        int var54 = 0;

        int lvar1;
        int lvar2;
        for (int l = 0; l < var10; ++l) {
            lvar1 = var3.readUnsignedByte();
            if (lvar1 == 1) {
                var51 = var2.readQuarterBoundSmart() + var54;
                var52 = var2.readQuarterBoundSmart() + var51;
                var53 = var2.readQuarterBoundSmart() + var52;
                var54 = var53;
                this.aIndices[l] = var51;
                this.bIndices[l] = var52;
                this.cIndices[l] = var53;
            }

            if (lvar1 == 2) {
                var52 = var53;
                var53 = var2.readQuarterBoundSmart() + var54;
                var54 = var53;
                this.aIndices[l] = var51;
                this.bIndices[l] = var52;
                this.cIndices[l] = var53;
            }

            if (lvar1 == 3) {
                var51 = var53;
                var53 = var2.readQuarterBoundSmart() + var54;
                var54 = var53;
                this.aIndices[l] = var51;
                this.bIndices[l] = var52;
                this.cIndices[l] = var53;
            }

            if (lvar1 == 4) {
                lvar2 = var51;
                var51 = var52;
                var52 = lvar2;
                var53 = var2.readQuarterBoundSmart() + var54;
                var54 = var53;
                this.aIndices[l] = var51;
                this.bIndices[l] = var52;
                this.cIndices[l] = var53;
            }
        }

        var2.setPosition(var41);
        var3.setPosition(var42);
        var4.setPosition(var43);
        var5.setPosition(var44);
        var6.setPosition(var45);
        var7.setPosition(var46);

        int mvar1;
        for (int m = 0; m < var11; ++m) {
            mvar1 = this.textureRenderTypes[m] & 255;
            if (mvar1 == 0) {
                this.texTriangleX[m] = (short) var2.readUnsignedShort();
                this.texTriangleY[m] = (short) var2.readUnsignedShort();
                this.texTriangleZ[m] = (short) var2.readUnsignedShort();
            }
        }

        var2.setPosition(var26);
        int var55 = var2.readUnsignedByte();
        if (var55 != 0) {
            var2.readUnsignedShort();
            var2.readUnsignedShort();
            var2.readUnsignedShort();
            var2.readInteger();
        }
    }

    @SuppressWarnings("DuplicatedCode")
    private void load3(byte[] var1) {
        boolean var2 = false;
        boolean var3 = false;
        JByteBuffer var4 = new JByteBuffer(var1);
        JByteBuffer var5 = new JByteBuffer(var1);
        JByteBuffer var6 = new JByteBuffer(var1);
        JByteBuffer var7 = new JByteBuffer(var1);
        JByteBuffer var8 = new JByteBuffer(var1);
        var4.setPosition(var1.length - 23);
        int var9 = var4.readUnsignedShort();
        int var10 = var4.readUnsignedShort();
        int var11 = var4.readUnsignedByte();
        int var12 = var4.readUnsignedByte();
        int var13 = var4.readUnsignedByte();
        int var14 = var4.readUnsignedByte();
        int var15 = var4.readUnsignedByte();
        int var16 = var4.readUnsignedByte();
        int var17 = var4.readUnsignedByte();
        int var18 = var4.readUnsignedShort();
        int var19 = var4.readUnsignedShort();
        /*int var20 = */
        var4.readUnsignedShort();
        int var21 = var4.readUnsignedShort();
        int var22 = var4.readUnsignedShort();
        int var23 = 0;
        int var47 = var23 + var9;
        int var25 = var47;
        var47 += var10;
        int var26 = var47;
        if (var13 == 255) {
            var47 += var10;
        }

        int var27 = var47;
        if (var15 == 1) {
            var47 += var10;
        }

        int var28 = var47;
        if (var12 == 1) {
            var47 += var10;
        }

        int var29 = var47;
        var47 += var22;
        int var30 = var47;
        if (var14 == 1) {
            var47 += var10;
        }

        int var31 = var47;
        var47 += var21;
        int var32 = var47;
        var47 += var10 * 2;
        int var33 = var47;
        var47 += var11 * 6;
        int var34 = var47;
        var47 += var18;
        int var35 = var47;
        var47 += var19;
//        int var10000 = var47 + var20;
        this.vertexCount = var9;
        this.triangleFaceCount = var10;
        this.t_osrs_$ = var11;
        this.xVertices = new int[var9];
        this.yVertices = new int[var9];
        this.zVertices = new int[var9];
        this.aIndices = new int[var10];
        this.bIndices = new int[var10];
        this.cIndices = new int[var10];
        if (var11 > 0) {
            this.textureRenderTypes = new byte[var11];
            this.texTriangleX = new short[var11];
            this.texTriangleY = new short[var11];
            this.texTriangleZ = new short[var11];
        }

        if (var16 == 1) {
            this.vertexSkins = new int[var9];
        }

        if (var12 == 1) {
            this.faceRenderType = new byte[var10];
            this.textureCoords = new byte[var10];
            this.faceTextures = new short[var10];
        }

        if (var13 == 255) {
            this.faceRenderPriorities = new byte[var10];
        } else {
            this.priority = (byte) var13;
        }

        if (var14 == 1) {
            this.faceAlphas = new byte[var10];
        }

        if (var15 == 1) {
            this.triangleSkinValues = new int[var10];
        }

        if (var17 == 1) {
            this.ao_osrs_$ = new int[var9][];
            this.ab_osrs_$ = new int[var9][];
        }

        this.faceColors = new short[var10];
        var4.setPosition(var23);
        var5.setPosition(var34);
        var6.setPosition(var35);
        var7.setPosition(var47);
        var8.setPosition(var29);
        int var37 = 0;
        int var38 = 0;
        int var39 = 0;

        int ivar1;
        int ivar2;
        int ivar3;
        int ivar4;
        for (int i = 0; i < var9; ++i) {
            ivar1 = var4.readUnsignedByte();
            ivar2 = 0;
            if ((ivar1 & 1) != 0) {
                ivar2 = var5.readQuarterBoundSmart();
            }

            ivar3 = 0;
            if ((ivar1 & 2) != 0) {
                ivar3 = var6.readQuarterBoundSmart();
            }

            ivar4 = 0;
            if ((ivar1 & 4) != 0) {
                ivar4 = var7.readQuarterBoundSmart();
            }

            this.xVertices[i] = var37 + ivar2;
            this.yVertices[i] = var38 + ivar3;
            this.zVertices[i] = var39 + ivar4;
            var37 = this.xVertices[i];
            var38 = this.yVertices[i];
            var39 = this.zVertices[i];
            if (var16 == 1) {
                this.vertexSkins[i] = var8.readUnsignedByte();
            }
        }

        if (var17 == 1) {
            int jvar2;
            for (int j = 0; j < var9; ++j) {
                jvar2 = var8.readUnsignedByte();
                this.ao_osrs_$[j] = new int[jvar2];
                this.ab_osrs_$[j] = new int[jvar2];

                for (int k = 0; k < jvar2; ++k) {
                    this.ao_osrs_$[j][k] = var8.readUnsignedByte();
                    this.ab_osrs_$[j][k] = var8.readUnsignedByte();
                }
            }
        }

        var4.setPosition(var32);
        var5.setPosition(var28);
        var6.setPosition(var26);
        var7.setPosition(var30);
        var8.setPosition(var27);

        int lvar1;
        for (int l = 0; l < var10; ++l) {
            this.faceColors[l] = (short) var4.readUnsignedShort();
            if (var12 == 1) {
                lvar1 = var5.readUnsignedByte();
                if ((lvar1 & 1) == 1) {
                    this.faceRenderType[l] = 1;
                    var2 = true;
                } else {
                    this.faceRenderType[l] = 0;
                }

                if ((lvar1 & 2) == 2) {
                    this.textureCoords[l] = (byte) (lvar1 >> 2);
                    this.faceTextures[l] = this.faceColors[l];
                    this.faceColors[l] = 127;
                    if (this.faceTextures[l] != -1) {
                        var3 = true;
                    }
                } else {
                    this.textureCoords[l] = -1;
                    this.faceTextures[l] = -1;
                }
            }

            if (var13 == 255) {
                this.faceRenderPriorities[l] = var6.readByte();
            }

            if (var14 == 1) {
                this.faceAlphas[l] = var7.readByte();
            }

            if (var15 == 1) {
                this.triangleSkinValues[l] = var8.readUnsignedByte();
            }
        }

        var4.setPosition(var31);
        var5.setPosition(var25);
        int var40 = 0;
        int var41 = 0;
        int var42 = 0;
        int var43 = 0;

        int mvar1;
        int mvar2;
        for (int m = 0; m < var10; ++m) {
            mvar1 = var5.readUnsignedByte();
            if (mvar1 == 1) {
                var40 = var4.readQuarterBoundSmart() + var43;
                var41 = var4.readQuarterBoundSmart() + var40;
                var42 = var4.readQuarterBoundSmart() + var41;
                var43 = var42;
                this.aIndices[m] = var40;
                this.bIndices[m] = var41;
                this.cIndices[m] = var42;
            }

            if (mvar1 == 2) {
                var41 = var42;
                var42 = var4.readQuarterBoundSmart() + var43;
                var43 = var42;
                this.aIndices[m] = var40;
                this.bIndices[m] = var41;
                this.cIndices[m] = var42;
            }

            if (mvar1 == 3) {
                var40 = var42;
                var42 = var4.readQuarterBoundSmart() + var43;
                var43 = var42;
                this.aIndices[m] = var40;
                this.bIndices[m] = var41;
                this.cIndices[m] = var42;
            }

            if (mvar1 == 4) {
                mvar2 = var40;
                var40 = var41;
                var41 = mvar2;
                var42 = var4.readQuarterBoundSmart() + var43;
                var43 = var42;
                this.aIndices[m] = var40;
                this.bIndices[m] = var41;
                this.cIndices[m] = var42;
            }
        }

        var4.setPosition(var33);

        for (int n = 0; n < var11; ++n) {
            this.textureRenderTypes[n] = 0;
            this.texTriangleX[n] = (short) var4.readUnsignedShort();
            this.texTriangleY[n] = (short) var4.readUnsignedShort();
            this.texTriangleZ[n] = (short) var4.readUnsignedShort();
        }

        if (this.textureCoords != null) {
            boolean ovar1 = false;
            int ovar2;
            for (int o = 0; o < var10; ++o) {
                ovar2 = this.textureCoords[o] & 255;
                if (ovar2 != 255) {
                    if (this.aIndices[o] == (this.texTriangleX[ovar2] & '\uffff') &&
                        this.bIndices[o] == (this.texTriangleY[ovar2] & '\uffff') &&
                        this.cIndices[o] == (this.texTriangleZ[ovar2] & '\uffff')) {
                        this.textureCoords[o] = -1;
                    } else {
                        ovar1 = true;
                    }
                }
            }

            if (!ovar1) {
                this.textureCoords = null;
            }
        }

        if (!var3) {
            this.faceTextures = null;
        }

        if (!var2) {
            this.faceRenderType = null;
        }

    }

    @SuppressWarnings("DuplicatedCode")
    private void load4(byte[] var1) {
        JByteBuffer var2 = new JByteBuffer(var1);
        JByteBuffer var3 = new JByteBuffer(var1);
        JByteBuffer var4 = new JByteBuffer(var1);
        JByteBuffer var5 = new JByteBuffer(var1);
        JByteBuffer var6 = new JByteBuffer(var1);
        JByteBuffer var7 = new JByteBuffer(var1);
        JByteBuffer var8 = new JByteBuffer(var1);
        var2.setPosition(var1.length - 26);
        int var9 = var2.readUnsignedShort();
        int var10 = var2.readUnsignedShort();
        int var11 = var2.readUnsignedByte();
        int var12 = var2.readUnsignedByte();
        int var13 = var2.readUnsignedByte();
        int var14 = var2.readUnsignedByte();
        int var15 = var2.readUnsignedByte();
        int var16 = var2.readUnsignedByte();
        int var17 = var2.readUnsignedByte();
        int var18 = var2.readUnsignedByte();
        int var19 = var2.readUnsignedShort();
        int var20 = var2.readUnsignedShort();
        int var21 = var2.readUnsignedShort();
        int var22 = var2.readUnsignedShort();
        int var23 = var2.readUnsignedShort();
        int var24 = var2.readUnsignedShort();
        int var25 = 0;
        int var26 = 0;
        int var27 = 0;
        if (var11 > 0) {
            this.textureRenderTypes = new byte[var11];
            var2.setPosition(0);

            byte ivar1;
            for (int i = 0; i < var11; ++i) {
                ivar1 = this.textureRenderTypes[i] = var2.readByte();
                if (ivar1 == 0) {
                    ++var25;
                }

                if (ivar1 >= 1 && ivar1 <= 3) {
                    ++var26;
                }

                if (ivar1 == 2) {
                    ++var27;
                }
            }
        }

        int var28 = var11 + var9;
        int var30 = var28;
        if (var12 == 1) {
            var28 += var10;
        }

        int var31 = var28;
        var28 += var10;
        int var32 = var28;
        if (var13 == 255) {
            var28 += var10;
        }

        int var33 = var28;
        if (var15 == 1) {
            var28 += var10;
        }

        int var34 = var28;
        var28 += var24;
        int var35 = var28;
        if (var14 == 1) {
            var28 += var10;
        }

        int var36 = var28;
        var28 += var22;
        int var37 = var28;
        if (var16 == 1) {
            var28 += var10 * 2;
        }

        int var38 = var28;
        var28 += var23;
        int var39 = var28;
        var28 += var10 * 2;
        int var40 = var28;
        var28 += var19;
        int var41 = var28;
        var28 += var20;
        int var42 = var28;
        var28 += var21;
        int var43 = var28;
        var28 += var25 * 6;
        int var44 = var28;
        var28 += var26 * 6;
        int var45 = var28;
        var28 += var26 * 6;
        int var46 = var28;
        var28 += var26 * 2;
        int var47 = var28;
        var28 += var26;
        int var48 = var28;
        var28 += var26 * 2 + var27 * 2;
        this.vertexCount = var9;
        this.triangleFaceCount = var10;
        this.textureTriangleCount = var11;
        this.xVertices = new int[var9];
        this.yVertices = new int[var9];
        this.zVertices = new int[var9];
        this.aIndices = new int[var10];
        this.bIndices = new int[var10];
        this.cIndices = new int[var10];
        if (var17 == 1) {
            this.vertexSkins = new int[var9];
        }

        if (var12 == 1) {
            this.faceRenderType = new byte[var10];
        }

        if (var13 == 255) {
            this.faceRenderPriorities = new byte[var10];
        } else {
            this.priority = (byte) var13;
        }

        if (var14 == 1) {
            this.faceAlphas = new byte[var10];
        }

        if (var15 == 1) {
            this.triangleSkinValues = new int[var10];
        }

        if (var16 == 1) {
            this.faceTextures = new short[var10];
        }

        if (var16 == 1 && var11 > 0) {
            this.textureCoords = new byte[var10];
        }

        if (var18 == 1) {
            this.ao_osrs_$ = new int[var9][];
            this.ab_osrs_$ = new int[var9][];
        }

        this.faceColors = new short[var10];
        if (var11 > 0) {
            this.texTriangleX = new short[var11];
            this.texTriangleY = new short[var11];
            this.texTriangleZ = new short[var11];
        }

        var2.setPosition(var11);
        var3.setPosition(var40);
        var4.setPosition(var41);
        var5.setPosition(var42);
        var6.setPosition(var34);
        int var50 = 0;
        int var51 = 0;
        int var52 = 0;

        int jvar1;
        int jvar2;
        int jvar3;
        int jvar4;
        for (int j = 0; j < var9; ++j) {
            jvar1 = var2.readUnsignedByte();
            jvar2 = 0;
            if ((jvar1 & 1) != 0) {
                jvar2 = var3.readQuarterBoundSmart();
            }

            jvar3 = 0;
            if ((jvar1 & 2) != 0) {
                jvar3 = var4.readQuarterBoundSmart();
            }

            jvar4 = 0;
            if ((jvar1 & 4) != 0) {
                jvar4 = var5.readQuarterBoundSmart();
            }

            this.xVertices[j] = var50 + jvar2;
            this.yVertices[j] = var51 + jvar3;
            this.zVertices[j] = var52 + jvar4;
            var50 = this.xVertices[j];
            var51 = this.yVertices[j];
            var52 = this.zVertices[j];
            if (var17 == 1) {
                this.vertexSkins[j] = var6.readUnsignedByte();
            }
        }

        if (var18 == 1) {
            int kvar1;
            for (int k = 0; k < var9; ++k) {
                kvar1 = var6.readUnsignedByte();
                this.ao_osrs_$[k] = new int[kvar1];
                this.ab_osrs_$[k] = new int[kvar1];

                for (int l = 0; l < kvar1; ++l) {
                    this.ao_osrs_$[k][l] = var6.readUnsignedByte();
                    this.ab_osrs_$[k][l] = var6.readUnsignedByte();
                }
            }
        }

        var2.setPosition(var39);
        var3.setPosition(var30);
        var4.setPosition(var32);
        var5.setPosition(var35);
        var6.setPosition(var33);
        var7.setPosition(var37);
        var8.setPosition(var38);

        for (int m = 0; m < var10; ++m) {
            this.faceColors[m] = (short) var2.readUnsignedShort();
            if (var12 == 1) {
                this.faceRenderType[m] = var3.readByte();
            }

            if (var13 == 255) {
                this.faceRenderPriorities[m] = var4.readByte();
            }

            if (var14 == 1) {
                this.faceAlphas[m] = var5.readByte();
            }

            if (var15 == 1) {
                this.triangleSkinValues[m] = var6.readUnsignedByte();
            }

            if (var16 == 1) {
                this.faceTextures[m] = (short) (var7.readUnsignedShort() - 1);
            }

            if (this.textureCoords != null && this.faceTextures[m] != -1) {
                this.textureCoords[m] = (byte) (var8.readUnsignedByte() - 1);
            }
        }

        var2.setPosition(var36);
        var3.setPosition(var31);
        int var53 = 0;
        int var54 = 0;
        int var55 = 0;
        int var56 = 0;

        int nvar1;
        int nvar2;
        for (int n = 0; n < var10; ++n) {
            nvar1 = var3.readUnsignedByte();
            if (nvar1 == 1) {
                var53 = var2.readQuarterBoundSmart() + var56;
                var54 = var2.readQuarterBoundSmart() + var53;
                var55 = var2.readQuarterBoundSmart() + var54;
                var56 = var55;
                this.aIndices[n] = var53;
                this.bIndices[n] = var54;
                this.cIndices[n] = var55;
            }

            if (nvar1 == 2) {
                var54 = var55;
                var55 = var2.readQuarterBoundSmart() + var56;
                var56 = var55;
                this.aIndices[n] = var53;
                this.bIndices[n] = var54;
                this.cIndices[n] = var55;
            }

            if (nvar1 == 3) {
                var53 = var55;
                var55 = var2.readQuarterBoundSmart() + var56;
                var56 = var55;
                this.aIndices[n] = var53;
                this.bIndices[n] = var54;
                this.cIndices[n] = var55;
            }

            if (nvar1 == 4) {
                nvar2 = var53;
                var53 = var54;
                var54 = nvar2;
                var55 = var2.readQuarterBoundSmart() + var56;
                var56 = var55;
                this.aIndices[n] = var53;
                this.bIndices[n] = var54;
                this.cIndices[n] = var55;
            }
        }

        var2.setPosition(var43);
        var3.setPosition(var44);
        var4.setPosition(var45);
        var5.setPosition(var46);
        var6.setPosition(var47);
        var7.setPosition(var48);

        int ovar1;
        for (int o = 0; o < var11; ++o) {
            ovar1 = this.textureRenderTypes[o] & 255;
            if (ovar1 == 0) {
                this.texTriangleX[o] = (short) var2.readUnsignedShort();
                this.texTriangleY[o] = (short) var2.readUnsignedShort();
                this.texTriangleZ[o] = (short) var2.readUnsignedShort();
            }
        }

        var2.setPosition(var28);
        int var57 = var2.readUnsignedByte();
        if (var57 != 0) {
//            new ModelData0();
            var2.readUnsignedShort();
            var2.readUnsignedShort();
            var2.readUnsignedShort();
            var2.readUnsignedInteger();
        }

    }

    @SuppressWarnings("DuplicatedCode")
    private void decodeRS3(Js5InputStream stream) {
        byte[] bytes = stream.getBackingArray();
        version = 12;
        vertexCount = 0;
        h_rs3_$ = 0;
        triangleCount = 0;
        am_rs3_$ = 0;
        textureTriangleCount = 0;
        final JByteBuffer byteBuffer = new JByteBuffer(bytes);
        final JByteBuffer byteBuffer2 = new JByteBuffer(bytes);
        final JByteBuffer byteBuffer3 = new JByteBuffer(bytes);
        final JByteBuffer byteBuffer4 = new JByteBuffer(bytes);
        final JByteBuffer byteBuffer5 = new JByteBuffer(bytes);
        final JByteBuffer byteBuffer6 = new JByteBuffer(bytes);
        final JByteBuffer byteBuffer7 = new JByteBuffer(bytes);
        final int identifier = byteBuffer.readUnsignedByte();
        if (identifier != 1) {
            throw new IllegalStateException("Invalid model id: " + identifier);
        }
        byteBuffer.readUnsignedByte();
        version = byteBuffer.readUnsignedByte();
        byteBuffer.setPosition((bytes.length - 26));
        vertexCount = byteBuffer.readUnsignedShort();
        triangleCount = byteBuffer.readUnsignedShort();
        textureTriangleCount = byteBuffer.readUnsignedShort();
        final int footerFlags = byteBuffer.readUnsignedByte();
        final boolean hasFillAttributes = (footerFlags & 0x1) == 0x1;
        final boolean hasSurfaceEffects = (footerFlags & 0x2) == 0x2;
        final boolean hasVertexNormals = (footerFlags & 0x4) == 0x4;
        final boolean hasManyVertices = (footerFlags & 0x10) == 0x10;
        final boolean hasManyTriangles = (footerFlags & 0x20) == 0x20;
        final boolean hasManyVertexNormals = (footerFlags & 0x40) == 0x40;
        final boolean hasUVCoordinates = (footerFlags & 0x80) == 0x80;
        final int priority = byteBuffer.readUnsignedByte();
        final int hasTriangleAlphas = byteBuffer.readUnsignedByte();
        final int hasTriangleSkins = byteBuffer.readUnsignedByte();
        final int isTextured = byteBuffer.readUnsignedByte();
        final int hasVertexSkins = byteBuffer.readUnsignedByte();
        final int ai = byteBuffer.readUnsignedShort();
        final int ai2 = byteBuffer.readUnsignedShort();
        final int ai3 = byteBuffer.readUnsignedShort();
        final int ai4 = byteBuffer.readUnsignedShort();
        final int ai5 = byteBuffer.readUnsignedShort();
        int verticeCount = byteBuffer.readUnsignedShort();
        int triangleCount = byteBuffer.readUnsignedShort();
        if (!hasManyVertices) {
            if (hasVertexSkins == 1) {
                verticeCount = vertexCount;
            } else {
                verticeCount = 0;
            }
        }
        if (!hasManyTriangles) {
            if (hasTriangleSkins == 1) {
                triangleCount = this.triangleCount;
            } else {
                triangleCount = 0;
            }
        }
        int textureAmount = 0;
        int particleAmount = 0;
        int particleColor = 0;
        if (textureTriangleCount > 0) {
            textureRenderTypes = new byte[textureTriangleCount];
            byteBuffer.setPosition(3);
            for (int i = 0; i < textureTriangleCount; ++i) {
                final byte renderType = byteBuffer.readByte();
                textureRenderTypes[i] = renderType;
                if (renderType == 0) {
                    ++textureAmount;
                }
                if (renderType >= 1 && renderType <= 3) {
                    ++particleAmount;
                }
                if (renderType == 2) {
                    ++particleColor;
                }
            }
        }
        final int n9;
        final int n8;
        int n7 = n8 = (n9 = 3 + textureTriangleCount) + vertexCount;
        if (hasFillAttributes) {
            n7 += this.triangleCount;
        }
        final int n10 = n7;
        final int n12;
        int n11 = n12 = n7 + this.triangleCount;
        if (priority == 255) {
            n11 += this.triangleCount;
        }
        final int n13 = n11;
        final int n16;
        final int n15;
        int n14 = n15 = (n16 = n11 + triangleCount) + verticeCount;
        if (hasTriangleAlphas == 1) {
            n14 += this.triangleCount;
        }
        final int n17 = n14;
        final int n19;
        int n18 = n19 = n14 + ai4;
        if (isTextured == 1) {
            n18 += this.triangleCount << 1;
        }
        final int n20 = n18;
        final int n27;
        final int n26;
        final int n25;
        final int n24;
        final int n23;
        final int n22;
        final int n21 = (
            n22 = (
                n23 =
                    (n24 = (n25 = (n26 = (n27 = n18 + ai5) + (this.triangleCount << 1)) + ai) + ai2) +
                        ai3
            ) + textureAmount * 6
        ) + particleAmount * 6;
        int scale = 6;
        if (version == 14) {
            scale = 7;
        } else if (version >= 15) {
            scale = 9;
        }
        final int n33;
        final int n32;
        final int n31;
        final int n30 =
            (n31 = (n32 = (n33 = n21 + particleAmount * scale) + particleAmount) + particleAmount) +
                (particleAmount + (particleColor << 1));
        int length = bytes.length;
        int length2 = bytes.length;
        int length3 = bytes.length;
        int length4 = bytes.length;
        if (hasUVCoordinates) {
            final JByteBuffer byteBuffer8 = new JByteBuffer(bytes);
            byteBuffer8.setPosition((bytes.length - 26));
            byteBuffer8.setPosition(
                byteBuffer8.getPosition() - bytes[byteBuffer8.getPosition() - 1]);
            y_rs3_$ = byteBuffer8.readUnsignedShort();
            final int ai6 = byteBuffer8.readUnsignedShort();
            final int ai7 = byteBuffer8.readUnsignedShort();
            length = n30 + ai6;
            length2 = length + ai7;
            length3 = length2 + vertexCount;
            length4 = length3 + (y_rs3_$ << 1);
        }
        xVertices = new int[vertexCount];
        yVertices = new int[vertexCount];
        zVertices = new int[vertexCount];
        aIndices = new int[this.triangleCount];
        bIndices = new int[this.triangleCount];
        cIndices = new int[this.triangleCount];
        if (hasVertexSkins == 1) {
            vertexSkins = new int[vertexCount];
        }
        if (hasFillAttributes) {
            faceRenderType = new byte[this.triangleCount];
        }
        if (priority == 255) {
            v_rs3_$ = new byte[this.triangleCount];
        } else {
            am_rs3_$ = (byte) priority;
        }
        if (hasTriangleAlphas == 1) {
            faceAlphas = new byte[this.triangleCount];
        }
        if (hasTriangleSkins == 1) {
            triangleSkins = new int[this.triangleCount];
        }
        if (isTextured == 1) {
            faceTextures = new short[this.triangleCount];
        }
        if (isTextured == 1 && (textureTriangleCount > 0 || y_rs3_$ > 0)) {
            i_rs3_$ = new short[this.triangleCount];
        }
        faceColors = new short[this.triangleCount];
        if (textureTriangleCount > 0) {
            at_rs3_$ = new short[textureTriangleCount];
            al_rs3_$ = new short[textureTriangleCount];
            aw_rs3_$ = new short[textureTriangleCount];
            if (particleAmount > 0) {
                af_rs3_$ = new int[particleAmount];
                av_rs3_$ = new int[particleAmount];
                au_rs3_$ = new int[particleAmount];
                ap_rs3_$ = new byte[particleAmount];
                as_rs3_$ = new byte[particleAmount];
                ah_rs3_$ = new int[particleAmount];
            }
            if (particleColor > 0) {
                ao_rs3_$ = new int[particleColor];
                an_rs3_$ = new int[particleColor];
            }
        }
        byteBuffer.setPosition(n9);
        byteBuffer2.setPosition(n26);
        byteBuffer3.setPosition(n25);
        byteBuffer4.setPosition(n24);
        byteBuffer5.setPosition(n16);
        int n34 = 0;
        int n35 = 0;
        int n36 = 0;
        for (int index = 0; index < vertexCount; ++index) {
            final int vertexMask = byteBuffer.readUnsignedByte();
            int xoff = 0;
            if ((vertexMask & 0x1) != 0x0) {
                xoff = byteBuffer2.readQuarterBoundSmart();
            }
            int yoff = 0;
            if ((vertexMask & 0x2) != 0x0) {
                yoff = byteBuffer3.readQuarterBoundSmart();
            }
            int zoff = 0;
            if ((vertexMask & 0x4) != 0x0) {
                zoff = byteBuffer4.readQuarterBoundSmart();
            }
            xVertices[index] = n34 + xoff;
            yVertices[index] = n35 + yoff;
            zVertices[index] = n36 + zoff;
            n34 = xVertices[index];
            n35 = yVertices[index];
            n36 = zVertices[index];
            if (hasVertexSkins == 1) {
                if (hasManyVertices) {
                    vertexSkins[index] = byteBuffer5.readReducedShortSmart();
                } else {
                    vertexSkins[index] = byteBuffer5.readUnsignedByte();
                    if (vertexSkins[index] == 255) {
                        vertexSkins[index] = -1;
                    }
                }
            }
        }
        if (y_rs3_$ > 0) {
            byteBuffer.setPosition(length2);
            byteBuffer2.setPosition(length3);
            byteBuffer3.setPosition(length4);
            r_rs3_$ = new int[vertexCount];
            int k = 0;
            int n37 = 0;
            while (k < vertexCount) {
                r_rs3_$[k] = n37;
                n37 += byteBuffer.readUnsignedByte();
                ++k;
            }
            w_rs3_$ = new byte[this.triangleCount];
            d_rs3_$ = new byte[this.triangleCount];
            g_rs3_$ = new byte[this.triangleCount];
            j_rs3_$ = new float[y_rs3_$];
            u_rs3_$ = new float[y_rs3_$];
            for (int l = 0; l < y_rs3_$; ++l) {
                j_rs3_$[l] = byteBuffer2.readShort() / 4096.0f;
                u_rs3_$[l] = byteBuffer3.readShort() / 4096.0f;
            }
        }
        byteBuffer.setPosition(n27);
        byteBuffer2.setPosition(n8);
        byteBuffer3.setPosition(n12);
        byteBuffer4.setPosition(n15);
        byteBuffer5.setPosition(n13);
        byteBuffer6.setPosition(n19);
        byteBuffer7.setPosition(n20);
        for (int n38 = 0; n38 < this.triangleCount; ++n38) {
            faceColors[n38] = (short) byteBuffer.readUnsignedShort();
            if (hasFillAttributes) {
                faceRenderType[n38] = byteBuffer2.readByte();
            }
            if (priority == 255) {
                v_rs3_$[n38] = byteBuffer3.readByte();
            }
            if (hasTriangleAlphas == 1) {
                faceAlphas[n38] = byteBuffer4.readByte();
            }
            if (hasTriangleSkins == 1) {
                if (hasManyTriangles) {
                    triangleSkins[n38] = byteBuffer5.readReducedShortSmart();
                } else {
                    triangleSkins[n38] = byteBuffer5.readUnsignedByte();
                    if (triangleSkins[n38] == 255) {
                        triangleSkins[n38] = -1;
                    }
                }
            }
            if (isTextured == 1) {
                faceTextures[n38] = (short) (byteBuffer6.readUnsignedShort() - 1);
            }
            if (i_rs3_$ != null) {
                if (faceTextures[n38] != -1) {
                    i_rs3_$[n38] = version >= 16 ? (short) (byteBuffer7.readShortSmart() - 1) :
                        (short) (byteBuffer7.readUnsignedByte() - 1);
                } else {
                    i_rs3_$[n38] = -1;
                }
            }
        }
        h_rs3_$ = -1;
        byteBuffer.setPosition(n17);
        byteBuffer2.setPosition(n10);
        byteBuffer3.setPosition(length);
        init(byteBuffer, byteBuffer2, byteBuffer3);
        byteBuffer.setPosition(n23);
        byteBuffer2.setPosition(n22);
        byteBuffer3.setPosition(n21);
        byteBuffer4.setPosition(n33);
        byteBuffer5.setPosition(n32);
        byteBuffer6.setPosition(n31);
        k(byteBuffer, byteBuffer2, byteBuffer3, byteBuffer4, byteBuffer5, byteBuffer6);
        byteBuffer.setPosition(n30);
    }

    @SuppressWarnings("DuplicatedCode")
    private void init(final JByteBuffer jbuf1, final JByteBuffer jbuf2, final JByteBuffer jbuf3) {
        short h_rs3_$ = 0;
        int h_rs3_$2 = 0;
        int n = 0;
        int n2 = 0;
        for (int i = 0; i < this.triangleCount; ++i) {
            final int ag = jbuf2.readUnsignedByte();
            final int n3 = ag & 0x7;
            if (n3 == 1) {
                final int[] aIndices = this.aIndices;
                h_rs3_$ = (short) (jbuf1.readQuarterBoundSmart() + n2);
                aIndices[i] = h_rs3_$;
                final short n5 = h_rs3_$;
                final int[] e_rs3_$ = this.bIndices;
                h_rs3_$2 = (short) (jbuf1.readQuarterBoundSmart() + n5);
                e_rs3_$[i] = (short) h_rs3_$2;
                final int n7 = h_rs3_$2;
                final int[] q_rs3_$ = this.cIndices;
                n = (short) (jbuf1.readQuarterBoundSmart() + n7);
                q_rs3_$[i] = (short) n;
                n2 = n;
                if (h_rs3_$ > this.h_rs3_$) {
                    this.h_rs3_$ = h_rs3_$;
                }
                if (h_rs3_$2 > this.h_rs3_$) {
                    this.h_rs3_$ = h_rs3_$2;
                }
                if (n > this.h_rs3_$) {
                    this.h_rs3_$ = n;
                }
            }
            if (n3 == 2) {
                h_rs3_$2 = n;
                n = (n2 = (short) (jbuf1.readQuarterBoundSmart() + n2));
                this.aIndices[i] = h_rs3_$;
                this.bIndices[i] = (short) h_rs3_$2;
                if ((this.cIndices[i] = (short) n) > this.h_rs3_$) {
                    this.h_rs3_$ = n;
                }
            }
            if (n3 == 3) {
                h_rs3_$ = (short) n;
                n = (n2 = (short) (jbuf1.readQuarterBoundSmart() + n2));
                this.aIndices[i] = h_rs3_$;
                this.bIndices[i] = (short) h_rs3_$2;
                if ((this.cIndices[i] = (short) n) > this.h_rs3_$) {
                    this.h_rs3_$ = n;
                }
            }
            if (n3 == 4) {
                final short n9 = h_rs3_$;
                h_rs3_$ = (short) h_rs3_$2;
                h_rs3_$2 = n9;
                n = (n2 = (short) (jbuf1.readQuarterBoundSmart() + n2));
                this.aIndices[i] = h_rs3_$;
                this.bIndices[i] = (short) h_rs3_$2;
                if ((this.cIndices[i] = (short) n) > this.h_rs3_$) {
                    this.h_rs3_$ = n;
                }
            }
            if (this.y_rs3_$ > 0 && (ag & 0x8) != 0x0) {
                this.w_rs3_$[i] = (byte) jbuf3.readUnsignedByte();
                this.d_rs3_$[i] = (byte) jbuf3.readUnsignedByte();
                this.g_rs3_$[i] = (byte) jbuf3.readUnsignedByte();
            }
        }
        ++this.h_rs3_$;
    }

    private void k(
        JByteBuffer jbuf1, JByteBuffer jbuf2, JByteBuffer jbuf3, JByteBuffer jbuf4,
        JByteBuffer jbuf5, JByteBuffer jbuf6
    ) {
        for (int i = 0; i < this.textureTriangleCount; ++i) {
            final int n = this.textureRenderTypes[i] & 0xFF;
            if (n == 0) {
                this.at_rs3_$[i] = (short) jbuf1.readUnsignedShort();
                this.al_rs3_$[i] = (short) jbuf1.readUnsignedShort();
                this.aw_rs3_$[i] = (short) jbuf1.readUnsignedShort();
            }
            if (n == 1) {
                this.at_rs3_$[i] = (short) jbuf2.readUnsignedShort();
                this.al_rs3_$[i] = (short) jbuf2.readUnsignedShort();
                this.aw_rs3_$[i] = (short) jbuf2.readUnsignedShort();
                if (this.version < 15) {
                    this.af_rs3_$[i] = jbuf3.readUnsignedShort();
                    this.av_rs3_$[i] =
                        this.version < 14 ? jbuf3.readUnsignedShort() : jbuf3.readUnsignedTribyte();
                    this.au_rs3_$[i] = jbuf3.readUnsignedShort();
                } else {
                    this.af_rs3_$[i] = jbuf3.readUnsignedTribyte();
                    this.av_rs3_$[i] = jbuf3.readUnsignedTribyte();
                    this.au_rs3_$[i] = jbuf3.readUnsignedTribyte();
                }
                this.ap_rs3_$[i] = jbuf4.readByte();
                this.as_rs3_$[i] = jbuf5.readByte();
                this.ah_rs3_$[i] = jbuf6.readByte();
            }
            if (n == 2) {
                this.at_rs3_$[i] = (short) jbuf2.readUnsignedShort();
                this.al_rs3_$[i] = (short) jbuf2.readUnsignedShort();
                this.aw_rs3_$[i] = (short) jbuf2.readUnsignedShort();
                if (this.version < 15) {
                    this.af_rs3_$[i] = jbuf3.readUnsignedShort();
                    this.av_rs3_$[i] =
                        this.version < 14 ? jbuf3.readUnsignedShort() : jbuf3.readUnsignedTribyte();
                    this.au_rs3_$[i] = jbuf3.readUnsignedShort();
                } else {
                    this.af_rs3_$[i] = jbuf3.readUnsignedTribyte();
                    this.av_rs3_$[i] = jbuf3.readUnsignedTribyte();
                    this.au_rs3_$[i] = jbuf3.readUnsignedTribyte();
                }
                this.ap_rs3_$[i] = jbuf4.readByte();
                this.as_rs3_$[i] = jbuf5.readByte();
                this.ah_rs3_$[i] = jbuf6.readByte();
                this.ao_rs3_$[i] = jbuf6.readByte();
                this.an_rs3_$[i] = jbuf6.readByte();
            }
            if (n == 3) {
                this.at_rs3_$[i] = (short) jbuf2.readUnsignedShort();
                this.al_rs3_$[i] = (short) jbuf2.readUnsignedShort();
                this.aw_rs3_$[i] = (short) jbuf2.readUnsignedShort();
                if (this.version < 15) {
                    this.af_rs3_$[i] = jbuf3.readUnsignedShort();
                    this.av_rs3_$[i] =
                        this.version < 14 ? jbuf3.readUnsignedShort() : jbuf3.readUnsignedTribyte();
                    this.au_rs3_$[i] = jbuf3.readUnsignedShort();
                } else {
                    this.af_rs3_$[i] = jbuf3.readUnsignedTribyte();
                    this.av_rs3_$[i] = jbuf3.readUnsignedTribyte();
                    this.au_rs3_$[i] = jbuf3.readUnsignedTribyte();
                }
                this.ap_rs3_$[i] = jbuf4.readByte();
                this.as_rs3_$[i] = jbuf5.readByte();
                this.ah_rs3_$[i] = jbuf6.readByte();
            }
        }
    }
}
