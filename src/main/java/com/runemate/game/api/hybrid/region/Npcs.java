package com.runemate.game.api.hybrid.region;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.osrs.region.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;

/**
 * For retrieval, sorting, and analysis of Npcs
 */
public final class Npcs {
    private Npcs() {
    }

    public static NpcQueryBuilder newQuery() {
        return new NpcQueryBuilder();
    }

    /**
     * Gets all loaded Npcs
     */
    public static LocatableEntityQueryResults<Npc> getLoaded() {
        return getLoaded((Predicate<? super Npc>) null);
    }

    /**
     * Gets all loaded Npcs with the specified ids
     */
    public static LocatableEntityQueryResults<Npc> getLoaded(final int... ids) {
        return getLoaded(getIdPredicate(ids));
    }

    /**
     * Gets all loaded Npcs with the specified names
     */
    public static LocatableEntityQueryResults<Npc> getLoaded(final String... names) {
        return getLoaded(getNamePredicate(names));
    }

    public static LocatableEntityQueryResults<Npc> getLoaded(final Pattern... names) {
        return getLoaded(getNamePredicate(names));
    }

    /**
     * Gets all loaded Npcs interacting with the specified Actor (Npc/Player)
     */
    public static LocatableEntityQueryResults<Npc> getLoaded(final Actor target) {
        return getLoaded(npc -> {
            final Actor npcTarget = npc.getTarget();
            return npcTarget != null && npcTarget.equals(target);
        });
    }

    /**
     * Gets all loaded Npcs that are accepted by the filter
     */
    public static LocatableEntityQueryResults<Npc> getLoaded(final Predicate<? super Npc> filter) {
        return OSRSNpcs.getLoaded(filter);
    }

    public static LocatableEntityQueryResults<Npc> getLoadedOn(
        final Coordinate position,
        final Predicate<? super Npc> filter
    ) {
        return getLoaded(filter != null ? getPositionPredicate(position).and(filter) :
            getPositionPredicate(position));
    }

    public static LocatableEntityQueryResults<Npc> getLoadedOn(final Coordinate position) {
        return getLoadedOn(position, null);
    }

    public static LocatableEntityQueryResults<Npc> getLoadedWithin(final Area area) {
        return getLoadedWithin(area, (Predicate<? super Npc>) null);
    }

    public static LocatableEntityQueryResults<Npc> getLoadedWithin(
        final Area area,
        final Predicate<? super Npc> filter
    ) {
        return getLoaded(
            filter != null ? getAreaPredicate(area).and(filter) : getAreaPredicate(area));
    }

    public static LocatableEntityQueryResults<Npc> getLoadedWithin(
        final Area area,
        final int... ids
    ) {
        return getLoaded(getIdPredicate(ids).and(getAreaPredicate(area)));
    }

    public static LocatableEntityQueryResults<Npc> getLoadedWithin(
        final Area area,
        final String... names
    ) {
        return getLoaded(getNamePredicate(names).and(getAreaPredicate(area)));
    }

    /**
     * Gets the Npc at a given index/with the given uid in the internal table/array.
     */
    public static Npc getAt(final long uid) {
        int index = (int) uid;
        if (index != uid) {
            System.err.println(
                "[ERROR] We lost information converting the npc uid " + uid + " to " + index + ".");
        }
        return OSRSNpcs.getByIndex(index);
    }

    /**
     * Gets a Npc filter that can be used to get a Npc with one of the specified names
     *
     * @param acceptedNames the names that are valid (case-sensitive)
     * @return a filter
     */
    public static Predicate<Npc> getNamePredicate(final String... acceptedNames) {
        return npc -> {
            final String name = npc.getName();
            if (name != null) {
                for (final String acceptedName : acceptedNames) {
                    if (name.equals(acceptedName)) {
                        return true;
                    }
                }
            }
            return false;
        };
    }

    public static Predicate<Npc> getNamePredicate(final Pattern... acceptedNames) {
        return npc -> {
            final String name = npc.getName();
            if (name != null) {
                for (final Pattern acceptedName : acceptedNames) {
                    if (acceptedName.matcher(name).matches()) {
                        return true;
                    }
                }
            }
            return false;
        };
    }

    public static Predicate<Npc> getInteractingEntityPredicate(
        final Actor... acceptedIteractingEntities
    ) {
        return npc -> {
            final Actor interacting = npc.getTarget();
            if (interacting != null) {
                for (final Actor accepted : acceptedIteractingEntities) {
                    if (interacting.equals(accepted)) {
                        return true;
                    }
                }
            }
            return false;
        };
    }

    /**
     * Gets an Npc filter that can be used to get an with one of the specified action
     *
     * @param acceptedActions the powers that are valid (case-sensitive)
     * @return a filter
     */
    public static Predicate<Npc> getActionPredicate(final String... acceptedActions) {
        return npc -> {
            final NpcDefinition def = npc.getDefinition();
            if (def != null) {
                for (final String action : def.getActions()) {
                    for (final String acceptedAction : acceptedActions) {
                        if (action.equals(acceptedAction)) {
                            return true;
                        }
                    }
                }
            }
            return false;
        };
    }

    /**
     * Gets a model hash Npc filter
     */
    public static Predicate<Npc> getModelPredicate(final int... acceptedHashes) {
        return new Predicate<Npc>() {
            private final HashMap<Integer, Integer> idToHash = new HashMap<>();

            @Override
            public boolean test(Npc npc) {
                int objectId = npc.getId();
                if (!idToHash.containsKey(objectId)) {
                    final Model model = npc.getModel();
                    if (model != null) {
                        idToHash.put(objectId, model.hashCode());
                    }
                }
                if (idToHash.containsKey(objectId)) {
                    final int hash = idToHash.get(objectId);
                    for (int accepted : acceptedHashes) {
                        if (hash == accepted) {
                            return true;
                        }
                    }
                }
                return false;
            }
        };
    }

    public static Predicate<Npc> getAppearancePredicate(final int... acceptedAppearanceIds) {
        return npc -> {
            NpcDefinition def = npc.getDefinition();
            if (def != null) {
                for (int appearanceId : def.getAppearance()) {
                    for (int accepted : acceptedAppearanceIds) {
                        if (appearanceId == accepted) {
                            return true;
                        }
                    }
                }
                def = def.getLocalState();
                if (def != null) {
                    for (int appearanceId : def.getAppearance()) {
                        for (int accepted : acceptedAppearanceIds) {
                            if (appearanceId == accepted) {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        };
    }

    public static Predicate<Npc> getPositionPredicate(final Coordinate... coordinates) {
        return npc -> {
            Coordinate position = npc.getPosition();
            if (position != null) {
                for (final Coordinate coordinate : coordinates) {
                    if (coordinate.equals(position)) {
                        return true;
                    }
                }
            }
            return false;
        };
    }

    public static Predicate<Npc> getAreaPredicate(final Area... areas) {
        return npc -> {
            for (final Area area : areas) {
                if (area.contains(npc)) {
                    return true;
                }
            }
            return false;
        };
    }

    public static Predicate<Npc> getIdPredicate(final int... acceptedIds) {
        return npc -> {
            final int id = npc.getId();
            for (int accepted : acceptedIds) {
                if (id == accepted) {
                    return true;
                }
            }
            return false;
        };
    }
}
