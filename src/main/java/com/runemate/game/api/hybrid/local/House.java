package com.runemate.game.api.hybrid.local;


import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.annotations.*;

public final class House {

    private House() {
    }

    public static House.Location getCurrent() {
        Varbit varbit = Varbits.load(2187);
        if (varbit != null) {
            int value = varbit.getValue();
            for (Location location : House.Location.values()) {
                if (location.varbitValue == value) {
                    return location;
                }
            }
        }
        return null;
    }

    /**
     * Checks whether your local player is currently inside a house
     *
     * @return if you're inside
     */
    public static boolean isInside() {
        return !getPortals().isEmpty();
    }

    /**
     * Gets your current servant
     *
     * @return an Npc or null if not available
     */
    public static Npc getServant() {
        return Npcs.newQuery().actions("Talk-to")
            .names("Royal Guard", "Rick", "Maid", "Cook", "Butler", "Demon butler",
                "Mind-controlled monkey butler"
            )
            .results().nearest();
    }

    /**
     * Gets the enterance/exit portal inside the current house
     *
     * @return a portal object
     */
    public static LocatableEntityQueryResults<GameObject> getPortals() {
        return GameObjects.newQuery().types(GameObject.Type.PRIMARY).names("Portal").actions("Lock")
            .results();
    }

    /**
     * Gets a list of altars within the current house
     *
     * @return a list of altars
     */
    public static LocatableEntityQueryResults<GameObject> getAltars() {
        //TODO Specify type to vastly improve the speed of this query
        return GameObjects.newQuery().names("Altar").actions("Pray").results();
    }

    /**
     * Gets the amulet of glory that is mounted on the wall
     *
     * @return a game object
     */
    public static LocatableEntityQueryResults<GameObject> getMountedGlories() {
        //TODO Specify type to vastly improve the speed of this query.
        return GameObjects.newQuery()
            .actions("Remove", "Edgeville", "Karamja", "Draynor", "Al Kharid")
            .names("Amulet of Glory").results();
    }

    /**
     * Gets a list of all lecterns
     *
     * @return a query results
     */
    public static LocatableEntityQueryResults<GameObject> getLecterns() {
        //TODO Specify type to vastly improve the speed of this query.
        return GameObjects.newQuery().actions("Study").names("Lectern").results();
    }

    /**
     * Gets a list of all bell pulls
     *
     * @return the query results
     */
    private static LocatableEntityQueryResults<GameObject> getBellPulls() {
        //TODO Specify type to vastly improve the speed of this query.
        return GameObjects.newQuery().actions("Ring").results();
    }

    public enum Location {
        RIMMINGTON(1, new Coordinate(2954, 3224, 0)),
        TAVERLEY(2, new Coordinate(2893, 3465, 0)),
        POLLNIVNEACH(3, new Coordinate(3339, 3003, 0)),
        RELLEKKA(4),
        BRIMHAVEN(5),
        YANILLE(6),
        KOUREND(8, new Coordinate(1743, 3516, 0));
        private final int varbitValue;
        private final Coordinate teleportDestination;

        Location(int varbitValue) {
            this(varbitValue, null);
        }

        Location(int varbitValue, Coordinate teleportDestination) {
            this.varbitValue = varbitValue;
            this.teleportDestination = teleportDestination;
        }

        public Coordinate getTeleportDestination() {
            return teleportDestination;
        }
    }

    public static class Options {

        @OSRSOnly
        private static final int CONTAINER = 370;
        private static final int SUB_TAB_VARBIT = 9683;

        /**
         * Gets whether or not the house options tab is currently opened.
         *
         * @return true if the tab is opened, otherwise false.
         */
        @OSRSOnly
        public static boolean isTabOpened() {
            return !Interfaces.newQuery().containers(CONTAINER).grandchildren(false)
                .types(InterfaceComponent.Type.LABEL).texts("House Options").visible().results()
                .isEmpty();
        }

        /**
         * Opens the house options tab and the options tab, if necessary.
         *
         * @return true if the tab is opened or is already open, otherwise false.
         */
        @OSRSOnly
        public static boolean openTab() {
            if (isTabOpened()) {
                return true;
            }
            if (!OptionsTab.Menu.CONTROLS.isOpen()) {
                OptionsTab.Menu.CONTROLS.open();
            }
            InterfaceComponent button = Interfaces.newQuery().containers(116).grandchildren(false)
                .types(InterfaceComponent.Type.CONTAINER).actions("View House Options").results()
                .first();
            return button != null && button.interact("View House Options") &&
                Execution.delayUntil(Options::isTabOpened, 1200, 1800);
        }

        @OSRSOnly
        public static boolean closeTab() {
            if (!isTabOpened()) {
                return true;
            }
            InterfaceComponentQueryResults results =
                Interfaces.newQuery().containers(CONTAINER).grandchildren(false)
                    .types(InterfaceComponent.Type.SPRITE).actions("Close").results();
            InterfaceComponent button = null;
            for (final InterfaceComponent result : results) {
                if (button == null || button.getIndex() < result.getIndex()) {
                    button = result;
                }
            }
            return button != null && button.interact("Close") &&
                Execution.delayUntil(() -> !isTabOpened(), 1200, 1800);
        }

        /**
         * Gets whether or not the teleport inside option is turned on.
         *
         * @return true if teleport inside is turned on, otherwise false.
         */
        public static boolean isTeleportInsideOn() {
            final Varbit varbit = Varbits.load(4744);
            return varbit != null && varbit.getValue() == 0;
        }

        /**
         * Gets whether or not the render doors open option is turned on.
         *
         * @return true if render doors open is turned on, otherwise false.
         */
        public static boolean isRenderDoorsOpenOn() {
            return getRenderDoorsState() == 1;
        }

        /**
         * Gets whether or not the render doors closed option is turned on.
         *
         * @return true if render doors closed is turned on, otherwise false.
         */
        @OSRSOnly
        public static boolean isRenderDoorsClosedOn() {
            return getRenderDoorsState() == 0;
        }

        /**
         * Gets whether or not the do not render doors option is turned on.
         *
         * @return true if do not render doors is turned on, otherwise false.
         */
        @OSRSOnly
        public static boolean isDoNotRenderDoorsOn() {
            return getRenderDoorsState() == 2;
        }

        @OSRSOnly
        private static int getRenderDoorsState() {
            final Varbit varbit = Varbits.load(6269);
            return varbit != null ? varbit.getValue() : -1;
        }

        /**
         * Gets whether or not the building mode option is turned on.
         *
         * @return true if building mode is turned on, otherwise false.
         */
        public static boolean isBuildingModeOn() {
            final Varbit varbit = Varbits.load(2176);
            return varbit != null && varbit.getValue() == 1;
        }

        /**
         * Sets the render doors open option to the value of on or off
         *
         * @param on whether to turn the option on or off
         * @return true if the render doors open option is turned to the value of on or if it was already at the value of on.
         */
        @OSRSOnly
        @Deprecated
        public static boolean setRenderDoorsOpen(boolean on) {
            return setRenderDoorsState(on ? 1 : 0);
        }

        @OSRSOnly
        public static boolean setRenderDoorsOpen() {
            return setRenderDoorsState(1);
        }

        @OSRSOnly
        public static boolean setRenderDoorsClosed() {
            return setRenderDoorsState(0);
        }

        @OSRSOnly
        public static boolean setDoNotRenderDoors() {
            return setRenderDoorsState(2);
        }

        @OSRSOnly
        private static boolean setRenderDoorsState(int state) {
            if (getRenderDoorsState() == state) {
                return true;
            }
            if (!isTabOpened()) {
                openTab();
            }
            final InterfaceComponentQueryResults results =
                Interfaces.newQuery().containers(CONTAINER).types(InterfaceComponent.Type.SPRITE)
                    .actions("Select").results();
            InterfaceComponent button = null;
            if (results.size() == 6) {
                //there should be 6 results - three are the doors, three are the buttons.  you can interact with either, but the buttons are at 1, 3, and 5
                if (state == 0) {
                    button = results.get(1);
                }
                if (state == 1) {
                    button = results.get(3);
                }
                if (state == 2) {
                    button = results.get(5);
                }
            }
            return button != null && button.interact("Select") &&
                Execution.delayUntil(() -> getRenderDoorsState() == state, 1200, 1800);
        }

        /**
         * Sets the building mode option to the value of on
         *
         * @param on whether to turn the option on or off
         * @return true if the building mode option is turned to the value of on or if it was already at the value of on.
         */
        @OSRSOnly
        public static boolean setBuildingMode(boolean on) {
            if (isBuildingModeOn() == on) {
                return true;
            }
            if (!isTabOpened()) {
                openTab();
            }
            final InterfaceComponentQueryResults results =
                Interfaces.newQuery().containers(CONTAINER).grandchildren(false)
                    .types(InterfaceComponent.Type.SPRITE).actions("On", "Off").results();
            InterfaceComponent button = null;
            if (results.size() == 4) {
                if (on) {
                    button = results.get(0);
                } else {
                    button = results.get(1);
                }
            }
            return button != null && button.interact(on ? "On" : "Off") &&
                Execution.delayUntil(() -> isBuildingModeOn() == on, 1200, 1800);
        }

        /**
         * Sets the teleport inside option to the value of on
         *
         * @param on whether to turn the option on or off
         * @return true if the teleport inside option is turned to the value of on or if it was already at the value of on.
         */
        @OSRSOnly
        public static boolean setTeleportInside(boolean on) {
            if (isTeleportInsideOn() == on) {
                return true;
            }
            if (!isTabOpened()) {
                openTab();
            }
            final InterfaceComponentQueryResults results =
                Interfaces.newQuery().containers(CONTAINER).grandchildren(false)
                    .types(InterfaceComponent.Type.SPRITE).actions("On", "Off").results();
            InterfaceComponent button = null;
            if (results.size() == 4) {
                if (on) {
                    button = results.get(2);
                } else {
                    button = results.get(3);
                }
            }
            return button != null && button.interact(on ? "On" : "Off") &&
                Execution.delayUntil(() -> isTeleportInsideOn() == on, 1200, 1800);
        }

        /**
         * Clicks the expel guests button.
         *
         * @return true if the button to expel guests was successfully clicked, otherwise false.
         */
        public static boolean expelGuests() {
            if (!isTabOpened()) {
                openTab();
            }
            InterfaceComponentQueryResults results =
                Interfaces.newQuery().containers(CONTAINER).types(InterfaceComponent.Type.CONTAINER)
                    .actions("Expel Guests").results();
            InterfaceComponent button = null;
            for (final InterfaceComponent result : results) {
                if (button == null || button.getIndex() < result.getIndex()) {
                    button = result;
                }
            }
            return button != null && button.interact("Expel Guests");
        }

        /**
         * Clicks the leave house button.
         *
         * @return true if the button to leave the house was successfully clicked, otherwise false.
         */
        public static boolean leaveHouse() {
            if (!isTabOpened()) {
                openTab();
            }
            InterfaceComponentQueryResults results =
                Interfaces.newQuery().containers(CONTAINER).types(InterfaceComponent.Type.CONTAINER)
                    .actions("Leave House").results();
            InterfaceComponent button = null;
            for (final InterfaceComponent result : results) {
                if (button == null || button.getIndex() < result.getIndex()) {
                    button = result;
                }
            }
            return button != null && button.interact("Leave House");
        }

        /**
         * Clicks the call servant button.
         *
         * @return true if the button to call your servant was successfully clicked, otherwise false.
         */
        public static boolean callServant() {
            if (!isTabOpened()) {
                openTab();
            }
            InterfaceComponentQueryResults results =
                Interfaces.newQuery().containers(CONTAINER).types(InterfaceComponent.Type.CONTAINER)
                    .actions("Call Servant").results();
            InterfaceComponent button = null;
            for (final InterfaceComponent result : results) {
                if (button == null || button.getIndex() < result.getIndex()) {
                    button = result;
                }
            }
            return button != null && button.interact("Call Servant");
        }
    }
}
