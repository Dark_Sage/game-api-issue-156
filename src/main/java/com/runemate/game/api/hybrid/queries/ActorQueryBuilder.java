package com.runemate.game.api.hybrid.queries;

import com.google.common.primitives.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.status.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.regex.*;

public abstract class ActorQueryBuilder<T extends Actor, Q extends QueryBuilder>
    extends RotatableQueryBuilder<T, Q> {
    private Collection<Integer> models, animations, stances, overheadIcons;
    private Collection<Pattern> names, dialogues;
    private Collection<Actor> targeting, targetedBy;
    private Collection<Color> defaultColors;
    private Boolean moving, animating;
    private Integer minimumHealth, maximumHealth;
    private boolean targetless = false;

    public final Q animating(final boolean animating) {
        this.animating = animating;
        return get();
    }

    public final Q animating() {
        return animating(true);
    }

    public final Q names(final String... names) {
        return names(Regex.getPatternsForExactStrings(names).toArray(new Pattern[names.length]));
    }

    public final Q overheadIcons(final Collection<Integer> overheadIcons) {
        this.overheadIcons = overheadIcons;
        return get();
    }

    public final Q overheadIcons(final int... overheadIcons) {
        return overheadIcons(Ints.asList(overheadIcons));
    }

    public final Q names(final Pattern... names) {
        return names(Arrays.asList(names));
    }

    public final Q names(final Collection<Pattern> names) {
        this.names = names;
        return get();
    }

    public final Q moving(final boolean moving) {
        this.moving = moving;
        return get();
    }

    public final Q moving() {
        return moving(true);
    }

    public final Q models(final int... models) {
        return models(Ints.asList(models));
    }

    public final Q models(final Collection<Integer> models) {
        this.models = models;
        return get();
    }

    /**
     * Adds the requirement that the result has a model with one of the specified default colors
     */
    public final Q defaultColors(final Color... colors) {
        return defaultColors(Arrays.asList(colors));
    }

    public final Q defaultColors(final Collection<Color> colors) {
        this.defaultColors = colors;
        return get();
    }

    /**
     * Sets the required dialogue to match one of the specified dialogues.
     */
    public final Q dialogues(final String... dialogues) {
        return dialogues(
            Regex.getPatternsForExactStrings(dialogues).toArray(new Pattern[dialogues.length]));
    }

    /**
     * Sets the required dialogue to contain one of the specified dialogues.
     */
    public final Q dialogueContains(final String... dialogueContains) {
        return dialogues(Regex.getPatternsForContainsStrings(dialogueContains));
    }

    public final Q dialogues(final Pattern... dialogues) {
        return dialogues(Arrays.asList(dialogues));
    }

    public final Q dialogues(final Collection<Pattern> dialogues) {
        this.dialogues = dialogues;
        return get();
    }

    public final Q targeting(Actor... targeting) {
        if (targeting == null) {
            this.targetless = true;
            return targeting(Collections.emptyList());
        }
        return targeting(Arrays.asList(targeting));
    }

    public final Q targeting(Collection<Actor> targeting) {
        if (targeting == null) {
            this.targetless = true;
            targeting = Collections.emptyList();
        }
        this.targeting = targeting;
        return get();
    }

    public final Q targetedBy(Actor... targetedBy) {
        return targetedBy(Arrays.asList(targetedBy));
    }

    public final Q targetedBy(Collection<Actor> targetedBy) {
        this.targetedBy = targetedBy;
        return get();
    }

    public final Q animations(int... animations) {
        return animations(Ints.asList(animations));
    }

    public final Q animations(Collection<Integer> animations) {
        this.animations = animations;
        return get();
    }

    public final Q stances(int... stances) {
        return stances(Ints.asList(stances));
    }

    public final Q stances(Collection<Integer> stances) {
        this.stances = stances;
        return get();
    }

    public final Q health(int minimumHealth, int maximumHealth) {
        this.minimumHealth = minimumHealth;
        this.maximumHealth = maximumHealth;
        return get();
    }

    public final Q health(int health) {
        this.minimumHealth = health;
        this.maximumHealth = health;
        return get();
    }

    @Override
    public boolean accepts(T argument) {
        boolean condition;
        if (animations != null || animating != null) {
            condition = false;
            int animationId = argument.getAnimationId();
            if (animating != null) {
                condition = (animationId != -1);
            } else {
                for (final int an_animation : animations) {
                    if (animationId == an_animation) {
                        condition = true;
                        break;
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (stances != null) {
            condition = false;
            int stance = argument.getStanceId();
            for (final int a_stance : stances) {
                if (stance == a_stance) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (names != null) {
            condition = false;
            String name = argument.getName();
            if (name != null) {
                for (final Pattern pattern : names) {
                    if (pattern.matcher(name).find()) {
                        condition = true;
                        break;
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        int health = -1;
        if (minimumHealth != null) {
            if (health == -1) {
                CombatGauge gauge = argument.getHealthGauge();
                if (gauge != null) {
                    health = gauge.getPercent();
                }
            }
            if (health != -1 && health < minimumHealth) {
                return false;
            }
        }
        if (maximumHealth != null) {
            if (health == -1) {
                CombatGauge gauge = argument.getHealthGauge();
                if (gauge != null) {
                    health = gauge.getPercent();
                }
            }
            if (health != -1 && health > minimumHealth) {
                return false;
            }
        }
        if (targeting != null) {
            condition = false;
            Actor target = argument.getTarget();
            if (targetless) {
                condition = target == null;
            } else {
                for (final Actor desiredTarget : targeting) {
                    if (Objects.equals(target, desiredTarget)) {
                        condition = true;
                        break;
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (targetedBy != null) {
            condition = false;
            for (final Actor attacker : targetedBy) {
                if (attacker != null) {
                    Actor target = attacker.getTarget();
                    if (target != null && target.equals(argument)) {
                        condition = true;
                        break;
                    }
                } else {
                    boolean targeted = false;
                    for (Actor target : getTargetedActors()) {
                        if (target != null && target.equals(argument)) {
                            targeted = true;
                            break;
                        }
                    }
                    if (!targeted) {
                        condition = true;
                        break;
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (dialogues != null) {
            condition = false;
            String dialogue = argument.getDialogue();
            if (dialogue != null) {
                for (final Pattern pattern : dialogues) {
                    if (pattern.matcher(dialogue).find()) {
                        condition = true;
                        break;
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        Model model = null;
        if (models != null) {
            condition = false;
            model = argument.getModel();
            if (model != null) {
                int modelHash = model.hashCode();
                for (final int hash : models) {
                    if (hash == modelHash) {
                        condition = true;
                        break;
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (defaultColors != null) {
            condition = false;
            if (model == null) {
                model = argument.getModel();
            }
            if (model != null) {
                Set<Color> colors = model.getDefaultColors();
                outer:
                for (final Color dc : defaultColors) {
                    for (final Color c : colors) {
                        if (Objects.equals(c, dc)) {
                            condition = true;
                            break outer;
                        }
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (overheadIcons != null) {
            condition = false;
            for (final OverheadIcon icon : argument.getOverheadIcons()) {
                if (overheadIcons.contains(icon.getId())) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (moving != null && argument.isMoving() != moving) {
            return false;
        }
        return super.accepts(argument);
    }

    private List<Npc> getNpcs() {
        Map<String, Object> cache = getCache();
        List<Npc> npcs = (List<Npc>) cache.get("npcs");
        if (npcs == null) {
            npcs = Npcs.getLoaded().asList();
            cache.put("npcs", npcs);
        }
        return npcs;
    }

    private List<Player> getPlayers() {
        Map<String, Object> cache = getCache();
        List<Player> players = (List<Player>) cache.get("players");
        if (players == null) {
            players = Players.getLoaded().asList();
            cache.put("players", players);
        }
        return players;
    }

    private Set<Actor> getTargetedActors() {
        Map<String, Object> cache = getCache();
        Set<Actor> targeted_actors = (Set<Actor>) cache.get("targeted_actors");
        if (targeted_actors == null) {
            List<Npc> npcs = getNpcs();
            List<Player> players = getPlayers();
            targeted_actors = new HashSet<>(npcs.size() + players.size());
            for (Npc npc : npcs) {
                targeted_actors.add(npc.getTarget());
            }
            for (Player player : players) {
                targeted_actors.add(player.getTarget());
            }
            cache.put("targeted_actors", targeted_actors);
        }
        return targeted_actors;
    }
}
