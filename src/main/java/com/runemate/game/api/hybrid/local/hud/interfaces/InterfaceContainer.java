package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.util.*;
import java.awt.*;
import java.util.function.*;
import javafx.scene.canvas.*;

public abstract class InterfaceContainer implements Validatable, Renderable {
    protected final long uid;
    private final int index;

    public InterfaceContainer(final long uid, final int index) {
        this.uid = uid;
        this.index = index;
    }

    public final InterfaceComponentQueryResults getComponents() {
        return getComponents(null);
    }

    public abstract InterfaceComponentQueryResults getComponents(
        final Predicate<InterfaceComponent> predicate
    );

    public final int getIndex() {
        return index;
    }


    @Override
    public final boolean isValid() {
        return OpenClient.validate(uid);
    }

    @Override
    public void render(Graphics2D g2d) {
        Parallelize.collect(getComponents(), InterfaceComponent::isVisible).forEach(ic -> ic.render(g2d));
    }

    @Override
    public void render(GraphicsContext gc) {
        Parallelize.collect(getComponents(), InterfaceComponent::isVisible).forEach(ic -> ic.render(gc));
    }

    @Override
    public String toString() {
        return "InterfaceContainer " + index;
    }

    public abstract InterfaceComponent getComponent(final int index);
}
