package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.cache.item.*;
import java.util.*;

public class SpotAnimationDefinitionLoader extends SerializedFileLoader<CacheSpotAnimationDefinition> {
    private final boolean rs3;

    public SpotAnimationDefinitionLoader(boolean rs3, int file) {
        super(file);
        this.rs3 = rs3;
    }

    @Override
    protected CacheSpotAnimationDefinition construct(int entry, int file, Map<String, Object> arguments) {
        return new CacheSpotAnimationDefinition(rs3, rs3 ? (entry << 8) + file : file);
    }
}
