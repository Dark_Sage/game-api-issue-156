package com.runemate.game.api.hybrid.util;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.script.framework.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.stream.*;
import org.jetbrains.annotations.*;

public class Parallelize {

    public static final int MAX_PARALLELISM = 16;
    public static final ConcurrentMap<AbstractBot, ForkJoinPool> BOT_FORK_JOIN_POOLS = new ConcurrentHashMap<>();

    @NotNull
    public static <T> List<T> collectToList(Collection<T> collection, Predicate<T> predicate) {
        if (predicate == null) {
            return collection instanceof List ? (List<T>) collection : new ArrayList<>(collection);
        }
        final List<T> result = execute(collection, stream -> stream.filter(predicate).collect(Collectors.toList()));
        return result != null ? result : Collections.emptyList();
    }

    @NotNull
    public static <T, R> List<R> mapAndCollectToList(Collection<T> collection, Function<? super T, ? extends R> mapper) {
        final List<R> result = execute(collection, stream -> stream.map(mapper).collect(Collectors.toList()));
        return result != null ? result : Collections.emptyList();
    }

    @NotNull
    public static <T> Set<T> collect(Collection<T> collection, Predicate<T> predicate) {
        if (predicate == null) {
            return collection instanceof Set ? (Set<T>) collection : new HashSet<>(collection);
        }
        final Set<T> result = execute(collection, stream -> stream.filter(predicate).collect(Collectors.toSet()));
        return result != null ? result : Collections.emptySet();
    }

    @Nullable
    public static <T> T findFirst(Collection<T> collection, Predicate<T> predicate) {
        if (collection.isEmpty()) {
            return null;
        }
        return execute(collection, stream -> stream.filter(predicate).findFirst().orElse(null));
    }

    @NotNull
    public static <T, R> Set<R> map(Collection<T> collection, Function<? super T, ? extends R> mapper) {
        final Set<R> result = execute(collection, stream -> stream.map(mapper).collect(Collectors.toSet()));
        return result != null ? result : Collections.emptySet();
    }

    public static <T> boolean anyMatch(Collection<T> collection, Predicate<T> predicate) {
        final Boolean result = execute(collection, stream -> stream.anyMatch(predicate));
        return result != null ? result : false;
    }

    public static <T> boolean allMatch(Collection<T> collection, Predicate<T> predicate) {
        final Boolean result = execute(collection, stream -> stream.allMatch(predicate));
        return result != null ? result : false;
    }

    public static <T> boolean noneMatch(Collection<T> collection, Predicate<T> predicate) {
        final Boolean result = execute(collection, stream -> stream.noneMatch(predicate));
        return result != null ? result : false;
    }

    public static <T> void forEach(Collection<T> collection, Consumer<T> action) {
        execute(collection, (Function<Stream<T>, Void>) stream -> {
            stream.forEach(action);
            return null;
        });
    }

    private static <T, R> R execute(Collection<T> collection, Function<Stream<T>, R> func) {
        if (collection == null) {
            return null;
        }
        final AbstractBot bot = Environment.getBot();
        if (bot == null) {
            return null;
        }
        try {
            return getForkJoinPool(bot)
                .submit(() -> func.apply(collection.parallelStream()))
                .get();
        } catch (InterruptedException | ExecutionException | CancellationException e) {
            Environment.getLogger().debug("Parallel collect task were cancelled");
            return null;
        }
    }

    private static ForkJoinPool getForkJoinPool(AbstractBot bot) {
        return BOT_FORK_JOIN_POOLS.computeIfAbsent(
            bot,
            it -> new ForkJoinPool(Math.min(MAX_PARALLELISM, Runtime.getRuntime().availableProcessors()))
        );
    }
}
