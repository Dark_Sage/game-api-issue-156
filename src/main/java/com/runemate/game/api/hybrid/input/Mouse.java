package com.runemate.game.api.hybrid.input;

import com.runemate.client.framework.open.*;
import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.local.hud.MenuItem;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.internal.input.*;
import java.awt.*;
import java.awt.event.*;
import java.rmi.*;
import java.util.List;
import java.util.*;
import lombok.*;

public final class Mouse {

    public static final PathGenerator HOPPING_PATH_GENERATOR = new HoppingPathGenerator();
    @Deprecated
    public static final PathGenerator CLOUSE_PATH_GENERATOR = new MultiLayerPerceptronPathGenerator();
    public static final PathGenerator MLP_PATH_GENERATOR = new MultiLayerPerceptronPathGenerator();
    public static final PathGenerator DEFAULT_PATH_GENERATOR = MLP_PATH_GENERATOR;
    public static final double DEFAULT_MOUSE_SPEED = 1.00;

    private Mouse() {
    }

    /**
     * Presses a button and waits a calculated time before release.
     */
    public static boolean click(final Button button) {
        int clickPressLength = Random.nextInt(10, 100);
        if (clickPressLength < 0) {
            System.err.println("[Debug] The ClickPressLength biostatistic returned an unusable value of " + clickPressLength + '.');
            return click(button);
        }
        return click(button, clickPressLength);
    }

    /**
     * Presses a button, and waits a specific amount of time before release
     */
    public static boolean click(final Button button, final int delay) {
        return press(button) && Execution.delay(delay) && release(button);
    }

    /**
     * Moves the mouse over the target and presses the specified button
     *
     * @return true if the target acknowledges the click
     */
    public static boolean click(final Interactable target, final Button button) {
        if (!move(target)) {
            //System.out.println("[Debug] Movement failed on "+target.getClass().getName());
            return false;
        }
        boolean keyRun = (
            target instanceof Locatable || (
                target instanceof MenuItem && ((MenuItem) target).getTargetEntity() instanceof Locatable
            )
        ) && !Traversal.isRunEnabled() && PlayerSense.getAsBoolean(PlayerSense.Key.RUN_ON_INTERACTION_VIA_KEY)
            && Traversal.getRunEnergy() >= PlayerSense.getAsInteger(PlayerSense.Key.ENABLE_RUN_AT);
        if (keyRun) {
            Keyboard.pressKey(Keyboard.Key.KEY_CTRL);
        }
        boolean response = click(button);
        if (keyRun) {
            Keyboard.releaseKey(Keyboard.Key.KEY_CTRL);
        }
        return response;
    }

    public static boolean drag(final Interactable start, final Interactable target) {
        return drag(start, target, Button.LEFT);
    }

    public static boolean drag(final Interactable start, final Interactable target, final Button button) {
        return move(start) && press(button) && move(target) && release(button);
    }


    public static CrosshairState getCrosshairState() {
        return CrosshairState.values()[OpenInput.getCrosshairState()];
    }

    public static PathGenerator getPathGenerator() {
        final AbstractBot bot = Environment.getBot();
        if (bot == null) {
            return DEFAULT_PATH_GENERATOR;
        }
        return (PathGenerator) bot.getConfiguration().computeIfAbsent("mouse.path.generator", t -> DEFAULT_PATH_GENERATOR);
    }

    public static void setPathGenerator(final PathGenerator generator) {
        if (generator == null) {
            throw new IllegalArgumentException("Generator must not be null");
        }
        final AbstractBot script = Environment.getBot();
        if (script != null) {
            script.getConfiguration().put("mouse.path.generator", generator);
        }
    }

    /**
     * The current position of the virtual mouse
     */
    public static Point getPosition() {
        //TODO we need to make the mouse position be updated when a human intervenes
        return Environment.getBot().getInputManager().getMousePosition();
    }

    public static double getSpeedMultiplier() {
        final AbstractBot bot = Environment.getBot();
        if (bot == null) {
            return DEFAULT_MOUSE_SPEED;
        }
        return (double) bot.getConfiguration().computeIfAbsent("mouse.speed.multiplier", t -> DEFAULT_MOUSE_SPEED);
    }

    public static void setSpeedMultiplier(final double multiplier) {
        if (multiplier <= 0) {
            throw new IllegalArgumentException("Mouse speed multiplier must be greater than 0");
        }
        final AbstractBot script = Environment.getBot();
        if (script != null) {
            script.getConfiguration().put("mouse.speed.multiplier", multiplier);
        }
    }

    /**
     * The target that the mouse is moving towards
     */
    public static Interactable getTarget() {
        final AbstractBot bot = Environment.getBot();
        return bot == null ? null : (Interactable) bot.getInputManager().getMouseTarget();
    }

    public static Interactable getPreviousTarget() {
        final AbstractBot bot = Environment.getBot();
        return bot == null ? null : (Interactable) bot.getInputManager().getPreviousMouseTarget();
    }

    public static boolean isInputAllowed() {
        return !OpenInput.isMouseInputBlocked();
    }

    public static boolean isMenuInteractionForced() {
        final AbstractBot bot = Environment.getBot();
        if (bot != null && bot.getConfiguration().containsKey("menu.interaction.forced")) {
            return (boolean) bot.getConfiguration().get("menu.interaction.forced");
        }
        return OpenClientPreferences.getForceMenuInteraction();
    }

    public static boolean isMoving() {
        return getTarget() != null;
    }

    /**
     * Gets whether the specified mouse button is pressed
     */
    public static boolean isPressed(Button button) {
        final AbstractBot bot = Environment.getBot();
        return bot != null && bot.getInputManager().getPressedMouseButtons().contains(button.button);
    }

    /**
     * Gets whether or mouse button is pressed
     */
    public static boolean isPressed() {
        final AbstractBot bot = Environment.getBot();
        return bot != null && !bot.getInputManager().getPressedMouseButtons().isEmpty();
    }

    /**
     * Moves the mouse over the interactable target
     *
     * @return true if hovering the target
     */
    public static synchronized boolean move(final Interactable target) {
        double speed = getSpeedMultiplier() * OpenClientPreferences.getMouseSpeedMultiplier();
        PathGenerator generator = getPathGenerator();
        final AbstractBot bot = Environment.getBot();
        if (bot == null) {
            return false;
        }
        BotInputService input = bot.getInputManager();
        try {
            input.setMouseTarget(target);
            return generator.move(target, speed);
        } finally {
            input.setPreviousMouseTarget(target);
            input.setMouseTarget(null);
        }
    }


    @SneakyThrows(RemoteException.class)
    public static boolean press(final Button button) {
        final AbstractBot bot = Environment.getBot();
        if (bot == null) {
            return false;
        }
        BotInputService input = bot.getInputManager();
        List<Integer> pressed_buttons = input.getPressedMouseButtons();
        if (pressed_buttons.contains(button.button)) {
            return false;
        }
        Point p = input.getMousePosition();
        OpenInput.pressMouse(p.x, p.y, button.mask, button.button, 1, false);
        pressed_buttons.add(button.button);
        return true;
    }


    @SneakyThrows(RemoteException.class)
    public static boolean release(final Button button) {
        final AbstractBot bot = Environment.getBot();
        if (bot == null) {
            return false;
        }
        BotInputService input = bot.getInputManager();
        List<Integer> pressed_buttons = input.getPressedMouseButtons();
        if (!pressed_buttons.contains(button.button)) {
            return false;
        }
        Point p = input.getMousePosition();
        OpenInput.releaseMouse(p.x, p.y, button.mask, button.button, 1, false);
        pressed_buttons.removeIf(i -> i == button.button);
        OpenInput.clickMouse(p.x, p.y, button.mask, button.button, 1, false);
        return true;
    }

    /**
     * Scrolls the mouse
     *
     * @param down true to scroll the mouse down, otherwise up
     */
    public static void scroll(final boolean down) {
        scroll(down, (int) Random.nextGaussian(Random.nextInt(20, 40), Random.nextInt(250, 300), 120));
    }

    /**
     * Rotates the mouse wheel and then delays for the specified amount of time
     */

    @SneakyThrows(RemoteException.class)
    public static void scroll(final boolean down, final int delay) {
        final AbstractBot bot = Environment.getBot();
        if (bot == null) {
            return;
        }
        BotInputService input = bot.getInputManager();
        Point p = input.getMousePosition();
        OpenInput.scrollMouse(p.x, p.y, 0, 0, false, MouseWheelEvent.WHEEL_UNIT_SCROLL, 3, down ? 1 : -1);
        Execution.delay(delay);
    }

    public static void setForceMenuInteraction(boolean value) {
        final AbstractBot bot = Environment.getBot();
        if (bot != null) {
            bot.getConfiguration().put("menu.interaction.forced", value);
        } else {
            throw new IllegalStateException("The value of forcing menu interaction can only be changed while a bot is active.");
        }
    }

    /**
     * DEPRECATED - toggling mouse and keyboard input is now exclusively controlled by the user, who can set a default value via user settings.
     */
    @Deprecated
    public static void toggleInput() {
        Environment.getLogger().warn("[Deprecated API Usage] Mouse.toggleInput() is deprecated and could be removed at a later date.");
    }

    @Deprecated
    public static boolean wasClickSuccessful(Interactable target) {
        CrosshairState desiredCrosshair = Mouse.getOverlayCrosshairColorFor(target);
        return CrosshairState.VARIES.equals(desiredCrosshair) || Objects.equals((getCrosshairState()), desiredCrosshair);
    }


    /**
     * Checks if a recent click was likely successful based on target type and the current crosshair state.
     * This is used internally by the interaction engine and shouldn't be used elsewhere.
     */
    public static Mouse.CrosshairState getOverlayCrosshairColorFor(Interactable target) {
        //TODO not all of this is liekly route
        if (target == null || target instanceof InterfaceComponent || target instanceof SpriteItem
            || target instanceof Coordinate.MinimapCoordinate) {
            return CrosshairState.NONE;
        }
        while (target instanceof Interactable) {
            if (target instanceof Model) {
                target = ((Model) target).getOwner();
            } else if (target instanceof MenuItem) {
                target = ((MenuItem) target).getTargetEntity();
            } else {
                break;
            }
        }
        if (target instanceof InteractablePoint || target instanceof InteractableRectangle || target instanceof InteractablePolygon
            || target instanceof InteractableShape) {
            return CrosshairState.VARIES;
        } else if (target instanceof Coordinate || target instanceof Area) {
            return CrosshairState.YELLOW;
        } else if (target instanceof Entity) {
            //Not red if not interactable?
            return CrosshairState.RED;
        } else {
            throw new IllegalStateException(
                "[Error] Unable to determine if the click on the interectable of type " + target.getClass().getSimpleName()
                    + " was successful because no information on the expected crosshair is available.");
        }
    }

    public enum Button {
        LEFT(MouseEvent.BUTTON1, InputEvent.BUTTON1_DOWN_MASK),
        WHEEL(MouseEvent.BUTTON2, InputEvent.BUTTON2_DOWN_MASK),
        RIGHT(MouseEvent.BUTTON3, InputEvent.BUTTON3_DOWN_MASK);
        private final int button, mask;

        Button(final int button, final int mask) {
            this.button = button;
            this.mask = mask;
        }

        @Override
        public String toString() {
            return "Button{" + "button=" + button + ", mask=" + mask + '}';
        }
    }

    public enum CrosshairState {
        NONE,
        YELLOW,
        RED,
        VARIES;

        @Override
        public String toString() {
            return name().charAt(0) + name().substring(1).toLowerCase();
        }
    }

    public abstract static class PathGenerator {

        /**
         * Sends a MouseEvent to the game that tells it the mouse moved to the given point
         *
         * @return true if it sent the mouse event, false if the point was null, it failed to send the event or the mouse was already at the specified point
         */
        public static boolean hop(Point point) {
            final AbstractBot bot = Environment.getBot();
            if (bot == null) {
                return false;
            }
            return bot.getInputManager().hop(point);
        }

        /**
         * @param target             The object to move into
         * @param velocityMultiplier A velocity multiplier with a base of 1.00
         */
        public abstract boolean move(Interactable target, double velocityMultiplier);
    }
}
