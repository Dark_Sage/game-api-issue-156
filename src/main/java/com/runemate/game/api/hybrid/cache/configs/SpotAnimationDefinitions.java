package com.runemate.game.api.hybrid.cache.configs;

import com.google.common.cache.*;
import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.api.hybrid.cache.loaders.*;
import com.runemate.game.internal.exception.*;
import com.runemate.game.cache.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import javax.annotation.*;

public class SpotAnimationDefinitions {

    private static final Cache<Integer, SpotAnimationDefinition> CACHE = CacheBuilder.newBuilder()
        .maximumSize(100)
        .expireAfterAccess(1, TimeUnit.MINUTES)
        .build();
    private static final SpotAnimationDefinitionLoader LOADER = new SpotAnimationDefinitionLoader(false, 2);

    /**
     * Gets a list of definitions within the range of [first, last].
     */
    @Nonnull
    public static List<SpotAnimationDefinition> load(final int first, final int last) {
        return load(first, last, null);
    }

    /**
     * Gets a list of definitions within the range of [first, last] that are accepted by the filter.
     */
    @Nonnull
    public static List<SpotAnimationDefinition> load(final int first, final int last, final Predicate<SpotAnimationDefinition> filter) {
        ArrayList<SpotAnimationDefinition> definitions = new ArrayList<>(last - first + 1);
        for (int id = first; id <= last; ++id) {
            final SpotAnimationDefinition definition = load(false, id);
            if (definition != null && (filter == null || filter.test(definition))) {
                definitions.add(definition);
            }
        }
        definitions.trimToSize();
        return definitions;
    }

    @Nullable
    private static SpotAnimationDefinition load(final boolean cache, final int id) {
        return load(JS5CacheController.getLargestJS5CacheController(), cache, id);
    }

    @Nullable
    private static SpotAnimationDefinition load(JS5CacheController cache, final boolean storeInCache, final int id) {
        if (id >= 0) {
            try {
                SpotAnimationDefinition def = CACHE.getIfPresent(id);
                if (def != null) {
                    return def;
                }
                CacheSpotAnimationDefinition bdef = LOADER.load(cache, ConfigType.SPOTANIM, id);
                if (bdef != null) {
                    def = bdef.extended();
                    if (storeInCache) {
                        CACHE.put(id, def);
                    }
                    return def;
                }
            } catch (final IOException ioe) {
                throw new UnableToParseBufferException(
                    "Unable to load spot animation definition for " + id + ": \"" + ioe.getMessage() + '"', ioe);
            }
        }
        return null;
    }

    /**
     * Gets the definition for the object with the specified id.
     *
     * @return The definition if available, otherwise null
     */
    public static SpotAnimationDefinition load(final int id) {
        return load(true, id);
    }

    /**
     * Loads all definitions
     */
    public static List<SpotAnimationDefinition> loadAll() {
        return loadAll(null);
    }

    /**
     * Loads all definitions that are accepted by the filter.
     */
    public static List<SpotAnimationDefinition> loadAll(final Predicate<SpotAnimationDefinition> filter) {
        int quantity;
        quantity = LOADER.getFiles(JS5CacheController.getLargestJS5CacheController(), 13).length;
        ArrayList<SpotAnimationDefinition> definitions = new ArrayList<>(quantity);
        for (int id = 0; id <= quantity; ++id) {
            final SpotAnimationDefinition definition = load(false, id);
            if (definition != null && (filter == null || filter.test(definition))) {
                definitions.add(definition);
            }
        }
        definitions.trimToSize();
        return definitions;
    }
}
