package com.runemate.game.internal.events;

import com.runemate.client.framework.open.*;
import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.status.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.osrs.entities.*;
import com.runemate.game.api.osrs.region.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.core.*;
import com.runemate.game.api.script.framework.listeners.*;
import com.runemate.game.api.script.framework.listeners.dispatchers.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import org.jetbrains.annotations.*;

/**
 * Processes and dispatches events to the registered listeners.
 */
public class EventDispatcherImpl extends EventDispatcher {

    private final HashSet<Class<? extends EventListener>> using = new HashSet<>();
    private final AbstractBot bot;

    public EventDispatcherImpl(AbstractBot bot) {
        super(bot);
        this.bot = bot;
    }

    private <T> void forEach(Class<T> type, Consumer<T> consumer) {
        for (final var listener : listeners) {
            if (type.isAssignableFrom(listener.getClass())) {
                consumer.accept((T) listener);
            }
        }
    }

    /**
     * Forwards an event to the registered listeners of the correct type.
     */
    public void dispatchLater(@NotNull Event event) {
        try {
            bot.getPlatform().invokeLater(() -> {
                for (final EventListener listener : listeners) {
                    if (listener instanceof GlobalListener) {
                        ((GlobalListener) listener).onEvent(event);
                    }
                }

                if (event instanceof EntityEvent) {
                    switch (((EntityEvent) event).getEntityType()) {
                        case PLAYER: {
                            if (event instanceof DeathEvent) {
                                forEach(PlayerListener.class, l -> l.onPlayerDeath((DeathEvent) event));
                            } else if (event instanceof AnimationEvent) {
                                forEach(PlayerListener.class, l -> l.onPlayerAnimationChanged((AnimationEvent) event));
                            } else if (event instanceof TargetEvent) {
                                forEach(PlayerListener.class, l -> l.onPlayerTargetChanged((TargetEvent) event));
                            } else if (event instanceof PlayerMovementEvent) {
                                forEach(PlayerListener.class, l -> l.onPlayerMoved((PlayerMovementEvent) event));
                            } else if (event instanceof HitsplatEvent) {
                                forEach(PlayerListener.class, l -> l.onPlayerHitsplat((HitsplatEvent) event));
                            }
                            break;
                        }
                        case NPC: {
                            if (((EntityEvent) event).getEntityType() == EntityEvent.EntityType.NPC) {
                                if (event instanceof DeathEvent) {
                                    forEach(NpcListener.class, l -> l.onNpcDeath((DeathEvent) event));
                                } else if (event instanceof AnimationEvent) {
                                    forEach(NpcListener.class, l -> l.onNpcAnimationChanged((AnimationEvent) event));
                                } else if (event instanceof TargetEvent) {
                                    forEach(NpcListener.class, l -> l.onNpcTargetChanged((TargetEvent) event));
                                } else if (event instanceof NpcSpawnedEvent) {
                                    forEach(NpcListener.class, l -> l.onNpcSpawned((NpcSpawnedEvent) event));
                                } else if (event instanceof HitsplatEvent) {
                                    forEach(NpcListener.class, l -> l.onNpcHitsplat((HitsplatEvent) event));
                                }
                            }
                        }
                        break;
                        case PROJECTILE: {
                            if (event instanceof ProjectileLaunchEvent) {
                                forEach(ProjectileLaunchListener.class, l -> l.onProjectileLaunched((ProjectileLaunchEvent) event));
                            }
                            break;
                        }
                        case GROUNDITEM: {
                            if (event instanceof GroundItemSpawnedEvent) {
                                forEach(GroundItemListener.class, l -> l.onGroundItemSpawned((GroundItemSpawnedEvent) event));
                            }
                            break;
                        }
                        default:
                            //Simply serves as a reminder in case we ever add other EntityType listeners
                            throw new IllegalStateException("Unsupported EntityEvent: " + event.getClass().getSimpleName());
                    }
                }

                if (event instanceof EngineEvent) {
                    EngineEvent.Type type = ((EngineEvent) event).getType();
                    for (final EventListener listener : listeners) {
                        if (listener instanceof EngineListener) {
                            switch (type) {
                                case CLIENT_CYCLE:
                                    ((EngineListener) listener).onCycleStart();
                                    break;
                                case SERVER_TICK:
                                    ((EngineListener) listener).onTickStart();
                                    break;
                                default:
                                    System.err.println("Unsupported engine event type of " + type);
                            }
                        }
                    }
                } else if (event instanceof CS2ScriptEvent) {
                    final CS2ScriptEvent sEvent = ((CS2ScriptEvent) event);
                    forEach(CS2ScriptEventListener.class, l -> {
                        if (CS2ScriptEvent.Type.STARTED.equals(sEvent.getType())) {
                            l.onScriptExecutionStarted(sEvent);
                        }
                    });
                } else if (event instanceof EngineStateEvent) {
                    forEach(EngineListener.class, l -> l.onEngineStateChanged((EngineStateEvent) event));
                } else if (event instanceof MenuInteractionEvent) {
                    forEach(MenuInteractionListener.class, l -> l.onInteraction((MenuInteractionEvent) event));
                } else if (event instanceof ProjectileLaunchEvent) {
                    forEach(ProjectileLaunchListener.class, l -> l.onProjectileLaunched((ProjectileLaunchEvent) event));
                } else if (event instanceof ItemEvent) {
                    final ItemEvent itemEvent = (ItemEvent) event;
                    for (final EventListener listener : listeners) {
                        SpriteItem item = itemEvent.getItem();
                        if (SpriteItem.Origin.INVENTORY.equals(item.getOrigin())) {
                            if (listener instanceof InventoryListener) {
                                if (itemEvent.getType() == ItemEvent.Type.ADDITION) {
                                    ((InventoryListener) listener).onItemAdded(itemEvent);
                                } else if (itemEvent.getType() == ItemEvent.Type.REMOVAL) {
                                    ((InventoryListener) listener).onItemRemoved(itemEvent);
                                }
                            }
                        } else if (SpriteItem.Origin.EQUIPMENT.equals(item.getOrigin())) {
                            if (listener instanceof EquipmentListener) {
                                if (itemEvent.getType() == ItemEvent.Type.ADDITION) {
                                    ((EquipmentListener) listener).onItemEquipped(itemEvent);
                                } else if (itemEvent.getType() == ItemEvent.Type.REMOVAL) {
                                    ((EquipmentListener) listener).onItemUnequipped(itemEvent);
                                }
                            }
                        }
                    }
                } else if (event instanceof SkillEvent) {
                    forEach(SkillListener.class, l -> {
                        final var sEvent = (SkillEvent) event;
                        if (sEvent.getType() == SkillEvent.Type.LEVEL_GAINED) {
                            l.onLevelUp(sEvent);
                        } else if (sEvent.getType() == SkillEvent.Type.EXPERIENCE_GAINED) {
                            l.onExperienceGained(sEvent);
                        } else if (sEvent.getType() == SkillEvent.Type.CURRENT_LEVEL_CHANGED) {
                            l.onCurrentLevelChanged(sEvent);
                        }
                    });
                } else if (event instanceof MessageEvent) {
                    forEach(ChatboxListener.class, l -> l.onMessageReceived((MessageEvent) event));
                } else if (event instanceof VarpEvent) {
                    forEach(VarpListener.class, l -> l.onValueChanged((VarpEvent) event));
                } else if (event instanceof VarcEvent) {
                    final var vEvent = (VarcEvent) event;
                    forEach(VarcListener.class, l -> {
                        if (vEvent.isString()) {
                            l.onStringChanged(vEvent);
                        } else {
                            l.onIntChanged(vEvent);
                        }
                    });
                } else if (event instanceof VarbitEvent) {
                    forEach(VarbitListener.class, l -> l.onValueChanged((VarbitEvent) event));
                } else if (event instanceof GrandExchangeEvent) {
                    forEach(GrandExchangeListener.class, l -> l.onSlotUpdated((GrandExchangeEvent) event));
                } else if (event instanceof GroundItemSpawnedEvent) {
                    forEach(GroundItemListener.class, l -> l.onGroundItemSpawned((GroundItemSpawnedEvent) event));
                } else if (event instanceof RegionLoadedEvent) {
                    forEach(RegionListener.class, l -> l.onRegionLoaded((RegionLoadedEvent) event));
                } else if (event instanceof SettingChangedEvent) {
                    forEach(SettingsListener.class, l -> l.onSettingChanged((SettingChangedEvent) event));
                } else if (event instanceof SettingsConfirmedEvent) {
                    forEach(SettingsListener.class, SettingsListener::onSettingsConfirmed);
                } else if (event instanceof HitsplatEvent) { //TODO Handled by EntityEvent branch - remove this at a later date
                    for (final EventListener listener : listeners) {
                        //Leaving this here so we don't break existing bots that rely on this listener
                        if (listener instanceof HitsplatListener) {
                            ((HitsplatListener) listener).onHitsplatAdded((HitsplatEvent) event);
                        }
                        final var e = (HitsplatEvent) event;
                        if (e.getType() == HitsplatEvent.Type.NPC) {
                            if (listener instanceof NpcListener) {
                                ((NpcListener) listener).onNpcHitsplat(e);
                            }
                        } else if (e.getType() == HitsplatEvent.Type.PLAYER) {
                            if (listener instanceof PlayerListener) {
                                ((PlayerListener) listener).onPlayerHitsplat(e);
                            }
                        }
                    }
                } else if (event instanceof TargetEvent) { //TODO Handled by EntityEvent branch - remove this at a later date
                    for (final EventListener listener : listeners) {
                        if (listener instanceof TargetListener) {
                            ((TargetListener) listener).onTargetChanged((TargetEvent) event);
                        }
                    }
                } else if (event instanceof PlayerMovementEvent) { //TODO Handled by EntityEvent branch - remove this at a later date
                    for (final EventListener listener : listeners) {
                        if (listener instanceof PlayerMovementListener) {
                            ((PlayerMovementListener) listener).onPlayerMoved((PlayerMovementEvent) event);
                        }
                    }
                } else if (event instanceof DeathEvent) { //TODO Handled by EntityEvent branch - remove this at a later date
                    for (final EventListener listener : listeners) {
                        if (listener instanceof DeathListener) {
                            ((DeathListener) listener).onDeath((DeathEvent) event);
                        }
                    }
                } else if (event instanceof AnimationEvent) { //TODO Handled by EntityEvent branch - remove this at a later date
                    for (final EventListener listener : listeners) {
                        if (listener instanceof AnimationListener) {
                            ((AnimationListener) listener).onAnimationChanged((AnimationEvent) event);
                        }
                    }
                }
            });
        } catch (final Throwable t) {
            ClientAlarms.handle(bot, t);
            t.printStackTrace();
            if (!bot.isStopped()) {
                bot.stop(t.getMessage());
            }
        }
    }

    /**
     * Registers a listener to dispatch in-game events to
     *
     * @param listener a listener (PaintListener, MouseListener, KeyListener, etc)
     */
    public void addListener(final EventListener listener) {
        listeners.add(listener);
        //The GrandExchangeListener is the only listeners that still rely on polling.
        if (listener instanceof GrandExchangeListener && !using.contains(GrandExchangeListener.class)) {
            GrandExchangeDispatcher dispatcher = new GrandExchangeDispatcher();
            pool.scheduleWithFixedDelay(dispatcher, 100, dispatcher.getIterationRateInMilliseconds(), TimeUnit.MILLISECONDS);
            using.add(GrandExchangeListener.class);
        }
    }

    @Override
    public void createAnimationEvent(@NotNull String type, long characterUid, int animationId) {
        final var entityType = EntityEvent.resolveType(type);
        dispatchLater(new AnimationEvent(entityType, createActor(entityType, characterUid), animationId));
    }

    @Override
    public void createChatboxEvent(int type, @NotNull String sender, @NotNull String message) {
        dispatchLater(new MessageEvent(Chatbox.Message.Type.resolve(type, message, sender), sender, message));
    }

    @Override
    public void createConnectionStateEvent(int old, int current) {
        dispatchLater(new EngineStateEvent(old, current));
    }

    @Override
    public void createDeathEvent(@NotNull String type, long actorUid, int gameCycle) {
        final var entityType = EntityEvent.resolveType(type);
        final Actor actor = createActor(entityType, actorUid);
        if (actor == null) //Shouldn't happen but just to be safe
        {
            return;
        }
        final Coordinate deathPos = actor.getPosition();
        final Area.Rectangular deathArea = actor.getArea();
        dispatchLater(new DeathEvent(entityType, actor, deathPos, deathArea, gameCycle));
    }

    @Override
    public void createEngineCycleEvent() {
        dispatchLater(new EngineEvent(EngineEvent.Type.CLIENT_CYCLE));
    }

    @Override
    public void createGroundItemEvent(int x, int y, int plane, long uid) {
        final var eventLoc = new Coordinate(x, y, plane);
        final var item = new OSRSGroundItem(uid, eventLoc);
        dispatchLater(new GroundItemSpawnedEvent(item, eventLoc));
    }

    @Override
    //TODO cleanup parameter types (long endCycle)
    public void createHitsplatEvent(
        @NotNull String type, long actorUid, int typeId, int damage, int specialTypeId, int startCyle, int endCycle
    ) {
        final var hitsplat = new Hitsplat(typeId, damage, specialTypeId, startCyle, endCycle);
        final var entityType = EntityEvent.resolveType(type);
        final var source = createActor(entityType, actorUid);
        dispatchLater(new HitsplatEvent(entityType, source, hitsplat));
    }

    @Override
    public void createMenuInteractionEvent(
        int arg1, int arg2, int opcode, int arg0, @NotNull String action, @NotNull String target, int mx, int my
    ) {
        dispatchLater(new MenuInteractionEvent(arg1, arg2, opcode, arg0, JagTags.remove(action), JagTags.remove(target), mx, my));
    }

    @Override
    public void createPlayerMovedEvent(long playerUid) {
        dispatchLater(new PlayerMovementEvent(new OSRSPlayer(playerUid)));
    }

    @Override
    public void createProjectileLaunchEvent(long uid) {
        dispatchLater(new ProjectileLaunchEvent(new OSRSProjectile(uid)));
    }

    @Override
    public void createRegionLoadedEvent(int x, int y) {
        final Coordinate previous = (Coordinate) bot.getCache().get("RegionBase");
        final Coordinate base = new Coordinate(x, y, 0);
        bot.getCache().put("RegionBase", base);
        dispatchLater(new RegionLoadedEvent(previous, base));

        //Force the pathfinder to download the current subregion by building a path to our current position
        bot.getPlatform().invokeLater(() -> {
            final var web = Traversal.getDefaultWeb();
            if (web != null) {
                web.getPathBuilder().buildTo(Players.getLocal());
            }
        });
    }

    @Override
    public void createScriptStartEvent(int scriptId, @Nullable Object[] scriptArgs) {
        dispatchLater(new CS2ScriptEvent(scriptId, CS2ScriptEvent.Type.STARTED, scriptArgs));

    }

    @Override
    public void createServerTickEvent() {
        dispatchLater(new EngineEvent(EngineEvent.Type.SERVER_TICK));
    }

    @Override
    public void createTargetEvent(final @NotNull String type, long actorUid, int targetIndex) {
        final var entityType = EntityEvent.resolveType(type);
        dispatchLater(new TargetEvent(
            entityType,
            createActor(entityType, actorUid),
            getCharacterTarget(targetIndex)
        ));
    }

    @Override
    public void createVarbitEvent(final int index, final int old, final int value) {
        //handled by 'createVarpEvent'
    }

    @Override
    public void createVarcIntEvent(final int index, final int old, final int value) {
        dispatchLater(new VarcEvent(false, index, old, value));
    }

    @Override
    public void createVarcStringEvent(final int index, @NotNull final String old, @NotNull final String value) {
        dispatchLater(new VarcEvent(true, index, old, value));
    }

    @Override
    public void createVarpEvent(final int index, final int old, final int value) {
        dispatchLater(new VarpEvent(Varps.getAt(index), old, value));
        Varbits.forVarp(index).forEach(varbit -> {
            final var oldValue = varbit.getValue(old);
            final var newValue = varbit.getValue(value);
            if (oldValue != newValue) {
                dispatchLater(new VarbitEvent(varbit, oldValue, newValue));
            }
        });
    }

    @Override
    public void createNpcSpawnEvent(final long uid) {
        final Npc npc = new OSRSNpc(uid);
        dispatchLater(new NpcSpawnedEvent(npc));
    }

    private Actor createActor(EntityEvent.EntityType type, long uid) {
        if (type == EntityEvent.EntityType.PLAYER) {
            return new OSRSPlayer(uid);
        } else if (type == EntityEvent.EntityType.NPC) {
            return new OSRSNpc(uid);
        }
        return null;
    }

    @Nullable
    private static Actor getCharacterTarget(int targetIndex) {
        if (targetIndex == -1) {
            return null;
        }
        if (targetIndex < 0x10000) {
            return OSRSNpcs.getByIndex(targetIndex);
        } else {
            return OSRSPlayers.getAt(targetIndex - 0x10000);
        }
    }
}
