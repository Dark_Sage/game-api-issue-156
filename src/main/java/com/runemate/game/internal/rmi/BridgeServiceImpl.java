package com.runemate.game.internal.rmi;

import com.runemate.client.framework.open.*;
import com.runemate.commons.internal.scene.entities.objects.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.*;
import lombok.*;
import org.jetbrains.annotations.*;

public class BridgeServiceImpl implements BridgeService {

    private final Map<Integer, List<SpriteItem>> itemState = new ConcurrentHashMap<>();

    private final AbstractBot bot;

    public BridgeServiceImpl(AbstractBot bot) {
        this.bot = bot;
    }

    @Nullable
    @Override
    public MinimalObjectDefinition getMinimalObjectDefinition(int i) {
        GameObjectDefinition definition = GameObjectDefinition.get(i);
        if (definition == null) {
            return null;
        }
        return new MinimalObjectDefinition(definition.getId(),
            definition.getClippingType(),
            definition.impassable(),
            definition.impenetrable()
        );

    }

    @Override
    public void initSkills() {

    }

    @Override
    public void onCurrentLevelUpdated(final int index, final int old, final int value) {
        final var skill = Skills.getByIndex(index);
        if (skill == null || old == -1 || value - old == 0) {
            return;
        }
        final var event = new SkillEvent(skill, SkillEvent.Type.CURRENT_LEVEL_CHANGED, value, old);
        bot.getEventDispatcher().dispatchLater(event);
    }

    @Override
    public void onExperienceUpdated(final int index, final int previous, final int current) {
        final var skill = Skills.getByIndex(index);
        if (skill == null || previous == -1 || current - previous == 0) {
            return;
        }
        bot.getEventDispatcher().dispatchLater(new SkillEvent(skill, SkillEvent.Type.EXPERIENCE_GAINED, current, previous));

        final var previousLevel = Skills.getLevelAtExperience(skill, previous);
        final var currentLevel = Skills.getLevelAtExperience(skill, current);
        if (currentLevel > previousLevel) {
            bot.getEventDispatcher().dispatchLater(new SkillEvent(skill, SkillEvent.Type.LEVEL_GAINED, previousLevel, currentLevel));
        }
    }

    @Override
    public void onLevelUpdated(final int index, final int old, final int value) {
        //handled by onExperienceUpdated
    }

    @Override
    public void processItemEventsFromChange(
        long invId, int itemIndex, int itemid, int itemQuantity
    ) {
        for (ItemEvent event : getItemEventsFromChange((int) invId, itemIndex, itemid, itemQuantity)) {
            bot.getEventDispatcher().dispatchLater(event);
        }
    }

    public List<ItemEvent> getItemEventsFromChange(
        int inventoryId, int itemIndex, int itemId, int itemQuantity
    ) {
        List<ItemEvent> events = new ArrayList<>(2);
        List<SpriteItem> inventory = itemState.get(inventoryId);
        if (inventory == null) {
            //TODO Events are missed when creating an inventory for the first time
            itemState.put(inventoryId, inventory = Inventories.lookup(inventoryId).asList());
        }
        List<SpriteItem> itemsInSlot = inventory.stream().filter(item -> item.getIndex() == itemIndex).collect(Collectors.toList());
        if (itemsInSlot.size() > 1) {
            bot.getLogger().severe("The inventory with id " + inventoryId + " cannot have more than one item in index " + itemIndex);
        }
        SpriteItem itemInSlot = !itemsInSlot.isEmpty() ? itemsInSlot.get(0) : null;
        if (itemInSlot == null && itemId >= 0 && itemQuantity > 0) {
            //Add item to empty slot
            SpriteItem item = new SpriteItem(itemId, itemQuantity, itemIndex, Inventories.Documented.getOrigin(inventoryId));
            inventory.add(item);
            events.add(new ItemEvent(item, itemQuantity));
        }
        if (itemInSlot != null) {
            if (itemInSlot.getId() == itemId) {
                //Update quantity of item in slot
                int stackChange = itemQuantity - itemInSlot.getQuantity();
                if (stackChange != 0) {
                    //but only bother sending an event and updating the cache if the quantity actually changed
                    SpriteItem adjustedStackItemInSlot = itemInSlot.derive(stackChange);
                    inventory.remove(itemInSlot);
                    inventory.add(adjustedStackItemInSlot);
                    events.add(new ItemEvent(adjustedStackItemInSlot, stackChange));
                }
            } else {
                //Replace an item in an occupied slot
                //1. Remove from cache and send event negativing it's entire presence
                //2. Build new item in it's slot, cache it, and fire added event.
                inventory.remove(itemInSlot);
                events.add(new ItemEvent(itemInSlot, -itemInSlot.getQuantity()));
                if (itemId != -1) {
                    SpriteItem newItemInSlot = new SpriteItem(itemId,
                        itemQuantity,
                        itemIndex,
                        Inventories.Documented.getOrigin(inventoryId)
                    );
                    inventory.add(newItemInSlot);
                    events.add(new ItemEvent(newItemInSlot, itemQuantity));
                }
            }
        }
        return events;
    }
}
