package com.runemate.game.internal.exception;

public class ResourceNotDeclaredException extends UnsupportedOperationException {
    public ResourceNotDeclaredException(String resource) {
        super("Resource not declared in manifest: " + resource);
    }

}
