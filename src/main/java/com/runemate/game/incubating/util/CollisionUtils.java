package com.runemate.game.incubating.util;

import com.runemate.game.api.hybrid.region.*;

public class CollisionUtils implements Region.CollisionFlags {
    public static int addFlag(int flag, int mask) {
        return flag | mask;
    }

    public static int removeFlag(int flag, int mask) {
        return flag & ~mask;
    }

    public static boolean hasBoundariesOnAllSides(
        int baseFlag,
        int northernFlag,
        int southernFlag,
        int easternFlag,
        int westernFlag
    ) {
        return countSidesWithBoundaries(baseFlag, northernFlag, southernFlag, easternFlag,
            westernFlag
        ) == 4;
    }

    public static boolean isUnusable(int flag) {
        return flag == PADDING || bitOn(flag, HelperFlags.BLOCKED_OFF);
    }

    public static boolean hasBoundaryOnOneOrMoreSides(
        int baseFlag,
        int northernFlag,
        int southernFlag,
        int easternFlag,
        int westernFlag
    ) {
        return countSidesWithBoundaries(baseFlag, northernFlag, southernFlag, easternFlag,
            westernFlag
        ) >= 1;
    }

    /**
     * Returns the amount of blocked sides from 0 to 4.
     *
     * @param baseFlag
     * @param northernFlag
     * @param southernFlag
     * @param easternFlag
     * @param westernFlag
     * @return
     */
    public static int countSidesWithBoundaries(
        int baseFlag,
        int northernFlag,
        int southernFlag,
        int easternFlag,
        int westernFlag
    ) {
        int count = 0;
        if (!noNorthernBoundary(baseFlag, northernFlag)) {
            count++;
        }
        if (!noEasternBoundary(baseFlag, easternFlag)) {
            count++;
        }
        if (!noSouthernBoundary(baseFlag, southernFlag)) {
            count++;
        }
        if (!noWesternBoundary(baseFlag, westernFlag)) {
            count++;
        }
        return count;
    }

    public static boolean bitOn(int flag, int mask) {
        return (flag & mask) != 0;
    }

    public static boolean bitOff(int flag, int mask) {
        return (flag & mask) == 0;
    }
    //read me
    //blocked tile (as in the flag) may be able to have characters  based on spline project.

    public static boolean allBitsOn(int flag, int... masks) {
        for (int mask : masks) {
            if (bitOff(flag, mask)) {
                return false;
            }
        }
        return true;
    }

    public static boolean anyBitsOn(int flag, int... masks) {
        for (int mask : masks) {
            if (bitOn(flag, mask)) {
                return true;
            }
        }
        return false;
    }

    public static boolean allBitsOff(int flag, int... masks) {
        for (int mask : masks) {
            if (bitOn(flag, mask)) {
                return false;
            }
        }
        return true;
    }

    public static boolean anyBitsOff(int flag, int... masks) {
        for (int mask : masks) {
            if (bitOff(flag, mask)) {
                return true;
            }
        }
        return false;
    }

    public static boolean noNorthernBoundary(int baseFlag, int northernFlag) {
        return CollisionUtils.allBitsOff(baseFlag, NORTH_BOUNDARY_OBJECT)
            &&
            CollisionUtils.allBitsOff(northernFlag, SOUTH_BOUNDARY_OBJECT, HelperFlags.BLOCKED_OFF);
    }

    public static boolean noSouthernBoundary(int baseFlag, int southernFlag) {
        return CollisionUtils.allBitsOff(baseFlag, SOUTH_BOUNDARY_OBJECT)
            &&
            CollisionUtils.allBitsOff(southernFlag, NORTH_BOUNDARY_OBJECT, HelperFlags.BLOCKED_OFF);
    }

    public static boolean noEasternBoundary(int baseFlag, int easternFlag) {
        return CollisionUtils.allBitsOff(baseFlag, EAST_BOUNDARY_OBJECT)
            &&
            CollisionUtils.allBitsOff(easternFlag, WEST_BOUNDARY_OBJECT, HelperFlags.BLOCKED_OFF);
    }

    public static boolean noWesternBoundary(int baseFlag, int westernFlag) {
        return CollisionUtils.allBitsOff(baseFlag, WEST_BOUNDARY_OBJECT)
            &&
            CollisionUtils.allBitsOff(westernFlag, EAST_BOUNDARY_OBJECT, HelperFlags.BLOCKED_OFF);
    }
}
