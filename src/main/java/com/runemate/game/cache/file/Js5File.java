package com.runemate.game.cache.file;

import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.nio.file.*;

class Js5File {
    private final File file;
    private final OpenOption[] mode;
    private FileChannel channel;

    protected Js5File(String path, OpenOption... mode) {
        this(new File(path), mode);
    }

    Js5File(File file, OpenOption... mode) {
        this.file = file;
        if (file == null) {
            throw new IllegalArgumentException("A cache file is missing!");
        }
        if (!file.exists()) {
            throw new IllegalArgumentException(
                "The cache file \"" + file.getAbsolutePath() + " \" must exist.");
        }
        this.mode = mode;
    }

    public File getFile() {
        return file;
    }

    protected ByteBuffer read() throws IOException {
        FileChannel channel = getChannel();
        ByteBuffer buffer = ByteBuffer.allocate((int) channel.size());
        channel.read(buffer);
        return buffer;
    }

    protected FileChannel getChannel() throws IOException {
        return getChannel(true);
    }

    private FileChannel openStream() throws IOException {
        return FileChannel.open(file.toPath(), mode);
    }

    private FileChannel getChannel(boolean reopen) throws IOException {
        if (reopen && (channel == null || !channel.isOpen())) {
            channel = openStream();
        }
        return channel;
    }

    @Override
    public String toString() {
        return "Js5File[" + file + ']';
    }
}
