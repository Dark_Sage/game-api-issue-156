package com.runemate.game.events.osrs;

import static com.runemate.game.api.hybrid.local.hud.interfaces.InterfaceComponent.Type.CONTAINER;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.net.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import com.runemate.game.events.*;
import lombok.*;

@CustomLog
public class GenieHandler extends GameEventHandler {

    private static final int MAX_FAILURES = 10;

    private Npc genie;
    private SpriteItem lamp;
    private String skill;
    private int failures = 0;

    @Override
    public GameEvents.GameEvent getAPIEventInstance() {
        return GameEvents.Universal.GENIE_HANDLER;
    }

    @Override
    public boolean isValid() {
        if (failures >= MAX_FAILURES) {
            disable();
            return false;
        }
        if ((skill = OpenAccountDetails.getLampSkill()) == null) {
            return false;
        }
        final var player = Players.getLocal();
        if (player == null || player.isMoving() || Bank.isOpen() || GrandExchange.isOpen() || Trade.isOpen()) {
            return false;
        }
        lamp = Inventory.newQuery().names("Lamp").actions("Rub").results().first();
        if (lamp != null) {
            return true;
        }
        genie = Npcs.newQuery().levels(0).names("Genie").reachableFrom(player).visible().targeting(player).results().first();
        return genie != null;
    }

    @Override
    public void run() {
        final Player local = Players.getLocal();
        if (local == null) {
            return;
        }
        final var selectedItem = Inventory.getSelectedItem();
        if (selectedItem != null) {
            log.fine("[GenieHandler] Deselecting item: " + selectedItem);
            if (selectedItem.click()) {
                Execution.delayWhile(() -> Inventory.getSelectedItem() != null, 1200, 2400);
            }
            return;
        }
        final var selectedSpell = Magic.getSelected();
        if (selectedSpell != null) {
            log.fine("[GenieHandler] Deselecting spell: " + selectedSpell);
            if (selectedSpell.deactivate()) {
                Execution.delayWhile(() -> Magic.getSelected() != null, 1200, 2400);
            }
            return;
        }

        final var cont = ChatDialog.getContinue();
        if (cont != null) {
            log.fine("[GenieHandler] Continuing dialog");
            if (cont.select()) {
                Execution.delayWhile(() -> ChatDialog.getContinue() != null, 1200, 2400);
            }
            return;
        }

        if (genie != null && genie.interact("Talk-to")) {
            log.fine("[GenieHandler] Talking to Genie");
            if (!Execution.delayUntil(ChatDialog::isOpen, local::isMoving, 1200, 2400)) {
                failures++;
            }
            return;
        }

        final var confirmComponent = getConfirmationButton();
        if (validateSelection(confirmComponent, skill)) {
            log.fine("[GenieHandler] Confirming selection");
            if (confirmComponent.click()) {
                if (Execution.delayWhile(confirmComponent::isVisible, 1200, 2400)) {
                    failures = 0;
                } else {
                    failures++;
                }
            }
            return;
        } else if (confirmComponent != null) {
            log.fine("[GenieHandler] Selecting " + skill);
            final var skillComponent = getSelectorForSkill(skill);
            if (skillComponent != null && skillComponent.click()) {
                if (!Execution.delayUntil(() -> validateSelection(getConfirmationButton(), skill), 1200, 2400)) {
                    failures++;
                }
            }
            return;
        }


        if (lamp != null) {
            log.fine("[GenieHandler] Rubbing lamp");
            if (lamp.interact("Rub")) {
                if (!Execution.delayUntil(() -> getConfirmationButton() != null, 1200, 2400)) {
                    failures++;
                }
            }
        }
    }

    private InterfaceComponent getSelectorForSkill(final String skill) {
        return Interfaces.newQuery().containers(240).types(CONTAINER).actions(skill).results().first();
    }

    private String getConfirmationTextFor(final String skill) {
        return "Confirm: " + skill;
    }

    private InterfaceComponent getConfirmationButton() {
        return Interfaces.getAt(240, 26, 0);
    }

    private boolean validateSelection(final InterfaceComponent component, final String skill) {
        return component != null && getConfirmationTextFor(skill).equals(component.getText());
    }
}
