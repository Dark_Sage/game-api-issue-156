package com.runemate.game.events.osrs;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.events.*;
import java.util.*;
import java.util.stream.*;
import javax.annotation.*;

public class OSRSBankPin extends GameEventHandler {
    private static final int CONTAINER = 213;
    private static final String[] STRINGS = { "FIRST", "SECOND", "THIRD", "FOURTH" };

    public OSRSBankPin() {
        super(PRIORITY_HIGH);
    }

    private static int getTarget() {
        final String text;
        final InterfaceComponent wordComponent =
            Interfaces.newQuery().containers(CONTAINER).grandchildren(false)
                .types(InterfaceComponent.Type.LABEL).textContains(STRINGS).results().first();
        if (wordComponent != null && (text = wordComponent.getText()) != null) {
            return IntStream.range(0, STRINGS.length).filter(index -> text.contains(STRINGS[index]))
                .findFirst().orElse(-1);
        }
        return -1;
    }

    @Override
    public GameEvents.GameEvent getAPIEventInstance() {
        return GameEvents.Universal.BANK_PIN;
    }

    @Override
    public boolean isValid() {
        return !Interfaces.newQuery()
            .containers(CONTAINER)
            .types(InterfaceComponent.Type.LABEL)
            .grandchildren(false)
            .textContains(STRINGS)
            .visible().results().isEmpty();
    }

    @Override
    public void run() {
        final String pin = OpenAccountDetails.getBankPin();
        if (pin == null || pin.length() != 4) {
            Environment.getBot().pause("Paused: Bank pin");
        } else {
            int target = getTarget();
            if (target != -1) {
                final char asChar = pin.toCharArray()[target];
                //Doesn't need to be visible, when hovering the text isn't visible
                InterfaceComponent targetTextComponent = Interfaces.newQuery().containers(CONTAINER)
                    .types(InterfaceComponent.Type.LABEL).texts(Character.toString(asChar))
                    .results().first();
                if (targetTextComponent != null) {
                    if (targetTextComponent.getParentComponent().interact("Select")) {
                        Execution.delayUntil(() -> getTarget() != target, 1800, 3000);
                    }
                } else {
                    InteractableRectangle r = Screen.getBounds();
                    if (r != null) {
                        //InteractableRectangles never have null interaction points
                        r.getInteractionPoint().hover();
                    }
                }
            }
        }
    }

    @Nonnull
    @Override
    public List<GameEvents.GameEvent> getChildren(AbstractBot bot) {
        return Collections.emptyList();
    }
}
