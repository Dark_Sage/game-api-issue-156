package com.runemate.bot.test;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.ui.setting.annotation.open.*;
import com.runemate.ui.setting.open.*;
import lombok.*;

/**
 * This bot can be used to test new features that you're working on.Recommend that you use this to make it obvious to the reviewer
 * how exactly the code change was tested.
 * <p>
 * The "testJar" gradle task will produce a jar containing this bot which you can place in a bot directory.
 * <p>
 * The following gradle command will build this bot and then launch the client:
 * testJar launch --args="--dev"
 */
public class FeatureTestBot extends LoopingBot {

//    @SettingsProvider(updatable = true)
    private ExampleSettings settings;

    @Override
    public void onStart(final String... arguments) {

    }

    @Override
    public void onLoop() {
        OpenClientPreferences.setLowDetail(!OpenClientPreferences.getLowDetail());
    }

    private void setStatus(@NonNull String status) {
        getConfiguration().put("bot.status", status);
    }

    @SettingsGroup
    public interface ExampleSettings extends Settings {

        @Setting(key = "testCheckbox", title = "Test Checkbox")
        default boolean testCheckbox() {
            return true;
        }

        @Suffix(Suffix.PERCENT)
        @Range(min = 1, max = 10)
        @Setting(key = "testIntSpinner", title = "Test Integer")
        default int testIntSpinner() {
            return 1;
        }

        @Setting(key = "testIntSpinner", title = "Test Integer")
        void setIntSpinner(int value);

        @Setting(key = "testDoubleSpinner", title = "Test Double")
        default double testDoubleSpinner() {
            return 1.15;
        }

        @Setting(key = "testText", title = "Test TextField")
        default String testText() {
            return "Default Text";
        }

        @Setting(key = "testPassword", title = "Test Password", secret = true)
        default String testPassword() {
            return "Default Text";
        }

        @Setting(key = "playerPosition", title = "Player Position", converter = CoordinateSettingConverter.class)
        default Coordinate testCoordinate() {
            return null;
        }

        @Setting(key = "testState", title = "Test Enum")
        default AbstractBot.State testEnum() {
            return State.RESTARTING;
        }

    }
}
